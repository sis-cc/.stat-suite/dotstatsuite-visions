var path = require('path');
var fs = require('fs');
var R = require('ramda');
var dox = require('dox');

var rootFolder = 'es';
var directoryPath = path.join(__dirname, `../${rootFolder}`);
var destination = './demo/src/components.json';

var folderNames = fs.readdirSync(directoryPath);
var data = R.pipe(
  R.map(folderName => {
    if (fs.existsSync(`./${rootFolder}/${folderName}/index.js`)) {
      var data = fs.readFileSync(`./${rootFolder}/${folderName}/index.js`);
      var source = String(data);
      return R.head(dox.parseComments(source));
    }
    return null;
  }),
  R.reject(R.isNil),
)(folderNames);

fs.writeFile(destination, JSON.stringify(data), function(err) {
  if (err) return console.log(err);

  console.log('✓ Generating documentation');
});
