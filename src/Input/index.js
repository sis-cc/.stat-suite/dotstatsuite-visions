/**
 * Input let users enter and edit text.
 *
 * @memberOf VISIONS
 * @name Input
 * @tag component
 * @api public
 * @props
 * Input.propTypes = {
 *   id: PropTypes.string,
 *   label: PropTypes.string,
 *   placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   leftIcon: PropTypes.object,
 *   rightIcon: PropTypes.object,
 *   value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *   type: PropTypes.string,
 *   onChange: PropTypes.func,
 *   onSubmit: PropTypes.func,
 *   fullWidth: PropTypes.bool,
 *   inputProps: PropTypes.object,
 * };
 * @theme
 * // no custom theme there
 **/

export { default } from './Input';
