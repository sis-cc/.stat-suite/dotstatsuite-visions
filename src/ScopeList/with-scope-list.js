/* eslint react/prop-types: 0 */
import React from 'react';
import * as R from 'ramda';
import { reduceChildren } from '../utils';

const range = tab => {
  if (R.isEmpty(tab)) return tab;
  return R.range(...tab);
};

const getCounter = (items, displayAccessor, tagAccessor) => {
  const count = R.pipe(R.filter(R.prop('isSelected')), R.length)(items);
  const total = R.pipe(
    R.when(R.always(R.is(Function, displayAccessor)), R.filter(displayAccessor)),
    R.length,
  )(items);
  if (R.is(Function, tagAccessor)) {
    return tagAccessor(count, total);
  }
  return `${count}/${total}`;
};

export const getIsBlank = (items, displayAccessor, limitDisplay) => {
  if (R.not(R.is(Function)(displayAccessor))) return R.pipe(R.length, R.gte(limitDisplay))(items);
  return R.pipe(R.filter(displayAccessor), R.length, R.gte(limitDisplay))(items);
};

//----------------------------------------------------------------------------------DisplayOrDisable
const addDisableProps = displayAccessor => list => {
  if (R.not(R.is(Function)(displayAccessor))) return list;
  return R.reduce((acc, item) => {
    return R.pipe(
      R.when(R.pipe(displayAccessor, Boolean, R.not), R.assoc('isDisabled', true)),
      R.ifElse(
        R.both(R.prop('isDisabled'), R.complement(R.prop)('hasChild')),
        R.always(acc),
        R.flip(R.append)(acc),
      ),
    )(item);
  }, [])(list);
};

const indexByParentId = R.indexBy(R.prop('parentId'));
const indexByParentIdWithHasData = displayAccessor =>
  R.indexBy(R.ifElse(displayAccessor, R.prop('parentId'), R.always(undefined)));
export const getItemsIndexedByParentId = displayAccessor => items => {
  if (R.not(R.is(Function)(displayAccessor))) return indexByParentId(items);
  return R.converge(R.mergeRight, [indexByParentId, indexByParentIdWithHasData(displayAccessor)])(
    items,
  );
};

//-----------------------------------------------------------------------------------------Spotlight
const getSpotlightedList = ({ topElementProps, term = '', items, itemsById, noPath, hasPath }) =>
  topElementProps.defaultSpotlight.engine({
    ...topElementProps.defaultSpotlight,
    term,
    items,
    itemsById,
    noPath,
    hasPath,
  });

export const getPlaceholder = ({
  placeholder = '',
  mainPlaceholder,
  secondaryPlaceholder,
  currentItem,
}) => {
  if (R.and(R.isNil(mainPlaceholder), R.isNil(secondaryPlaceholder))) return placeholder;
  if (R.isNil(currentItem)) return mainPlaceholder;
  return secondaryPlaceholder;
};
//----------------------------------------------------------------------------------------------List
export const withHasChild = itemsByParentId => item =>
  R.assoc(
    'hasChild',
    R.pipe(R.prop(item.hierarchicalId || item.id), R.isNil, R.not)(itemsByParentId),
  )(item);
export const getCurrentList = ({ currentItemId, itemsByParentId, items }) =>
  R.reduce((acc, item) => {
    if (R.equals(currentItemId, item.parentId)) {
      return [...acc, withHasChild(itemsByParentId)(item)];
    }
    return acc;
  }, [])(items);
export const getMainList = R.filter(R.pipe(R.prop('parentId'), R.isNil));
export const getList = ({
  items,
  itemsByParentId,
  itemsById,
  topElementProps,
  currentItem,
  term,
  hasPath,
  displayAccessor,
}) => {
  const currentItemId = currentItem ? currentItem.hierarchicalId || currentItem.id : null;
  if (R.or(R.isEmpty(term), R.isNil(R.prop('defaultSpotlight')(topElementProps)))) {
    const itemsByParentIdDisplayed = getItemsIndexedByParentId(displayAccessor)(items);
    if (R.isNil(currentItemId))
      return R.pipe(getMainList, R.map(item => withHasChild(itemsByParentIdDisplayed)(item)))(
        items,
      );
    return getCurrentList({ currentItemId, itemsByParentId: itemsByParentIdDisplayed, items });
  }
  if (R.isNil(currentItemId))
    return getSpotlightedList({ topElementProps, term, items, itemsById, hasPath });
  return getSpotlightedList({
    topElementProps,
    term,
    items: getCurrentList({ currentItemId, itemsByParentId, items }),
    noPath: true,
    hasPath,
  });
};
export const getScopeList = ({
  isActivePanel,
  displayAccessor,
  items,
  itemsByParentId,
  itemsById,
  topElementProps,
  currentItem,
  term,
  hasPath,
}) => {
  if (R.not(isActivePanel)) return [];
  return R.pipe(getList, addDisableProps(displayAccessor))({
    items,
    itemsByParentId,
    itemsById,
    topElementProps,
    currentItem,
    term,
    hasPath,
    displayAccessor,
  });
};
//----------------------------------------------------------------------------------------------Node
export const getEnhancedNode = (currentItem, items, itemsById, hasPath) => {
  if (R.isNil(currentItem)) return currentItem;
  const id = R.prop('id')(currentItem);
  const item = R.find(R.propEq('id', id))(items);
  const path = R.ifElse(
    R.both(R.always(hasPath), R.pipe(R.propOr([], 'path'), R.head, R.is(Object))),
    R.propOr([], 'path'),
    R.always(reduceChildren(currentItem, itemsById)),
  )(item);
  return R.append(currentItem, path);
};

const getCurrentItem = ({ isActivePanel, currentItem, items, itemsById, hasPath }) => {
  if (R.not(isActivePanel)) return null;
  return getEnhancedNode(currentItem, items, itemsById, hasPath);
};
//---------------------------------------------------------------------------------------------Multi
export const getItemsIds = R.curry((disableAccessor, { isSelected }, items) => {
  return R.reduce((acc, item) => {
    if (disableAccessor(item)) return acc;
    if (!R.prop('isSelected', item) === isSelected) return acc;
    return R.append(R.prop('id', item), acc);
  }, [])(items);
});

const updateIndexSelected = index =>
  R.ifElse(R.isEmpty, R.always([index, index]), R.update(1, index));

export const getItemsGroupByLevel = disableAccessor => itemsById =>
  R.pipe(
    R.reduce((acc, item) => {
      if (disableAccessor(item)) return acc;
      return R.append(R.assoc('level', reduceChildren(item, itemsById, R.add(1), 0), item), acc);
    }, []),
    R.reduceBy(
      (acc, { id, isSelected, ...rest }) => R.append({ ...rest, id, isSelected }, acc),
      [],
      R.prop('level'),
    ),
  );

export const getIsAllSelected = disableAccessor =>
  R.pipe(R.reject(disableAccessor), R.all(R.prop('isSelected')));
export const setAllTo = list =>
  R.pipe(R.map(index => R.nth(index, list)), R.head, R.prop('isSelected'));

const setPreviewSelectedList = R.curry((indexes, setValue, list) =>
  R.reduce((acc, index) =>
    R.over(R.lensIndex(index), R.over(R.lensProp('isSelected'), setValue))(acc),
  )(list, indexes),
);

const getRangeIndexes = R.pipe(R.sort((a, b) => a - b), R.over(R.lensIndex(1), R.add(1)), range);

export const setSelection = ({ isBulkSelection, isDiscrete, items = [] }, list) => {
  const [indexes = [], applyToSelected = R.identity] = items;
  if (R.or(R.isEmpty(indexes), isBulkSelection)) return list;
  if (isDiscrete) {
    return setPreviewSelectedList(indexes, R.not)(list);
  }
  const isSelected = R.pipe(setAllTo(list), applyToSelected)(indexes);
  return setPreviewSelectedList(getRangeIndexes(indexes), R.always(isSelected))(list);
};

const getPropByIndex = R.curry((fn, indexes, list) =>
  R.map(R.pipe(R.nth(R.__, list), fn))(indexes),
);

const allEquals = list => R.all(R.equals(R.head(list)))(list);

//-----------------------------------------------------------------------------------------------Hoc
export const withScopeList = Component =>
  class extends React.Component {
    state = {
      term: R.propOr('', 'term')(this.props),
      levels: [],
      shiftSelection: [],
      ctrlSelection: [],
      mouseSelection: [],
      isMousePreSelection: false,
      applyToShiftSelected: R.identity,
    };

    static getDerivedStateFromProps = (props, state) => ({
      ...state,
      itemsById: R.indexBy(R.prop('id'), props.items),
      itemsByHierarchicalId: R.indexBy(item => item.hierarchicalId || item.id, props.items),
      itemsByParentId: getItemsIndexedByParentId(props.displayAccessor)(props.items), // only use for hasChild, same keys are overide
    });

    changeList = id =>
      this.setState({ currentItem: R.prop(id)(this.state.itemsByHierarchicalId), term: '' });

    changeSelection = panelId => id => event => {
      event.preventDefault();
      if (R.is(Function, this.props.changeSelection)) {
        return this.props.changeSelection(panelId, [id]);
      }
    };

    changeSpotlight = spotlight => this.setState({ term: R.propOr('', 'term')(spotlight) });

    //---------------------------------------------------------------------------------bulkSelection
    changeMutliSelection = panelId => ids => event => {
      event.preventDefault();
      if (R.isEmpty(ids)) return;
      if (R.is(Function, this.props.changeSelection)) {
        return this.props.changeSelection(panelId, ids);
      }
      return null;
    };

    changeLevelSelection = (panelId, disableAccessor) => itemsGroupByLevel => (
      level,
      isSelected,
    ) => event => {
      event.preventDefault();
      if (R.is(Function, this.props.changeSelection)) {
        return this.props.changeSelection(
          panelId,
          R.pipe(R.prop(level), items => getItemsIds(disableAccessor)({ isSelected }, items))(
            itemsGroupByLevel,
          ),
        );
      }
      return;
    };

    changeBulkPreSelections = (id, index) => event => {
      event.preventDefault();
      if (event.shiftKey && !R.isEmpty(this.state.shiftSelection)) {
        return this.setState({
          shiftSelection: updateIndexSelected(index)(this.state.shiftSelection),
        });
      }
      if (event.shiftKey && R.isEmpty(this.state.shiftSelection)) {
        this.setState({
          shiftSelection: [index, index],
          applyToShiftSelected: R.not,
        });
      }
      if (event.ctrlKey) {
        return this.setState({
          ctrlSelection: R.ifElse(R.includes(index), R.without([index]), R.append(index))(
            this.state.ctrlSelection,
          ),
        });
      }
    };

    changeMultiSelectionKeyUp = disableAccessor => ({ list, id }) => e => {
      const shiftSelection = this.state.shiftSelection;
      const ctrlSelection = this.state.ctrlSelection;

      // e.shiftKey doesn't work
      if (R.and(e.key === 'Shift', R.not(R.isEmpty(shiftSelection)))) {
        if (allEquals(shiftSelection)) return;
        const items = R.pipe(getPropByIndex(R.identity), R.reject(disableAccessor))(
          getRangeIndexes(shiftSelection),
          list,
        );
        const indexFirstElement = R.head(shiftSelection);
        this.changeMutliSelection(id)(
          getItemsIds(disableAccessor)({
            isSelected: !R.pipe(
              R.nth(indexFirstElement),
              R.prop('isSelected'),
              this.state.applyToShiftSelected,
            )(list),
          })(items),
        )(e);
        return this.setState({
          shiftSelection: [R.last(shiftSelection), R.last(shiftSelection)],
          applyToShiftSelected: R.identity,
        });
      }
      // e.ctrlKey doesn't work
      if (R.and(e.key === 'Control', R.not(R.isEmpty(ctrlSelection)))) {
        this.changeMutliSelection(id)(getPropByIndex(R.prop('id'), ctrlSelection, list))(e);
        return this.setState({
          ctrlSelection: [],
          shiftSelection: [R.last(ctrlSelection), R.last(ctrlSelection)],
        });
      }
      return;
    };

    changeMouseSelection = disableAccessor => ({ list, id }) => (index, mouseAction) => e => {
      if (R.or(e.ctrlKey, e.shiftKey)) return this.setState({ mouseSelection: [] });
      const mouseSelection = this.state.mouseSelection;
      const isMousePreSelection = this.state.isMousePreSelection;
      if (R.prop('isDown')(mouseAction)) {
        return this.setState({
          mouseSelection: updateIndexSelected(index)(mouseSelection),
          isMousePreSelection: true,
        });
      }
      // if (R.isEmpty(mouseSelection)) return;
      if (R.and(R.prop('isOver')(mouseAction), isMousePreSelection)) {
        return this.setState({
          mouseSelection: updateIndexSelected(index)(mouseSelection),
        });
      }
      if (R.prop('isUp')(mouseAction) && isMousePreSelection) {
        const items = R.pipe(getPropByIndex(R.identity), R.reject(disableAccessor))(
          getRangeIndexes(mouseSelection),
          list,
        );
        const isFirstElementSelected = !!R.prop('isSelected', R.head(items));
        this.changeMutliSelection(id)(
          getItemsIds(disableAccessor)({ isSelected: isFirstElementSelected })(items),
        )(e);
        return this.setState({
          mouseSelection: [],
          isMousePreSelection: false,
          shiftSelection: [R.last(mouseSelection), R.last(mouseSelection)],
        });
      }
    };

    render = () => {
      // "items", "itemsByParentId", "itemsById" are the same collection,
      // but only "items" is updated by the selection (on hold the selection): { isSelected: true/false }
      const {
        displayAccessor,
        disableAccessor = R.prop('isDisabled'),
        tagAccessor,
        items,
        topElementProps = {},
        activePanelId,
        id,
        hasPath,
        testId = 'scopeList',
        limitDisplay = 0,
        bulkSelection,
      } = this.props;
      const {
        currentItem,
        term,
        itemsByParentId,
        itemsById,
        itemsByHierarchicalId,
        shiftSelection,
        ctrlSelection,
        mouseSelection,
        applyToShiftSelected,
      } = this.state;
      const isActivePanel = R.equals(activePanelId, id);
      const list = getScopeList({
        items,
        itemsByParentId,
        itemsById,
        itemsByHierarchicalId,
        topElementProps,
        currentItem,
        term,
        hasPath,
        isActivePanel,
        displayAccessor,
        disableAccessor,
      });
      return (
        <Component
          {...this.props}
          disableAccessor={disableAccessor}
          itemsById={itemsById}
          items={items}
          isBlank={getIsBlank(items, displayAccessor, limitDisplay)}
          tagValue={getCounter(items, displayAccessor, tagAccessor)}
          changeList={this.changeList}
          currentItem={getCurrentItem({
            isActivePanel,
            currentItem,
            items,
            itemsById: itemsByHierarchicalId,
            hasPath,
          })}
          action={this.changeSpotlight}
          term={term}
          placeholder={getPlaceholder({
            ...topElementProps,
            currentItem,
          })}
          changeSelect={this.changeSelect}
          testId={testId}
          list={
            bulkSelection
              ? setSelection(
                  {
                    isBulkSelection: R.not(bulkSelection),
                    isDiscrete: R.not(R.isEmpty(ctrlSelection)),
                    items: R.find(([indexes]) => R.not(R.isEmpty(indexes)))([
                      [ctrlSelection],
                      [mouseSelection, R.not],
                      [shiftSelection, applyToShiftSelected],
                    ]),
                  },
                  list,
                )
              : list
          }
          changeSelection={bulkSelection ? this.changeBulkPreSelections : this.changeSelection(id)}
          changeMutliSelection={
            bulkSelection
              ? this.changeMutliSelection(id)(
                  getItemsIds(disableAccessor)(
                    { isSelected: getIsAllSelected(disableAccessor)(list) },
                    list,
                  ),
                )
              : null
          }
          changeLevelSelection={
            bulkSelection ? this.changeLevelSelection(id, disableAccessor) : null
          }
          isCurrentListAllSelected={bulkSelection ? getIsAllSelected(disableAccessor)(list) : null}
          changeMultiSelectionKeyUp={
            bulkSelection ? this.changeMultiSelectionKeyUp(disableAccessor)({ list, id }) : null
          }
          changeMouseSelection={
            bulkSelection ? this.changeMouseSelection(disableAccessor)({ list, id }) : null
          }
        />
      );
    };
  };
