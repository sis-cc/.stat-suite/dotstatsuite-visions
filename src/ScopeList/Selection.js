import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import cx from 'classnames';

const CustomListItem = ({ label, classes, action, isSelected }) => (
  <ListItem
    dense
    button
    onClick={e => action(e)}
    onKeyPress={e => {
      if (e.key === 'Enter') {
        action(e);
      }
    }}
    className={cx(classes.listItemContainer, {
      [classes.listItemSelected]: isSelected,
    })}
  >
    <ListItemText
      primary={label}
      primaryTypographyProps={{
        color: 'primary',
        variant: 'body2',
      }}
    />
  </ListItem>
);

CustomListItem.propTypes = {
  classes: PropTypes.object,
  label: PropTypes.string,
  action: PropTypes.func,
  isSelected: PropTypes.bool,
};

const Selection = ({
  id,
  labels,
  isOpen,
  classes,
  changeMutliSelection,
  changeLevelSelection,
  isCurrentListAllSelected,
  levels,
  itemsGroupByLevel,
  setIsLoading,
}) => (
  <Collapse in={isOpen}>
    <Paper elevation={2}>
      <div className={classes.selectionContainer}>
        <List>
          <ListItem dense>
            <Typography variant="body2">{R.prop('title')(labels)}</Typography>
          </ListItem>
          <CustomListItem
            classes={classes}
            label={R.prop('currentLevel')(labels)}
            action={e => {
              // move setLoading in the main file when with-scope-list will be deleted
              setIsLoading(true);
              changeMutliSelection(e);
            }}
            isSelected={isCurrentListAllSelected}
          />
          {R.not(R.isEmpty(levels)) && <Divider />}
          {R.map(([level, isLevelSelected]) => (
            <CustomListItem
              key={`bulkSelection-${id}-${level}`}
              classes={classes}
              label={`${R.prop('level')(labels)} ${R.add(1)(Number(level))}`}
              action={e => {
                // move setLoading in the main file when with-scope-list will be deleted
                setIsLoading(true);
                changeLevelSelection(itemsGroupByLevel)(level, isLevelSelected)(e);
              }}
              isSelected={isLevelSelected}
            />
          ))(levels)}
        </List>
      </div>
    </Paper>
  </Collapse>
);

Selection.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  labels: PropTypes.object,
  isOpen: PropTypes.bool,
  classes: PropTypes.object,
  changeMutliSelection: PropTypes.func,
  changeLevelSelection: PropTypes.func,
  isCurrentListAllSelected: PropTypes.bool,
  levels: PropTypes.array,
  itemsGroupByLevel: PropTypes.object,
  setIsLoading: PropTypes.func,
};

export default Selection;
