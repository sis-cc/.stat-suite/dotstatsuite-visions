import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import ListItem from '@mui/material/ListItem';
import Typography from '@mui/material/Typography';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import SvgIcon from '@mui/material/SvgIcon';
import cx from 'classnames';
import { Tooltip } from '../';

const checkBoxPath =
  'M19 5v14H5V5h14m0-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z';
const checkPath = 'M9 16.17L4.83 12l-1.42 1.41L9 19 21 7l-1.41-1.41z';

// ugly hack virtualize requires
const containerStyles = {
  top: R.add(16),
  left: R.dissoc('left'),
  width: R.always(36),
  // height: R.always(height), manage the size of the arrow box
};

const getPathLabel = labelRenderer =>
  R.pipe(
    R.map(labelRenderer),
    R.ifElse(R.pipe(R.length, R.flip(R.gte)(2)), R.pipe(R.last, R.concat('... > ')), R.last),
  );

const Item = ({
  classes,
  id,
  label,
  description = '',
  nodePath = [],
  isSelected,
  isDisabled,
  changeList,
  changeListDescription,
  ariaLabel,
  NavigateIcon,
  changeSelection,
  changeMouseSelection,
  hasChild,
  count,
  svgPath,
  testId,
  style,
  isRtl,
  accessibility,
  index,
  labelRenderer,
  HTMLRenderer,
  hierarchicalId,
  labels,
  depth = 0,
}) => {
  const [isTooltipShown, setIsTooltipShown] = React.useState(false);
  return (
    <ListItem
      disableGutters
      classes={{ disabled: classes.disabled }}
      className={cx(classes.listItemContainer, {
        [classes.disableSecondaryAction]: R.not(hasChild),
      })}
      disabled={isDisabled}
      onClick={isDisabled ? null : changeSelection(id, index)}
      onMouseDown={
        R.is(Function, changeMouseSelection) && R.not(isDisabled) && !isTooltipShown
          ? changeMouseSelection(index, { isDown: true })
          : null
      }
      onMouseUp={
        R.is(Function, changeMouseSelection) && R.not(isDisabled) && !isTooltipShown
          ? changeMouseSelection(null, { isUp: true })
          : null
      }
      onMouseOver={
        R.is(Function, changeMouseSelection) && R.not(isDisabled) && !isTooltipShown
          ? changeMouseSelection(index, { isOver: true })
          : null
      }
      onKeyPress={e => {
        if (e.key === 'Enter' && R.not(isDisabled)) {
          changeSelection(id, index)(e);
        }
      }}
      tabIndex={-1}
      style={style} // virtual
    >
      <ListItemText
        tabIndex={0}
        aria-label={isDisabled ? `${R.propOr('', 'disableItemLabel')(labels)} ${label}` : label}
        aria-pressed={isSelected}
        aria-disabled={isDisabled}
        primaryTypographyProps={{ noWrap: true, color: 'inherit' }}
        secondaryTypographyProps={{ color: 'inherit' }}
        classes={{ root: classes.listItem }}
        className={cx({
          [classes.accessibilityItemHover]: accessibility,
          [classes.listItemSelected]: R.and(isSelected, R.not(isDisabled)),
          [classes.listItemHover]: R.not(isDisabled),
        })}
        style={{ marginLeft: `${depth * 20}px` }}
        title={isDisabled ? `${R.propOr('', 'disableItemLabel')(labels)} ${label}` : label}
        primary={
          <div
            className={cx(classes.labelContainer, {
              [classes.rtlLabelContainer]: isRtl,
            })}
          >
            <SvgIcon
              color="primary"
              className={cx(classes.labelIcon, { [classes.disabledLabel]: isDisabled })}
            >
              <path d={R.and(isSelected, R.not(isDisabled)) ? checkPath : checkBoxPath} />
            </SvgIcon>
            {svgPath && (
              <SvgIcon
                color="primary"
                className={cx(classes.labelIcon, { [classes.disabledLabel]: isDisabled })}
              >
                <path d={svgPath} />
              </SvgIcon>
            )}
            <Tooltip
              title={
                R.or(R.isNil(HTMLRenderer), R.isEmpty(description)) ? (
                  description
                ) : (
                  <HTMLRenderer html={description} onClick={e => e.stopPropagation()} />
                )
              }
              enterDelay={400}
              tabIndex={0}
              aria-label={description}
              aria-hidden={false}
              placement="top"
              interactive={Boolean(HTMLRenderer)}
              onOpen={() => setIsTooltipShown(true)}
              onClose={() => setIsTooltipShown(false)}
            >
              <Typography
                data-testid={`${testId}_value_${id}`}
                color="inherit"
                variant="body2"
                className={cx(classes.label, {
                  [classes.disabledLabel]: isDisabled,
                  [classes.tooltipNotice]: !R.isEmpty(description),
                })}
              >
                {label}
              </Typography>
            </Tooltip>
            {R.not(R.isNil(count)) && (
              <Typography variant="body2" className={classes.count}>
                ({count})
              </Typography>
            )}
          </div>
        }
        secondary={
          <Typography
            title={R.join(isRtl ? ' < ' : ' > ')(R.map(labelRenderer)(nodePath))}
            variant="body2"
            className={cx(classes.ellipsis, classes.path)}
          >
            {getPathLabel(labelRenderer)(nodePath)}
          </Typography>
        }
      />
      {R.and(NavigateIcon, hasChild) && (
        <ListItemSecondaryAction
          onMouseEnter={
            R.is(Function, changeMouseSelection) ? changeMouseSelection(null, { isUp: true }) : null
          }
          style={{
            ...R.evolve(containerStyles, style),
            left: isRtl && 0,
            right: R.not(isRtl) && 0,
            outline: accessibility ? undefined : 'none',
          }}
          className={classes.containerArrow}
          tabIndex={0}
          onClick={() => changeList(hierarchicalId || id)}
          onKeyPress={e => {
            if (e.key === 'Enter') {
              changeList(id);
            }
          }}
        >
          <Tooltip
            title={changeListDescription || ''}
            tabIndex={0}
            aria-label={changeListDescription}
            aria-hidden={false}
            placement="top"
            variant="light"
          >
            <IconButton
              disableRipple
              disableFocusRipple
              className={classes.arrow}
              aria-label={ariaLabel}
              tabIndex={-1}
              size="small"
              color="inherit"
            >
              <NavigateIcon />
            </IconButton>
          </Tooltip>
        </ListItemSecondaryAction>
      )}
    </ListItem>
  );
};

Item.propTypes = {
  classes: PropTypes.object,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  label: PropTypes.string,
  description: PropTypes.string,
  parentId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  nodePath: PropTypes.array,
  ariaLabel: PropTypes.string,
  NavigateIcon: PropTypes.object,
  isSelected: PropTypes.bool,
  isDisabled: PropTypes.bool,
  hasChild: PropTypes.bool,
  isSection: PropTypes.bool,
  changeList: PropTypes.func,
  changeListDescription: PropTypes.string,
  changeSelection: PropTypes.func,
  changeMouseSelection: PropTypes.func,
  count: PropTypes.number,
  svgPath: PropTypes.string,
  style: PropTypes.object,
  isRtl: PropTypes.bool,
  testId: PropTypes.string,
  accessibility: PropTypes.bool,
  index: PropTypes.number,
  labelRenderer: PropTypes.func,
  HTMLRenderer: PropTypes.func,
  hierarchicalId: PropTypes.string,
  labels: PropTypes.object,
  depth: PropTypes.number,
};

export default Item;
