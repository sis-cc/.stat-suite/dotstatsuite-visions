import React, { Fragment } from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import NavigateBefore from '@mui/icons-material/NavigateBefore';
import NavigateNext from '@mui/icons-material/NavigateNext';
import cx from 'classnames';
import { Tooltip } from '../';

const Header = ({ classes, changeList, testId, isRtl, items, labels, labelRenderer }) => (
  <Typography className={classes.spaceHeader}>
    {R.addIndex(R.map)((headerProps, index) => {
      const { id, parentId, isSelected, _isIcon } = headerProps;
      if (_isIcon)
        // icon between labels
        return (
          <Fragment key={`scopeList-${index}`}>
            {isRtl ? (
              <NavigateBefore className={cx(classes.svg, classes.arrowHeader)} />
            ) : (
              <NavigateNext className={cx(classes.svg, classes.arrowHeader)} />
            )}
          </Fragment>
        );
      return (
        <Tooltip
          key={id}
          placement="top"
          variant="light"
          title={R.propOr('', 'backHelper')(labels)}
        >
          <Typography
            aria-label={`${R.propOr('', 'backHelper')(labels)}: ${labelRenderer(headerProps)}`}
            tabIndex={0}
            component="span"
            data-testid={`${testId}_nav_${id}`}
            color="primary"
            variant="body2"
            className={cx(classes.headerItem, classes.containerItem, {
              [classes.listItemSelected]: isSelected,
            })}
            onClick={() => changeList(parentId)}
            onKeyPress={e => {
              if (e.key === 'Enter') {
                changeList(parentId);
              }
            }}
          >
            {labelRenderer(headerProps)}
          </Typography>
        </Tooltip>
      );
    })(R.pipe(R.intersperse({ _isIcon: true }), R.when(R.always(isRtl), R.reverse))(items))}
  </Typography>
);

Header.propTypes = {
  classes: PropTypes.object,
  changeList: PropTypes.func,
  isRtl: PropTypes.bool,
  testId: PropTypes.string,
  items: PropTypes.array,
  labels: PropTypes.object,
  labelRenderer: PropTypes.func,
};

export default Header;
