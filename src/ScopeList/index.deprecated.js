/**
 * Scope List allow a horizontal navigation in a tree
 * (open console to see selection onClick, onKeyPress)
 *
 * limitDisplay: default 0. by default the filter will be blank if items content n of in the list.
 *
 * displayAccessor: if displayAccessor is defined the list will be the items return by displayAccessor,
 * in other word if the prop defined get by displayAccessor is not there, then the scope list will ignore the item and the item will be completely hide
 *
 * disabledAccessor: this accessor allow you to allow disabled items. Contrary to displayAccessor the item will be show but not clikable.
 *
 * bulkselection: allow and show button to selection entire level or current view
 *
 * @memberOf VISIONS
 * @name ScopeList
 * @tag component
 * @api public
 * @props
 * ScopeList.propTypes = {
 *   disableAccessor: PropTypes.func,
 *   displayAccessor: PropTypes.func,
 *   testId: PropTypes.string,
 *   term: PropTypes.string,
 *   id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
 *   label: PropTypes.string,
 *   accessibility: PropTypes.bool,
 *   limitDisplay: PropTypes.number
 *   items: {
 *     id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
 *     label: PropTypes.string,
 *     parentId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *     isSelected: PropTypes.bool,
 *     isDisabled: PropTypes.bool,
 *     count: PropTypes.number,
 *     path: PropTypes.arrayOf(PropTypes.shape({
 *       id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
 *       label: PropTypes.string,
 *       parentId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *       isSelected: PropTypes.bool,
 *     })),
 *     [yourDisplayAccessorKey]: PropTypes.bool,
 *   },
 *   spotlight, c.f proptype spotlight
 *   mainPlaceholder: PropTypes.string,
 *   secondaryPlaceholder: PropTypes.string,
 *   activePanelId: PropTypes.string,,
 *   hasPath: PropTypes.bool,
 *   changeSelection: PropTypes.func,
 *   labels: {
 *     childrenNavigateDesc: PropTypes.string,
 *     navigateBefore: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     navigateNext: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     backHelper: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     disableItemLabel: PropTypes.string,
 *     selection: {
 *       title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *       currentLevel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *       level: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     }
 *   },
 *   expansionPanelProps: PropTypes.object
 *   bulkSelection: PropTypes.bool
 *   labelRenderer: PropTypes.func,
 * };
 * @theme
 * // see ExpansionPanel too custom open and close scopelist
 * level: a level is the current tree level start from level 1
 * {
 *   palette: {
 *     primary: {
 *       main: // label color and background color of around arrow to see children. global override: outerPalette.primaryMain
 *     },
 *     common: {
 *       white: // color of arrow to see children
 *     },
 *     action: {
 *       hover: // background color of element and header element (from level 2) on hover or selected
 *       selected: // background color of focus element (accessibility must be set to true) or hover of selected element
 *     },
 *     grey: {
 *       100: // color of tough line (line displayed from level 2)
 *       600: // count label (after label element) and path label color (use search to see path)
 *       700: // disabled label
 *       800: // arrow color between header element (displayed from level 3). globale override: outerPalette.textLight
 *     }
 *   }
 * }
 * @testId
 * @demoReady
 * root: [testId]
 * value: [testId]_value_[props.items.id]
 */

export { default } from './ScopeList';
