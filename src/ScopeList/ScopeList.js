import React, { Fragment, useRef, useEffect, useState } from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import AutoSizer from 'react-virtualized/dist/commonjs/AutoSizer';
import VirtualizedList from 'react-virtualized/dist/commonjs/List';
import { useTheme } from '@mui/material';
import List from '@mui/material/List';
import Container from '@mui/material/Container';
import NavigateBefore from '@mui/icons-material/NavigateBefore';
import NavigateNext from '@mui/icons-material/NavigateNext';
import HourglassEmptyIcon from '@mui/icons-material/HourglassEmpty';
import PlaylistAddCheckIcon from '@mui/icons-material/PlaylistAddCheck';
import Selection from './Selection';
import { withScopeList } from './with-scope-list';
import Header from './Header';
import Item from './Item';
import { ExpansionPanel, Tag as InternalTag, VerticalButton, Tooltip } from '../';
import { useStyles, height as rowHeight } from './styles';
import { withBlank, getIsRtl } from '../utils';
import { getItemsGroupByLevel } from './with-scope-list';

const getLevels = R.pipe(
  R.map(R.all(R.prop('isSelected'))),
  R.toPairs,
  R.when(R.pipe(R.length, R.equals(1)), R.always([])),
);

const ScopeList = props => {
  const {
    id,
    items,
    itemsById,
    list = [],
    currentItem,
    changeList,
    changeSelection,
    changeMutliSelection,
    changeLevelSelection,
    onChangeActivePanel,
    activePanelId,
    TopElementComponent,
    topElementProps,
    Tag,
    tagValue,
    testId,
    isBlank,
    labels,
    accessibility,
    expansionPanelProps = {},
    isCurrentListAllSelected,
    bulkSelection,
    changeMultiSelectionKeyUp,
    changeMouseSelection,
    labelRenderer = R.prop('label'),
    disableAccessor,
    HTMLRenderer,
  } = props;
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [scrollTop, setScrollTop] = useState(undefined);
  const ref = useRef();
  const classes = useStyles();
  const theme = useTheme();
  const [isLoading, setIsLoading] = useState(false);
  const [levels, setLevels] = useState([]);
  const [itemGroupByLevel, setItemGroupByLevel] = useState({});
  const isRtl = getIsRtl(theme);
  const Chip = R.isNil(Tag) ? InternalTag : Tag;
  const selectionLabels = R.prop('selection')(labels);
  const rowCount = R.length(list);
  const maxHeight = R.pipe(
    R.length,
    R.multiply(rowHeight),
    R.ifElse(R.gt(250), R.add(10), R.always(250)),
  )(list);

  useEffect(() => {
    if (!isLoading) return;
    setTimeout(() => {
      const itemGroupByLevel = getItemsGroupByLevel(disableAccessor)(itemsById)(items);
      R.pipe(getLevels, setLevels)(itemGroupByLevel);
      setItemGroupByLevel(itemGroupByLevel);
      setIsLoading(false);
    }, 0);
  }, [isLoading, items, itemsById, disableAccessor]);

  useEffect(() => {
    const scrollEl = R.path(['children', 0, 'children', 0], ref.current);
    if (scrollEl) {
      setScrollTop(0);
    }
  }, [R.prop('id', R.last(currentItem || []))]);

  useEffect(() => {
    const current = ref.current;
    const handler = event => {
      if (R.or(event.ctrlKey, event.shiftKey)) {
        // ugly hack, to get div element created by react-virtuliase (get scroll height)
        // ReactVirtualized__Grid ReactVirtualized__List made the scrollbar
        const scrollEl = R.path(['children', 0, 'children', 0], current);
        const isBottom = R.gte(
          R.add(R.prop('scrollTop')(scrollEl), R.prop('clientHeight')(scrollEl)),
          R.prop('scrollHeight')(scrollEl),
        );
        const nextpos = (scrollTop || scrollEl.scrollTop) + event.deltaY / 2;
        if (R.or(event.deltaY < 0, event.deltaY > 0 && R.not(isBottom))) {
          setScrollTop(nextpos < 0 ? 0 : nextpos);
        }
        event.preventDefault();
      }
      if (R.not(event.ctrlKey) && R.not(event.shiftKey)) {
        setScrollTop(undefined); // should be undefined if the user want use the normal scroll
      }
    };
    if (scrollTop === 0) {
      setScrollTop(undefined);
    }
    if (R.isNil(current)) return;

    current.addEventListener('wheel', handler, { passive: false });
    return () => {
      current.removeEventListener('wheel', handler, { passive: false });
    };
  }, [scrollTop]);

  const handleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
    if (R.not(isMenuOpen)) {
      setIsLoading(true);
    }
  };

  return (
    <ExpansionPanel
      isOpen={R.equals(id, activePanelId)}
      id={id}
      testId={`${testId}`}
      label={labelRenderer(props)}
      onChangeActivePanel={onChangeActivePanel}
      overflow={true}
      topElementComponent={
        <Fragment>
          {TopElementComponent && (
            <Fragment>
              <Container disableGutters className={classes.spotlightContainer}>
                <TopElementComponent
                  className={{ container: classes.middle }}
                  {...topElementProps}
                  {...props}
                />
                {bulkSelection && (
                  <Tooltip
                    key={id}
                    placement="bottom"
                    variant="light"
                    title={R.prop('title')(selectionLabels)}
                  >
                    <VerticalButton
                      aria-label={R.prop('title')(selectionLabels)}
                      classes={{ root: classes.bulkSelectionIcon }}
                      onClick={handleMenu}
                      selected={isMenuOpen}
                    >
                      {isLoading ? (
                        <HourglassEmptyIcon size={24} color="primary" />
                      ) : (
                        <PlaylistAddCheckIcon color="primary" />
                      )}
                    </VerticalButton>
                  </Tooltip>
                )}
              </Container>
              {bulkSelection && (
                <Selection
                  id={id}
                  setIsLoading={setIsLoading}
                  isOpen={isMenuOpen}
                  classes={classes}
                  items={items}
                  levels={levels}
                  itemsGroupByLevel={itemGroupByLevel}
                  itemsById={itemsById}
                  labels={selectionLabels}
                  isCurrentListAllSelected={isCurrentListAllSelected}
                  changeMutliSelection={changeMutliSelection}
                  changeLevelSelection={changeLevelSelection}
                />
              )}
            </Fragment>
          )}
          {currentItem && (
            <div className={classes.containerHeader}>
              <Header
                testId={testId}
                classes={classes}
                isRtl={isRtl}
                changeList={changeList}
                accessibility={accessibility}
                items={currentItem}
                labels={labels}
                labelRenderer={labelRenderer}
              />
            </div>
          )}
        </Fragment>
      }
      tag={<Chip items={list}>{tagValue}</Chip>}
      isBlank={isBlank}
      {...expansionPanelProps}
    >
      <List
        disablePadding
        style={{ height: maxHeight }}
        onKeyUp={changeMultiSelectionKeyUp}
        onMouseLeave={
          R.is(Function, changeMouseSelection) ? changeMouseSelection(null, { isUp: true }) : null
        }
        ref={ref}
      >
        <AutoSizer>
          {({ height, width }) => (
            <VirtualizedList
              overscan={10}
              tabIndex={-1}
              height={height}
              width={width}
              scrollTop={scrollTop} // control scroll position with ctrl or shift key
              rowCount={rowCount}
              rowHeight={R.add(1)(rowHeight)} // 1 is space between elements
              rowRenderer={({ index, style }) => {
                //if (isScrolling) return <div style={style}>...</div>
                const item = R.nth(index, list);
                return (
                  <Item
                    HTMLRenderer={HTMLRenderer}
                    labelRenderer={labelRenderer}
                    index={index}
                    style={style}
                    testId={`${testId}_${id}`}
                    key={item.hierarchicalId || item.id}
                    classes={classes}
                    isRtl={isRtl}
                    changeList={changeList}
                    changeListDescription={labels.childrenNavigateDesc}
                    changeSelection={changeSelection}
                    changeMouseSelection={changeMouseSelection}
                    ariaLabel={R.prop('navigateNext')(labels)}
                    NavigateIcon={isRtl ? NavigateBefore : NavigateNext}
                    accessibility={accessibility}
                    labels={labels}
                    {...item}
                    isDisabled={disableAccessor(item)}
                    label={labelRenderer(item)}
                  />
                );
              }}
            />
          )}
        </AutoSizer>
      </List>
    </ExpansionPanel>
  );
};

ScopeList.propTypes = {
  testId: PropTypes.string,
  accessibility: PropTypes.bool,
  bulkSelection: PropTypes.bool,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  list: PropTypes.array,
  currentItem: PropTypes.array,
  classes: PropTypes.object,
  changeList: PropTypes.func,
  changeSelection: PropTypes.func,
  changeMutliSelection: PropTypes.func,
  changeLevelSelection: PropTypes.func,
  changeMultiSelectionKeyUp: PropTypes.func,
  changeMouseSelection: PropTypes.func,
  onChangeActivePanel: PropTypes.func,
  activePanelId: PropTypes.string,
  TopElementComponent: PropTypes.func,
  topElementProps: PropTypes.object,
  Tag: PropTypes.func,
  tagValue: PropTypes.node,
  isBlank: PropTypes.bool,
  isCurrentListAllSelected: PropTypes.bool,
  labels: PropTypes.shape({
    childrenNavigateDesc: PropTypes.string,
    navigateNext: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    backHelper: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    disableItemLabel: PropTypes.string,
    selection: PropTypes.shape({
      title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
      currentLevel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
      level: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    }),
  }),
  expansionPanelProps: PropTypes.object,
  labelRenderer: PropTypes.func,
  disableAccessor: PropTypes.func,
  items: PropTypes.array,
  itemsById: PropTypes.object,
  HTMLRenderer: PropTypes.func,
};

export default R.compose(
  withBlank(({ items = [], isBlank }) => ({ isBlank: R.or(R.isEmpty(items), isBlank) })),
  withScopeList,
)(ScopeList);
