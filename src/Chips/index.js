/**
 * GroupedChips, is like a small board of whatever you want to show. You can delete sub item, item with sub item or all items
 * DeleteAllChip, is a chip that deletes all items displayed in the board
 * reducingNbChips: default 15, allow a possibility to reduce many chips to one. You will see the numbers of items inside the chip
 *
 * @memberOf VISIONS
 * @name Chips
 * @tag component
 * @api public
 * @demoReady
 * GroupedChips.propTypes = {
 *  testId: PropTypes.string,
 *  itemProps: PropTypes.shape({
 *    id: PropTypes.string,
 *    label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *    isNotRemovable: PropTypes.bool,
 *    values: PropTypes.arrayOf(
 *      PropTypes.arrayOf(
 *        PropTypes.shape({
 *          id: PropTypes.string,
 *          label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *          isNotRemovable: PropTypes.bool,
 *        }),
 *      ),
 *    ),
 *  }),
 *  onDelete: PropTypes.func,
 *  labelRenderer: PropTypes.func,
 *  tooltipRenderer: PropTypes.func,
 *  ariaLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *  reducingNbChips: PropTypes.number,
 *  labels: PropTypes.shape({
 *    reducingChip: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *  }),
 * };
 * 
 * DeleteAllChip.propTypes = {
 * onDeleteAll: PropTypes.func,
 * ariaLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 * clearAllLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};
 * @theme
 * {
 *   palette: {
 *     primary: {
 *       main: // color of icon, text values and tooltip indicator*. global override: outerPalette.primaryMain
 *     },
 *     tertiary: {
 *       light: // background color of values
 *       dark: // background color hover and focus if values
 *     },
 *     grey: {
 *       100: // used if you set tertiary light to null
 *       300: // used if you set tertiary light to null
 *       700: // divider's color
 *     },
 *     text: {
 *       primary: // color of main chip label. globale override: outerPalette.textDark
 *     },
 *   },
 *   typography: {
 *     body2: // tooltip font
 *   }
 * }
 * // *tooltip indicator is the dotted line under a chip
 */

export { default } from './GroupedChips';
export { default as DeleteAllChip } from './DeleteChip';
