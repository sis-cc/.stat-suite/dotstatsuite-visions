/**
 * ShortUrls, it's a component to shorten your large URL
 *
 * @memberOf VISIONS
 * @name ShortUrls
 * @tag component
 * @api public
 * @props
 * ShortUrls.propTypes = {
 *   onClick: PropTypes.func,
 *   isLoading: PropTypes.bool,
 *   contentValue: PropTypes.string,
 *   labels: PropTypes.shape({
 *     generateUrl: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   }),
 * };
 * @demoReady
 */

export { default } from './ShortUrls';
