/**
 * Button use api of material ui https://material-ui.com/api/button/
 *
 * textTransform: 'none' | 'capitalize' | 'uppercase' | 'lowercase'
 *
 * @memberOf VISIONS
 * @name ToggleButton
 * @tag component
 * @api public
 * @props
 * ToggleButton.PropTypes = {
 *   tabindex: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *   testId: PropTypes.string,
 *   isOpen: PropTypes.bool,
 *   label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   toggle: PropTypes.func,
 *   textTransform: PropTypes.string,
 * };
 *
 * @theme
 * // set toggleButton mixins to null if you wish auto contrasted text
 * {
 *   palette: {
 *     button: {
 *       mainOpacity: // background color of button
 *     },
 *     highlight: {
 *       hl1: // background color hover and selected of button. globale override: outerPalette.highlight1
 *     }
 *     primary: {
 *       main: // used if mainOpacity is null. global override: outerPalette.primaryMain
 *       dark: // used if hl1 is null. globale override: outerPalette.primaryDark
 *     }
 *   },
 *   mixins: {
 *     toggleButton: {
 *       selected: {
 *         color: outerPalette.textDark
 *       },
 *       hover: {
 *         color: outerPalette.textDark
 *       }
 *     }
 *   }
 * }
 * @testId
 * root: [testId]
 */

export { default } from './ToggleButton';
