/**
 * PeriodPicker allow to choice a period regarding a frequency
 *
 * @memberOf VISIONS
 * @name PeriodPicker
 * @tag component
 * @api public
 * @props
 * Period.propTypes = {
 *   defaultFrequency: PropTypes.string,
 *   availableFrequencies: PropTypes.array,
 *   boundaries: PropTypes.array,
 *   changePeriod: PropTypes.func,
 *   changeFrequency: PropTypes.func,
 *   period: PropTypes.array,
 *   labels: PropTypes.object,
 *   periodDisabled: PropTypes.bool,
 *   frequencyDisabled: PropTypes.bool,
 * };
 * @theme
 * // Select (drop down list, See Select Component)
 * // no custom theme there
 * @demoReady
 */

export { default } from './PeriodPicker';
