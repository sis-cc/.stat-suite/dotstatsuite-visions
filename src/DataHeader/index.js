/**
 * DataHeader display four elements
 * Title with flags
 * Subtitles with flags
 * uprs and disclaimer
 *
 * @memberOf VISIONS
 * @name DataHeader
 * @tag component
 * @api public
 * @props
 * title: PropTypes.shape({
 *   label: PropTypes.string,
 *   flags: PropTypes.arrayOf(PropTypes.shape({
 *     header: PropTypes.string,
 *     label: PropTypes.string,
 *     sub: PropTypes.arrayOf(PropTypes.shape({
 *       label: PropTypes.string
 *     }))
 *   })),
 * }),
 * subtitle: PropTypes.arrayOf(PropTypes.shape({
 *   header: PropTypes.string,
 *   label: PropTypes.string,
 *   flags: PropTypes.arrayOf(PropTypes.shape({
 *     header: PropTypes.string,
 *     label: PropTypes.string,
 *     sub: PropTypes.arrayOf(PropTypes.shape({
 *       label: PropTypes.string
 *     }))
 *   }))
 * })),
 * uprs: PropTypes.shape({
 *   header: PropTypes.string,
 *   label: PropTypes.string,
 * }),
 * disclaimer: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 * isSticky: PropTypes.bool,
 * @theme
 * // tooltip (see Tooltip Component)
 * {
 *   palette: {
 *     highlight: {
 *       hl1: // warning icon color. globale override: outerPalette.highlight1
 *     },
 *     primary: {
 *       dark: // used if hl1 is null. globale override: outerPalette.primaryDark
 *     }
 *     grey: {
 *       600: // info icon color
 *     }
 *   },
 *   typography: {
 *     body1: {
 *       // asterisk icon
 *     }
 *   },
 *   mixins: {
 *     dataHeader: {
 *       root: {
 *         // font, color
 *       },
 *       title: {
 *         // font, color
 *       },
 *       subtitle: {
 *         // font, color
 *       },
 *       disclaimer: {
 *         // font, color
 *       },
 *       icon: {
 *         color: dot icon color
 *       },
 *     }
 *   }
 * }
 * @demoReady
 *
 */

export { default } from './DataHeader';
