/**
 * Logo is basic component for show your logo, you can add children if you want to add subtitle
 *
 * @memberOf VISIONS
 * @name Logo
 * @tag component
 * @api public
 * @props
 * Logo.propTypes = {
 *   logo: PropTypes.string,
 *   maxHeight: PropTypes.number,
 *   children: PropTypes.object,
 * };
 * @theme
 * // no custom theme there
 */

export { default } from './Logo';
