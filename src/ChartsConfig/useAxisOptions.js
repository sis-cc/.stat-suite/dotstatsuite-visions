import * as R from 'ramda';

const useAxisOptions = (properties, suffix) => {
  const keys = [`min${suffix}`, `max${suffix}`, `pivot${suffix}`, `step${suffix}`];

  return R.pipe(R.pick(keys), R.keys)(properties);
};

export default useAxisOptions;
