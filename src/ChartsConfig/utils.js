import * as R from 'ramda';

// adapters for new Select which has not the same API as react-select
export const extractMultipleValue = R.pipe(R.propOr([], 'value'), R.pluck('value'));

export const getValue = R.ifElse(R.is(String), R.identity, R.prop('value'));
