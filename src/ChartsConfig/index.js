/**
 * ChartsConfig, it's a specific component for the configuration of charts
 *
 * @memberOf VISIONS
 * @name ChartsConfig
 * @tag component
 * @api public
 * @demoReady
 * @props
 * ChartsConfig.propTypes = {
 *   labels: PropTypes.object,
 *   properties: PropTypes.shape({
 *     highlight: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onChange: PropTypes.func,
 *       options: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           value: PropTypes.string,
 *           label: PropTypes.string,
 *         }),
 *       ),
 *       value: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           value: PropTypes.string,
 *           label: PropTypes.string,
 *         }),
 *       ),
 *     }),
 *     baseline: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onChange: PropTypes.func,
 *       options: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           value: PropTypes.string,
 *           label: PropTypes.string,
 *         }),
 *       ),
 *       value: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           value: PropTypes.string,
 *           label: PropTypes.string,
 *         }),
 *       ),
 *     }),
 *     width: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *     height: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *     freqStep: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *     scatterDimension: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onChange: PropTypes.func,
 *       options: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           value: PropTypes.string,
 *           label: PropTypes.string,
 *         }),
 *       ),
 *       value: PropTypes.shape({
 *         value: PropTypes.string,
 *         label: PropTypes.string,
 *       }),
 *     }),
 *     scatterX: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onChange: PropTypes.func,
 *       options: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           value: PropTypes.string,
 *           label: PropTypes.string,
 *         }),
 *       ),
 *       value: PropTypes.shape({
 *         value: PropTypes.string,
 *         label: PropTypes.string,
 *       }),
 *     }),
 *     scatterY: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onChange: PropTypes.func,
 *       options: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           value: PropTypes.string,
 *           label: PropTypes.string,
 *         }),
 *       ),
 *       value: PropTypes.shape({
 *         value: PropTypes.string,
 *         label: PropTypes.string,
 *       }),
 *     }),
 *     symbolDimension: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onChange: PropTypes.func,
 *       options: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           value: PropTypes.string,
 *           label: PropTypes.string,
 *         }),
 *       ),
 *       value: PropTypes.shape({
 *         value: PropTypes.string,
 *         label: PropTypes.string,
 *       }),
 *     }),
 *     stackedDimension: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onChange: PropTypes.func,
 *       options: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           value: PropTypes.string,
 *           label: PropTypes.string,
 *         }),
 *       ),
 *       value: PropTypes.shape({
 *         value: PropTypes.string,
 *         label: PropTypes.string,
 *       }),
 *     }),
 *     stackedMode: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onChange: PropTypes.func,
 *       options: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           value: PropTypes.string,
 *         }),
 *       ),
 *       value: PropTypes.string,
 *     }),
 *     maxX: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *     minX: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *     pivotX: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *     stepX: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *     maxY: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *     minY: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *     pivotY: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *     stepY: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *     }),
 *   }),
 *   onSubmit: PropTypes.func,
 * };
 * @theme
 * // Select (drop down list, See Select Component)
 *
 * {
 *    palette: {
 *      secondary: {
 *        dark: // background color of "Axis"
 *      }
 *      grey: {
 *        600: // text color of labels "Axis"
 *      }
 *    },
 *    mixins: {
 *      chartsConfig: {
 *        root: {
 *          // font, color of the complete component except "Axis" part
 *        }
 *      }
 *    }
 * }
 **/

export { default } from './ChartsConfig';
