/**
 * Table to display SDMX data with html5 for accessibility.
 * @memberOf VISIONS
 * @name TableHtml5
 * @tag component
 * @api public
 * @demoReady
 * @props
 * TableHtml5.propTypes = {
 *   cells: PropTypes.object,
 *   headerData: PropTypes.array,
 *   sectionsData: PropTypes.array,
 *   cellHandler: PropTypes.func,
 *   activeCellHandler: PropTypes.func,
 *   activeCellIds: PropTypes.string,
 *   selectedSideCoord: PropTypes.object,
 * };
 * @theme
 * {
 *   palette: {
 *     primary: {
 *       main: // background color header and hover/active cell borders. global override: outerPalette.primaryMain
 *     },
 *     secondary: {
 *       light: // used is tertiary is null
 *       dark: // used is tertiary is null
 *     },
 *     tertiary: {
 *       light: // background color row header and row labels,
 *       dark: // background color section,
 *     },
 *     grey: {
 *       'A700': // label color section, label row header, cells label color. globale override: outerPalette.textDark
 *       200: // background color between row label and row values
 *       500: //cells borders
 *     },
 *   },
 *   typography: {
 *     body1: {
 *       // flag content
 *     }
 *   },
 *   mixins: {
 *     table: {
 *      cellHighlight: {
 *         backgroundColor: // background color of the cell on click
 *       },
 *      flag: { // see usecase #7 flags
 *         fontFamily: "'PT Sans Narrow', 'Helvetica Neue', Helvetica, Arial, sans-serif",
 *         fontSize: 12,
 *         color: outerPalette.textLight
 *       },
 *      dot: {
 *         fontsize: // dot separator between labels in section
 *       }
 *     }
 *   }
 * }
 */

export { default } from './TableHtml5';
export { default as Cell } from './cell';
