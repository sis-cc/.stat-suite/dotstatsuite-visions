/**
 * ExpansionPanel hide or show children
 *
 * @memberOf VISIONS
 * @name ExpansionPanel
 * @tag component
 * @api public
 * @props
 * ExpansionPanel.propTypes = {
 *   testId: PropTypes.string,
 *   id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *   topElementComponent: PropTypes.element,
 *   onChangeActivePanel: PropTypes.func,
 *   label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   tag: PropTypes.element,
 *   overflow: PropTypes.bool,
 *   isOpen: PropTypes.bool,
 *   isBlank: PropTypes.bool,
 *   maxHeight: PropTypes.bool,
 *   children: PropTypes.oneOfType([
 *     PropTypes.arrayOf(PropTypes.node),
 *     PropTypes.node
 *   ]),
 *   fullWidth: PropTypes.bool,
 *   isPinned: PropTypes.bool,
 *   moreFilters: PropTypes.bool,
 *   isNarrow: PropTypes.bool,
 *   isPopper: PropTypes.bool,
 *   popperLabels: PropTypes.shape({
 *    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *    ariaLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
 * }),
 * };
 * @theme
 * {
 *   palette: {
 *     primary: {
 *       main: // icon color open colapse/close colapse. global override: outerPalette.primaryMain
 *     },
 *     grey: {
 *       700: // divider color, pinned icon, label
 *     },
 *   },
 *   mixins: {
 *     expansionPanel: {
 *       title: {
 *         // color, font
 *       }
 *     }
 *   }
 * }
 * @testId
 * @demoReady
 * root: [testId]
 * panel: [testId]_panel
 */

export { default } from './ExpansionPanel';
