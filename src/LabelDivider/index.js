/**
 * LabelDivider is label between two divider.
 *
 * @memberOf VISIONS
 * @name LabelDivider
 * @tag component
 * @api public
 * @props
 * LabelDivider.propTypes = {
 *   label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   color: PropTypes.string,
 *   withMargin: PropTypes.bool,
 * };
 * @theme
 * {
 *   palette: {
 *     tertiary: {
 *       main: // text's color
 *     },
 *     common: {
 *       black: // use common black if tertiary is null
 *     }
 *   }
 *   mixins: {
 *     labelDivider: {
 *       root: // font, color...
 *     }
 *   }
 * }
 */

export { default } from './LabelDivider';
