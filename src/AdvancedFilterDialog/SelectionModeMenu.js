import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import HintIcon from '@mui/icons-material/EmojiObjects';
import { Button } from '..';

const SelectionModeMenu = ({
  isOpen,
  onSelect,
  options,
  selected,
  classes,
  isNarrow,
  hint,
  maxHeight,
}) => {
  if (!isOpen) {
    return null;
  }
  return (
    <div className={isNarrow ? classes.narrowSelectionMenu : classes.selectionMenu}>
      <Card style={{ maxHeight: maxHeight - 5, overflow: 'auto' }}>
        <CardContent className={classes.selectModeContainer}>
          {R.map(
            ({ value, label, img }) => (
              <Button
                className={classes.selectButton}
                selected={value === selected}
                key={value}
                onClick={() => onSelect(value)}
              >
                <Paper className={classes.selectModeItem} key={value}>
                  <Typography variant="body2">{label}</Typography>
                  {!R.isNil(img) && R.is(String, img) && <img src={img} alt={value} />}
                </Paper>
              </Button>
            ),
            options,
          )}
        </CardContent>
        <div className={classes.hint}>
          <HintIcon />
          <Typography variant="body2">{hint}</Typography>
        </div>
      </Card>
    </div>
  );
};

SelectionModeMenu.propTypes = {
  classes: PropTypes.object,
  hint: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  isNarrow: PropTypes.bool,
  isOpen: PropTypes.bool,
  onSelect: PropTypes.func,
  options: PropTypes.array,
  selected: PropTypes.string,
  maxHeight: PropTypes.number,
};

export default SelectionModeMenu;
