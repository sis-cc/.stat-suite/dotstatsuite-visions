/**
 *
 * @memberOf VISIONS
 * @name AdvancedFilterDialog
 * @tag component
 * @api public
 * @props
 * AdvancedFilterDialog.propTypes = {
 *   items: {
 *     id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
 *     label: PropTypes.string,
 *     parentId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *     isSelected: PropTypes.bool,
 *     isDisabled: PropTypes.bool,
 *     count: PropTypes.number,
 *     path: PropTypes.arrayOf(PropTypes.shape({
 *       id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
 *       label: PropTypes.string,
 *       parentId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *       isSelected: PropTypes.bool,
 *     })),
 *   },
 * }
 */

export { default } from './AdvancedFilterDialog';
