/**
 * UserRightForm allows to view and edit an user right
 * @memberOf VISIONS
 * @name UserRightForm
 * @tag component
 * @api public
 *
 * UserRightForm.propTypes = {
 *  onSubmit: PropTypes.func,
 *  onCancel: PropTypes.func,
 *  artefactTypes: PropTypes.arrayOf(
 *    PropTypes.shape({
 *      value: PropTypes.string,
 *      label: PropTypes.string,
 *    }),
 *  ),
 *  dataSpaces: PropTypes.arrayOf(
 *    PropTypes.shape({
 *      value: PropTypes.string,
 *      label: PropTypes.string,
 *    }),
 *  ),
 *  permissionTabs: PropTypes.object,
 *  user: PropTypes.shape({
 *    value: PropTypes.string,
 *    disabled: PropTypes.bool,
 *  }),
 *  dataspace: PropTypes.shape({
 *    value: PropTypes.string,
 *    disabled: PropTypes.bool,
 *  }),
 *  artefactType: PropTypes.shape({
 *    value: PropTypes.string,
 *    disabled: PropTypes.bool,
 *  }),
 *  maintenanceAgencyID: PropTypes.shape({
 *    value: PropTypes.string,
 *    disabled: PropTypes.bool,
 *  }),
 *  artefactID: PropTypes.shape({
 *    value: PropTypes.string,
 *    disabled: PropTypes.bool,
 *  }),
 *  artefactVersion: PropTypes.shape({
 *  value: PropTypes.string,
 *  disabled: PropTypes.bool,
 *  }),
 *  labels: PropTypes.shape({
 *    title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *    user: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *    dataspace: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *    artefactType: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *    maintenanceAgencyID: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *    artefactID: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *    artefactVersion: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *  }),
 *  };
 *  @demoReady
 **/
export { default } from './UserRightForm';
