/**
 * NoData component is a basic placeholder to use when there is nothing to display.
 *
 * @memberOf VISIONS
 * @name NoData
 * @tag component
 * @api public
 * @props
 * icon: PropTypes.element
 * message: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
 * @theme
 * // no custom theme there
 */

export { default } from './NoData';
