/**
 * SisccFooter render img and button for your website.
 *
 * @memberOf VISIONS
 * @name SisccFooter
 * @tag component
 * @api public
 * @props
 * SisccFooter.propTypes = {
 *   leftLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   rightLabel: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.element])),
 * };
 * @theme
 * {
 *   palette: {
 *     grey: {
 *       'A700': // labels color. globale override: outerPalette.textDark
 *       100: // background color
 *     }
 *   },
 *   typography: {
 *     fontWeightBold: // left label
 *   },
 * }
 **/

export { default } from './SisccFooter';
