/**
 * InputNumber you can use this input number between two label and with a popper
 *
 * @memberOf VISIONS
 * @name InputNumber
 * @tag component
 * @api public
 * @demoReady
 * @props
 * InputNumber.propTypes = {
 *   value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *   onChange: PropTypes.func,
 *   beforeLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   afterLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   popperLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   textFieldProps: PropTypes.object,
 * };
 * @theme
 * // no custom theme there
 **/

export { default } from './InputNumber';
