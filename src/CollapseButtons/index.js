/**
 * Collapse Buttons show one or many buttons with Children and Sub-Children
 * Buttons are drawn in row of three
 * Children are drawn also in row of three and the sub-children are drawn in line separated by comma
 * justify properties can take all variant define in "justifyContent" https://material-ui.com/api/grid/
 * | 'flex-start'
 * | 'center'
 * | 'flex-end'
 * | 'space-between'
 * | 'space-around'
 * | 'space-evenly'
 *
 * textTransform: 'none' | 'capitalize' | 'uppercase' | 'lowercase'
 *
 * to manage layout you have three properties
 * nbColumns: numbers of column you want to have
 * gridButtonsProps: mainly to manage breakpoints in the differents layout of buttons but you can change all the grid properties. (nbColumns is use with the "sm" breakpoint)
 * breakpoint: is a function with theme arg. Allow you to change the mobile view, (default theme => theme.breakpoints.down('sm'))
 *
 * @memberOf VISIONS
 * @name CollapseButtons
 * @tag component
 * @api public
 * @props
 * CollapseButtons.propTypes = {
 *   testId: PropTypes.string,
 *   selectedItemId: PropTypes.string,
 *   items: PropTypes.arrayOf(
 *     PropTypes.shape({
 *       id: PropTypes.string.isRequired,
 *       label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *       svgPath: PropTypes.string,
 *       values: PropTypes.arrayOf(
 *         PropTypes.shape({
 *           id: PropTypes.string.isRequired,
 *           label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *           subtopics: PropTypes.arrayOf(
 *             PropTypes.oneOfType([
 *               PropTypes.shape({
 *                 id: PropTypes.string.isRequired,
 *                 label: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
 *               }),
 *               PropTypes.string,
 *             ]),
 *           ),
 *         }),
 *       ),
 *     }),
 *   ),
 *   action: PropTypes.func,
 *   nbColumns: PropTypes.number,
 *   isSecondLevelClikable: PropTypes.bool,
 *   breakpoint: PropTypes.func,
 *   gridButtonsProps: PropTypes.object,
 *   justify: PropTypes.string,
 *   textTransform: PropTypes.string,
 * };
 * @theme
 * // button (See ToggleButton Component)
 *
 * // item (an item is the list of labels that is shown after clicking on "Topic")
 * {
 *    palette: {
 *      primary: {
 *        main: // item main label and sub label's hover. global override: outerPalette.primaryMain
 *      }
 *      tertiary: {
 *        light: // hover background color items (e.g. Economie)
 *      },
 *      secondary: {
 *        light: // use secondary light if tertiary is null
 *      }
 *      grey: {
 *        600: // svg icon color
 *        800: // sub label color. globale override: outerPalette.textLight
 *      }
 *    },
 *    typography: {
 *      body2: // font main label
 *      body1: // font sub label
 *    }
 * }
 * @testId
 * root: [testId]_[props.items.id]
 * input: [testId]_[props.items.id]_value_[props.items.values.id]
 *
 * @demoReady
 */

export { default } from './CollapseButtons';
