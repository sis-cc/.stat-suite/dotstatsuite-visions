/**
 * Pagination
 *
 * @memberOf VISIONS
 * @name Pagination
 * @tag component
 * @api public
 * @props
 * Pagination.propTypes = {
 *   page: PropTypes.number,
 *   pages: PropTypes.number,
 *   onChange: PropTypes.func,
 *   onSubmit: PropTypes.func,
 *   labels: PropTypes.shape({
 *     page: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     of: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     startPage: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     previousPage: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     nextPage: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     endPage: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   }),
 * };
 * @theme
 * {
 *   typography: {
 *     fontSize: // input font size
 *   }
 * }
 **/

export { default } from './Pagination';
