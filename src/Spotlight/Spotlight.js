import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import * as R from 'ramda';
import ArrowForward from '@mui/icons-material/ArrowForward';
import { useTheme } from '@mui/material/styles';
import ArrowBack from '@mui/icons-material/ArrowBack';
import Search from '@mui/icons-material/Search';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';
import { Field } from './Fields';
import { useStyles } from './useStyles';
import { useSpotlight } from './useSpotlight';
import Cross from '../Icons/Cross';

const size = R.pipe(R.values, R.length);

const Spotlight = ({
  placeholder,
  hasClearAll,
  hasCommit,
  withBorder,
  defaultSpotlight,
  term,
  action,
  fullWidth,
  className,
  withAutoFocus = false,
  testId = 'spotlight',
}) => {
  const {
    value,
    spotlight,
    isFocus,
    onKeyPress,
    onClearTerm,
    onChangeTerm,
    onChangeField,
    onCommitTerm,
    setFocus,
  } = useSpotlight(hasCommit, term, defaultSpotlight, action);
  const classes = useStyles();
  const theme = useTheme();
  const isRtl = R.equals('rtl', R.prop('direction', theme));

  return (
    <div
      data-testid={testId}
      className={cx(R.prop('container', className), {
        [classes.container]: withBorder,
        [classes.containerFocus]: isFocus,
        [classes.fullWidth]: fullWidth,
        [classes.defaultWidth]: !fullWidth,
      })}
    >
      <Paper className={classes.root} elevation={0}>
        <IconButton className={classes.leftIcon} disabled aria-label="search" size="large">
          <Search />
        </IconButton>
        <InputBase
          inputProps={{
            'aria-label': placeholder,
            'data-testid': `${testId}_input`,
          }}
          className={classes.input}
          sx={{
            '& input::placeholder': {
              color: theme.palette.grey[700],
              opacity: 1,
            },
          }}
          classes={{ root: classes.inputBase, input: classes.inputBaseMargin }}
          placeholder={placeholder}
          onKeyDown={onKeyPress}
          value={value}
          onChange={onChangeTerm}
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          // eslint-disable-next-line jsx-a11y/no-autofocus
          autoFocus={withAutoFocus}
        />
        {hasClearAll && !R.isEmpty(value) ? (
          <IconButton
            aria-label="Clear"
            onClick={onClearTerm}
            className={cx(classes.rightIcon, classes.clear)}
            size="large"
          >
            <Cross />
          </IconButton>
        ) : null}
        {R.gt(size(R.prop('fields', spotlight)), 1) ? (
          <Fragment>
            <Divider className={classes.divider} />
            <Field
              spotlight={spotlight}
              onChangeField={onChangeField}
              isRtl={isRtl}
              term={value}
              classes={classes}
            />
          </Fragment>
        ) : null}
        {hasCommit ? (
          <Fragment>
            <Divider className={classes.divider} />
            <IconButton
              disabled={R.isEmpty(value)}
              color="primary"
              className={classes.rightIcon}
              aria-label="commit"
              onClick={onCommitTerm}
              size="large"
            >
              {isRtl ? <ArrowBack /> : <ArrowForward />}
            </IconButton>
          </Fragment>
        ) : null}
      </Paper>
    </div>
  );
};

Spotlight.propTypes = {
  term: PropTypes.string,
  testId: PropTypes.string,
  placeholder: PropTypes.string,
  defaultSpotlight: PropTypes.shape({
    fields: PropTypes.object,
    searchLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    FieldLabelRenderer: PropTypes.func,
  }),
  action: PropTypes.func,
  hasClearAll: PropTypes.bool,
  hasCommit: PropTypes.bool,
  withBorder: PropTypes.bool,
  fullWidth: PropTypes.bool,
  classes: PropTypes.object,
  className: PropTypes.object,
  withAutoFocus: PropTypes.bool,
};

export default Spotlight;
