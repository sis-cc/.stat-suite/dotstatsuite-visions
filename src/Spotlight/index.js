/**
 * Spotlight is an input for search something
 *
 * We have many options for design the component as you wish
 * hasClearAll: show cross button for delete your text
 * hasCommit: show arrow button for commit your text (You could press "enter" for commit)
 * withBorder: add padding around the spotlight
 * fullWidth: input take 100% of the width
 *
 * spotlightOptions: show on right side a drop down list where you can add some options
 *
 * open devTool to see actions
 *
 *
 * @memberOf VISIONS
 * @name Spotlight
 * @tag component
 * @api public
 * @props
 * Spotlight.propTypes = {
 *   term: PropTypes.string,
 *   testId: PropTypes.string,
 *   placeholder: PropTypes.string,
 *   defaultSpotlight: PropTypes.shape({
 *     fields: PropTypes.object,
 *     searchLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     FieldLabelRenderer: PropTypes.func,
 *   }),
 *   action: PropTypes.func,
 *   hasClearAll: PropTypes.bool,
 *   hasCommit: PropTypes.bool,
 *   withBorder: PropTypes.bool,
 *   fullWidth: PropTypes.bool,
 *   classes: PropTypes.object,
 *   className: PropTypes.object,
 * };
 * @theme
 * {
 *   palette: {
 *     button: {
 *       mainOpacity: // background color around the spotlight (withBorder)
 *     },
 *     highlight: {
 *       hl1: // background color around the spotlight on focus. globale override: outerPalette.highlight1
 *     },
 *     primary: {
 *      main: // color of icons on the right. global override: outerPalette.primaryMain
 *      light: // used if hl1 is null. globale override: outerPalette.primaryLight
 *      dark: // used if mainOpacity is null. globale override: outerPalette.primaryDark
 *     },
 *     grey: {
 *       400: // input border
 *     }
 *   },
 *   typography: {
 *     body2: {
 *       // font, color, size of input label
 *     },
 *   },
 *   mixins: {
 *     spotlight: {
 *       focus: {
 *         outlineColor: outerPalette.highlight1
 *         outlineWidth: 1,
 *         outlineStyle: 'auto',
 *       }
 *     },
 *   }
 * }
 * @testId
 * root: [testId]
 * input: [testId]_input
 */

export { default } from './Spotlight';
