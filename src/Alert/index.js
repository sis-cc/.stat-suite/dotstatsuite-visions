/**
 * An alert displays a short, important message in a way that attracts the user's attention without interrupting the user's task.
 * see more
 *
 * https://material-ui.com/components/alert/
 *
 * @memberOf VISIONS
 * @name Alert
 * @tag component
 * @api private
 */

export { default } from './Alert';
