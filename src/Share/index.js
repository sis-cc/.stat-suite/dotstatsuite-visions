/**
 * Share, it's a component to prepare your type of share
 *
 * @memberOf VISIONS
 * @name Share
 * @tag component
 * @api public
 * @props
 * Share.propTypes = {
 *   changeIsMessageOpen: PropTypes.func,
 *   changeMode: PropTypes.func,
 *   changeMail: PropTypes.func,
 *   share: PropTypes.func,
 *   hasError: PropTypes.bool,
 *   isMessageOpen: PropTypes.bool,
 *   isSharing: PropTypes.bool,
 *   mail: PropTypes.string,
 *   mode: PropTypes.string,
 *   modes: PropTypes.arrayOf(
 *     PropTypes.shape({
 *       label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *       value: PropTypes.string,
 *       warningMessage: PropTypes.string,
 *     }),
 *   ),
 *   labels: PropTypes.shape({
 *     title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     disclaimer: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     submit: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     email: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     successTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     successMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     errorTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     errorMessage: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   }),
 * };
 * @theme
 * {
 *   palette: {
 *     secondary: {
 *       dark: // background color of share mode
 *     },
 *     tertiary: {
 *       dark: // background color of disclaimer
 *     },
 *     grey: {
 *       100: // used if tertiary is null
 *     }
 *     divider: // divider color
 *   },
 *   typography: {
 *     body2: // font, color of mode's label
 *   },
 *   mixins: {
 *     share: {
 *       tile: // font, color...
 *       warning: // font, color, height...
 *     }
 *   }
 * }
 * @demoReady
 */

export { default } from './Share';
