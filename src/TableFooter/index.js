/**
 * TableFooter component display Two elements
 * One left side, with tooltip and the right side it diplays link and img
 *
 * @memberOf VISIONS
 * @name TableFooter
 * @tag component
 * @api public
 * @props
 * TableFooter.propTypes = {
 *   tooltip: PropTypes.shape({
 *     link: PropTypes.string,
 *     linkLabel: PropTypes.string,
 *     label: PropTypes.string,
 *   }),
 *   label: PropTypes.string,
 *   logo: PropTypes.string,
 *   link: PropTypes.string,
 *   linkLabel: PropTypes.string,
 *   height: PropTypes.number,
 *   classes: PropTypes.object,
 * };
 * @theme
 * {
 *   palette: {
 *     grey: {
 *       600: // color of labels
 *     }
 *   }
 * }
 */

export { default } from './TableFooter';
