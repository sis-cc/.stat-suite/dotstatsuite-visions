/**
 * VerticalButton Buttons allow users to take actions, and make choices, with a single tap
 * isSelected prop has added regarding Button of material UI
 * sizeLoading by default 18px
 *
 * @memberOf VISIONS
 * @name VerticalButton
 * @tag component
 * @api public
 * @props
 * VerticalButton.propTypes = {
 *   children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
 *   classes: PropTypes.shape({
 *     root: PropTypes.string,
 *     startIcon: PropTypes.string,
 *     label: PropTypes.string,
 *   }),
 *   startIcon: PropTypes.node,
 *   selected: PropTypes.bool,
 *   loading: PropTypes.bool,
 *   sizeLoading: PropTypes.number,
 * };
 * @theme
 * // no custom theme there
 **/

export { default } from './VerticalButton';
