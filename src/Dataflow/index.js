/**
 * Without onDownload the button download will disappear
 *
 * @demoReady
 * @memberOf VISIONS
 * @name Dataflow
 * @tag component
 * @api public
 * @props
 * Dataflow.propTypes = {
 *   id: PropTypes.string,
 *   testId: PropTypes.string,
 *   title: PropTypes.string,
 *   body: PropTypes.shape({
 *     description: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     highlightDescription: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   }),
 *   url: PropTypes.string,
 *   handleUrl: PropTypes.func,
 *   label: PropTypes.string,
 *   labels: PropTypes.shape({
 *     source: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     lastUpdated: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     readMore: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     note: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     date: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   }),
 *   dimensions: PropTypes.array,
 *   highlights: PropTypes.array,
 *   categories: PropTypes.array,
 *   HTMLRenderer: PropTypes.func,
 * };
 *
 * @theme
 * {
 *   palette: {
 *     primary: {
 *       light: // title hover. globale override: outerPalette.primaryLight
 *     },
 *     highlight: {
 *       hl3: // background color for highlighting words. globale override: outerPalette.highlight2
 *       hl2: // color to underline highlighted words. globale override: outerPalette.highlight3
 *     },
 *     success: {
 *       main: // used if hl3 is null
 *       dark: // used if hl2 is null
 *     },
 *     grey: {
 *       "A200": // fade content color
 *       700: // color of the divider and muted text*
 *       800: // open text description color. globale override: outerPalette.textLight
 *     }
 *   }
 * }
 * *muted text is when the description is not open
 * @testId
 * root: [testId]
 * title: [testId]_title
 * highlight: [testId]_highlight_[highlights[field]]
 */

export { default } from './Dataflow';
