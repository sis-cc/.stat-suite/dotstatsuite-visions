/**
 * CollapsibleTree is a component that show you a tree structure with a button to expand/collapse each node.
 * defaultDepthLevel is the maximum level that will be open by "collapse all" button value could be 0|1. set to 1 by default.
 *
 * @memberOf VISIONS
 * @name CollapsibleTree
 * @tag component
 * @api public
 * @props
 * CollapsibleTree.propTypes = {
 *  data: PropTypes.arrayOf(PropTypes.shape({
 *    id: PropTypes.string.isRequired,
 *    label: PropTypes.string,
 *    children: PropTypes.arrayOf(PropTypes.shape({
 *      id: PropTypes.string.isRequired,
 *      label: PropTypes.string,
 *      children: PropTypes.arrayOf(PropTypes.shape({
 *        id: PropTypes.string.isRequired,
 *        label: PropTypes.string,
 *        value: PropTypes.string,
 *      })),
 *    }))
 *  })),
 *  defaultDepthLevel: PropTypes.number,
 *  testId: PropTypes.string,
 *  labels: PropTypes.shape({
 *    collapseAll: PropTypes.string,
 *    expandAll: PropTypes.string,
 *    noValue: PropTypes.string,
 *  }),
 *};
 * @theme
 * {
 *   palette: {
 *     primary: {
 *       main: // color label button
 *     },
 *   }
 *   mixins: {
 *    CollapsibleTree: {
 *      title: {
 *        // font, color, of titles
 *      },
 * }
 * @demoReady
 *
 */

export { default } from './CollapsibleTree';
