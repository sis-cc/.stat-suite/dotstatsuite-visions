/**
 * ApiQueries allow users to copy paste a query.
 *
 * @memberOf VISIONS
 * @name ApiQueries
 * @tag component
 * @api public
 * @props
 * ApiQueries.propTypes = {
 *   labels: PropTypes.shape({
 *     title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     copy: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     copied: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     descriptions: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     notice: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     buttonsLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   }),
 *   delay: PropTypes.number,
 *   queries: PropTypes.arrayOf(PropTypes.shape({
 *     contents: PropTypes.arrayOf(PropTypes.shape({
 *       id: PropTypes.string,
 *       label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *       value: PropTypes.string,
 *     })),
 *     id: PropTypes.string,
 *     title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   }))
 * };
 * @theme
 * // buttons (See Button Component)
 *
 * // item (an item is the list of labels that is shown after clicking on "Topic")
 * {
 *    mixins: {
 *      apiQueries: {
 *        title: {
 *          // Customise your titles here
 *        }
 *      }
 *    }
 * }
 * @demoReady
 */

export { default } from './ApiQueries';
