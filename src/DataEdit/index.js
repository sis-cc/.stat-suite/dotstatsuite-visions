/**
 * DataEdit
 *
 * @demoReady
 * @memberOf VISIONS
 * @name DataEdit
 * @tag component
 * @api public
 * @props
 * DataEdit.propTypes = {
 *   labels: PropTypes.shape({
 *     title: PropTypes.string,
 *     subtitle: PropTypes.string,
 *     source: PropTypes.string,
 *     logo: PropTypes.string,
 *     copyright: PropTypes.string,
 *     reset: PropTypes.string,
 *   }),
 *   properties: PropTypes.shape({
 *     title: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *       onReset: PropTypes.func,
 *       value: PropTypes.string,
 *     }),
 *     subtitle: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *       onReset: PropTypes.func,
 *       value: PropTypes.string,
 *     }),
 *     source: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onSubmit: PropTypes.func,
 *       onReset: PropTypes.func,
 *       value: PropTypes.string,
 *     }),
 *     logo: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onChange: PropTypes.func,
 *       checked: PropTypes.bool,
 *     }),
 *     copyright: PropTypes.shape({
 *       id: PropTypes.string,
 *       isActive: PropTypes.bool,
 *       onChange: PropTypes.func,
 *       checked: PropTypes.bool,
 *     }),
 *   })
 * };
 * @theme
 * {
 *   palette: {
 *     primary: {
 *       main: // color of icons. global override: outerPalette.primaryMain
 *     },
 *   }
 *   mixins: {
 *     root: {
 *       // font, color of the title
 *     }
 *   }
 * }
 */

export { default } from './DataEdit';
