/**
 * Breadcrumbs allow users to make selections from a range of values.
 *
 * @memberOf VISIONS
 * @name Breadcrumbs
 * @tag component
 * @api public
 * @props
 * Breadcrumbs.propTypes = {
 *   items: PropTypes.shape({
 *     text: PropTypes.string.isRequired,
 *     action: PropTypes.func,
 *     isDisabled: PropTypes.bool,
 *   }),
 *   isRtl: PropTypes.bool,
 * };
 * @theme
 * {
 *    palette: {
 *      primary: {
 *        main: // label hover. global override: outerPalette.primaryMain
 *      },
 *      text: {
 *        disabled: // disabled label
 *      }
 *    }
 * }
 */

export { default } from './Breadcrumbs';
