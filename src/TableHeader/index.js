/**
 * TableHeader display until three elements
 * Title
 * Subtitles
 * and uprs
 *
 * @memberOf VISIONS
 * @name TableHeader
 * @tag component
 * @api public
 * @props
 * icon: PropTypes.element
 * message: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
 * @deprecated
 */

export { default } from './TableHeader';
