/**
 *
 * @memberOf VISIONS
 * @name HierarchicalFilter
 * @tag component
 * @api public
 * @props
 * HierarchicalFilter.propTypes = {
 *   items: {
 *     id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
 *     label: PropTypes.string,
 *     parentId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *     isSelected: PropTypes.bool,
 *     isDisabled: PropTypes.bool,
 *     count: PropTypes.number,
 *     path: PropTypes.arrayOf(PropTypes.shape({
 *       id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
 *       label: PropTypes.string,
 *       parentId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *       isSelected: PropTypes.bool,
 *     })),
 *   },
 * }
 */

export { default } from './HierarchicalFilter';
