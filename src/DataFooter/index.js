/**
 * DataFooter is the footer of the datavis component (tables, charts, maps).
 *
 * Responsive rules:
 * - no footer below 120px
 * - no legend below 370px
 * - no source below 370px, adaptative fontSize when displayed
 * - copyright, adaptative fontSize when displayed
 * - invariant logo, 21px (half min height of the footer)
 *
 * @memberOf VISIONS
 * @name DataFooter
 * @tag component
 * @api public
 * @props
 * logo: PropTypes.string.isRequired,
 * source: PropTypes.shape({
 *  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *  link: PropTypes.string,
 * }),
 * copyright: PropTypes.shape({
 *  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *  content: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 * }),
 * legend: PropTypes.element,
 * isSticky: PropTypes.bool,
 * @theme
 * // tooltip (see Tooltip Component)
 * @demoReady
 *
 */

export { default } from './DataFooter';
