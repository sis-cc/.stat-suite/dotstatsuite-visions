/**
 * TableLayout it's drag and drop, you can customise and see with the preview the layout of your table
 *
 * @memberOf VISIONS
 * @name TableLayout
 * @tag component
 * @api public
 * @props
 * const itemButtonProps= { id¹: { options, value, onChange }}
 * ¹ - first id in patern
 *
 * values is usefull for tablePreview only
 *
 * const patern = {
 *   id: PropTypes.string,
 *   label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   values: PropTypes.arrayOf(
 *     PropTypes.shape({
 *       id: PropTypes.string,
 *       label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     }),
 *   ),
 * };
 *
 * TableLayout.propTypes = {
 *   layout: PropTypes.shape({
 *     header: PropTypes.arrayOf(PropTypes.shape(patern)),
 *     sections: PropTypes.arrayOf(PropTypes.shape(patern)),
 *     rows: PropTypes.arrayOf(PropTypes.shape(patern)),
 *   }),
 *   itemRenderer: PropTypes.func,
 *   commit: PropTypes.func,
 *   noPreview: PropTypes.bool,
 *   itemButtonProps: PropTypes.object,
 *   labels: PropTypes.shape({
 *     commit: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     cancel: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     row: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     column: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     section: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     table: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     asc: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     desc: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     help: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     one: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   }),
 *   accessibility: PropTypes.bool,
 * };
 * @theme
 * {
 *   palette: {
 *     primary: {
 *       main: // header colors. global override: outerPalette.primaryMain
 *     },
 *     tertiary: {
 *       dark: // sections colors
 *       light: // rows colors
 *     },
 *     secondary: {
 *       dark: // used if tertiary dark is null
 *       light: // used if tertiary light is null
 *     }
 *     common: {
 *       white: // background color select asc/desc
 *     }
 *   },
 *   typography: {
 *     subtitle1: {
 *       // description "information DnD"
 *     }
 *   }
 * }
 **/

export { default } from './TableLayout';
