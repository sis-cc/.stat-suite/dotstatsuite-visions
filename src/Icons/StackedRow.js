import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export default props => (
  <SvgIcon viewBox="0 0 32 38" {...props}>
    <path d="M13 6 4 6 4 11 13 11 13 6ZM15 20 4 20 4 25 15 25 15 20ZM24 20 16 20 16 25 24 25 24 20ZM27 20 25 20 25 25 27 25 27 20ZM21 6 14 6 14 11 21 11 21 6ZM25 6 22 6 22 11 25 11 25 6ZM18 13 4 13 4 18 18 18 18 13ZM28 13 19 13 19 18 28 18 28 13ZM34 13 29 13 29 18 34 18 34 13ZM34.3 27.4V29.7H-2.3V2.3H0V27.4H34.3Z" />
  </SvgIcon>
);
