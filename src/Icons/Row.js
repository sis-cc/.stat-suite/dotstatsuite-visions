import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export default props => (
  <SvgIcon viewBox="0 0 32 38" {...props}>
    <path d="M22.848 11.424h-18.304v-4.576h18.304v4.576zM34.272 27.424v2.304h-36.544v-27.456h2.272v25.152h34.272zM18.272 18.272h-13.728v-4.544h13.728v4.544zM25.12 25.152h-20.576v-4.576h20.576v4.576z" />
  </SvgIcon>
);
