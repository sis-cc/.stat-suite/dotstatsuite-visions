import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export default props => (
  <SvgIcon viewBox="0 0 32 38" {...props}>
    <path d="M9.152 16v9.152h-4.576v-9.152h4.576zM16 6.848v18.304h-4.576v-18.304h4.576zM34.272 27.424v2.304h-36.544v-27.456h2.272v25.152h34.272zM22.848 11.424v13.728h-4.576v-13.728h4.576zM29.728 4.576v20.576h-4.576v-20.576h4.576z" />
  </SvgIcon>
);
