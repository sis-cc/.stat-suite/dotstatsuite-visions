import React from 'react';
import SvgIcon from '@mui/material/SvgIcon';

export default props => (
  <SvgIcon viewBox="0 0 32 38" {...props}>
    <path d="M10.656 21.12v4.032h-6.080v-4.032h6.080zM10.656 12.416v7.52h-6.080v-7.52h6.080zM10.656 9.28v1.984h-6.080v-1.984h6.080zM34.272 27.424v2.304h-36.544v-27.456h2.272v25.152h34.272zM29.728 22.56v2.592h-6.080v-2.592h6.080zM29.728 15.36v6.176h-6.080v-6.176h6.080zM29.728 11.36v3.008h-6.080v-3.008h6.080zM20.192 17.952v7.2h-6.080v-7.2h6.080zM20.192 8.096v8.736h-6.080v-8.736h6.080zM20.192 2.272v4.416h-6.080v-4.416h6.080z" />
  </SvgIcon>
);
