/**
 * Tooltip is component based on material ui https://material-ui.com/components/tooltips/.
 * this tooltip has onready integrate favorite style.
 * the tooltip will be placed on free space left, right, top or bottom
 * It has two color "light" and "dark"
 *
 * @memberOf VISIONS
 * @name Tooltip
 * @tag component
 * @api public
 * @props
 * Tooltip.propTypes = {
 *   variant: PropTypes.string,
 *   children: PropTypes.oneOfType([
 *     PropTypes.arrayOf(PropTypes.node),
 *     PropTypes.node
 *   ])
 * }
 * @theme
 * {
 *   palette: {
 *     warning: {
 *       main: // warning variant
 *     },
 *     common: {
 *       white: // light variant
 *       black: // dark variant
 *     },
 *     primary: {
 *       light: // link color. globale override: outerPalette.primaryLight
 *     }
 *   }
 * }
 */

export { default } from './Tooltip';
