/**
 *
 * Virtualized Tree visualization of a hierarchical list with handling of single selection
 * and Ctrl and shift binding keys multi selections.
 *
 * @memberOf VISIONS
 * @name VirtualizedTree
 * @tag component
 * @api public
 * @props
 * VirtualizedTree.propTypes = {
 *   accessibility: PropTypes.bool,
 *   items: PropTypes.arrayOf(PropTypes.shape({
 *     id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
 *     label: PropTypes.string,
 *     parentId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *     isSelected: PropTypes.bool,
 *     isDisabled: PropTypes.bool,
 *     count: PropTypes.number,
 *     path: PropTypes.arrayOf(PropTypes.shape({
 *       id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
 *       label: PropTypes.string,
 *       parentId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *       isSelected: PropTypes.bool,
 *     })),
 *     [yourDisplayAccessorKey]: PropTypes.bool,
 *   })),
 *   labels: PropTypes.shape({
 *     childrenNavigateDesc: PropTypes.string,
 *     navigateNext: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     backHelper: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     disableItemLabel: PropTypes.string,
 *   }),
 *   labelRenderer: PropTypes.func,
 * }
 */

export { default } from './VirtualizedTree';
