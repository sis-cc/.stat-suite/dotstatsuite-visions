/* eslint react/prop-types: 0 */
import * as R from 'ramda';
import React, { useEffect, useMemo, useState } from 'react';
import { getDescendants, getHierarchicalId } from './utils';

export const spotlightFilter =
  (term, labelRenderer = R.prop('label'), displayChildren = false) =>
  items => {
    if (R.isEmpty(term) || R.isNil(term)) {
      return items;
    }

    const groupedItemsByParentId = R.groupBy(R.propOr('#ROOT', 'parentId'), items);

    const findParents = (item, acc = []) => {
      const parentId = R.prop('parentId', item);
      if (parentId && parentId !== '#ROOT') {
        const parent = R.find(
          R.either(R.propEq('id', parentId), R.propEq('hierarchicalId', parentId)),
          items,
        );
        if (parent) {
          return findParents(parent, [parent, ...acc]);
        }
      }
      return acc;
    };
    const matchRegExp = str => str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

    // Highlight matching terms in the label
    const highlightLabel = label => {
      const regex = new RegExp(`(${matchRegExp(term)})`, 'gi');
      return label.replace(
        regex,
        match =>
          `<span style=background-color:yellow;border-bottom:2px solid #8CC841>${match}</span>`,
      );
    };

    const recurse = item => {
      const label = labelRenderer(item);
      const isValidItem = item => R.includes(R.toLower(term), R.toLower(item));

      let result = [];

      if (isValidItem(label)) {
        const highlightedLabel = highlightLabel(label);
        const parents = findParents(item);
        result = [...parents, { ...item, highlightedLabel }];

        if (displayChildren) {
          const children = getDescendants(getHierarchicalId(item), groupedItemsByParentId);
          const highlightedChildren = R.map(child => {
            const labelChild = labelRenderer(child);
            return isValidItem(labelChild)
              ? { ...child, highlightedLabel: highlightLabel(labelChild) }
              : child;
          })(children);
          return [...parents, { ...item, highlightedLabel }, ...highlightedChildren];
        }
      }

      if (R.has(getHierarchicalId(item), groupedItemsByParentId)) {
        const childItems = groupedItemsByParentId[getHierarchicalId(item)] || [];
        const matchingChildren = R.chain(recurse, childItems);

        // res with any matching children
        return R.uniq([...result, ...matchingChildren]);
      }
      return result;
    };

    return R.uniqBy(getHierarchicalId)(R.chain(recurse, groupedItemsByParentId['#ROOT'] || []));
  };

export const withSpotlight =
  Component =>
  ({ items, defaultSpotlight, labelRenderer, displayChildren, ...rest }) => {
    const [{ term }, setSpotlight] = useState({ term: '' });
    useEffect(() => {
      if (!R.isNil(defaultSpotlight) && !R.isEmpty(defaultSpotlight)) {
        setSpotlight(defaultSpotlight);
      }
    }, [defaultSpotlight]);

    const filteredItems = useMemo(
      () => spotlightFilter(term, labelRenderer, displayChildren)(items),
      [term, items, labelRenderer],
    );

    return (
      <Component
        {...rest}
        allItems={items}
        items={filteredItems}
        labelRenderer={labelRenderer}
        hasSpotlight={R.length(items) >= 8}
        spotlight={{ term }}
        setSpotlight={setSpotlight}
      />
    );
  };
