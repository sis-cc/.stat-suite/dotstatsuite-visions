/**
 * A modified version of the vanilla Material UI button with selected props
 *
 *
 * https://@mui/material/Button
 *
 * @memberOf VISIONS
 * @name Button
 * @tag component
 * @api public
 *
 * Button.propTypes = {
 *   alternative: PropTypes.string,
 *   className: PropTypes.string,
 *   selected: PropTypes.bool,
 *   children: PropTypes.node,
 * };
 * @theme
 * // default alternative
 * {
 *    palette: {
 *      action: {
 *        hover: // background color of hover button and selected state,
 *        selected: // background color of selected hover,
 *      }
 *    }
 * }
 * // siscc alternative
 * {
 *    palette: {
 *      primary: {
 *        main: // item main label and sub label's hover. global override: outerPalette.primaryMain
 *      }
 *      tertiary: {
 *        light: // hover background color items (e.g. Economie)
 *      },
 *      secondary: {
 *        light: // used if you set tertiary light to null
 *      }
 *      grey: {
 *        600: // svg icon color
 *        800: // sub label color. globale override: outerPalette.textLight
 *      }
 *    },
 *    typography: {
 *      body2: // font main label
 *      body1: // font sub label
 *    }
 * }
 */

export { default } from './Button';
