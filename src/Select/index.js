/**
 * Select boilerplate based on MUI components
 *
 * @memberOf VISIONS
 * @name Select
 * @tag component
 * @api public
 * @props
 * Select.propTypes = {
 *   id: PropTypes.string,
 *   value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
 *   label: PropTypes.oneOfType([
 *     PropTypes.string,
 *     PropTypes.number,
 *     PropTypes.element,
 *   ]),
 *   items: PropTypes.array,
 *   onChange: PropTypes.func,
 *   textFieldProps: PropTypes.object,
 *   valueAccessor: PropTypes.func,
 *   labelAccessor: PropTypes.func,
 *   isMultiple: PropTypes.bool,
 * };
 * @theme
 * {
 *   typography: {
 *     body2: // font, color
 *   }
 * }
 * @demoReady
 **/

export { default } from './Select';
