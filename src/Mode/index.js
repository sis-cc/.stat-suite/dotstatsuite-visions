/**
 * NoData component is a basic placeholder to use when there is nothing to display.
 *
 * @memberOf VISIONS
 * @name Mode
 * @tag component
 * @api public
 * @props
 * Mode.propTypes = {
 * changeMode: PropTypes.func,
 * mode: PropTypes.string,
 * modes: PropTypes.arrayOf(
 *  PropTypes.shape({
 *    label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *    value: PropTypes.string,
 *    popperLabel: PropTypes.string,
 *  }),
 * isSmall: PropTypes.bool,
 * }
 * @theme
 * // no custom theme there
 */

export { default } from './Mode';
