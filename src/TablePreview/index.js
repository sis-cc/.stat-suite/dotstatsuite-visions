/**
 * TablePreview use also by TableLayout. It's a preview of your main rows sections and header
 *
 * @memberOf VISIONS
 * @name TablePreview
 * @tag component
 * @api public
 *
 * const patern = {
 *   id: PropTypes.string,
 *   label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *   values: PropTypes.arrayOf(
 *     PropTypes.shape({
 *       id: PropTypes.string,
 *       label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
 *     }),
 *   ),
 * };
 *
 * Table.proptypes = {
 *   header: PropTypes.arrayOf(PropTypes.shape(patern)),
 *   sections: PropTypes.arrayOf(PropTypes.shape(patern)),
 *   rows: PropTypes.arrayOf(PropTypes.shape(patern)),
 *   labelRenderer: PropTypes.func,
 * }
 * @theme
 * {
 *   palette: {
 *     primary: {
 *       main: // header colors. global override: outerPalette.primaryMain
 *     },
 *     tertiary: {
 *       dark: // sections colors
 *       light: // rows colors
 *     },
 *     secondary: {
 *       dark: // used if tertiary dark is null
 *       light: // used if tertiary light is null
 *     }
 *   }
 * }
 **/

export { default } from './TablePreview';
