/**
 * Tag takes children for wrapped it into a hightlight
 *
 * @memberOf VISIONS
 * @name Tag
 * @tag component
 * @api public
 * @theme
 * {
 *   palette: {
 *     highlight: {
 *       hl2: // border around the label. globale override: outerPalette.highlight2
 *     },
 *     primary: {
 *       dark: // used if hl2 is null. globale override: outerPalette.primaryDark
 *     }
 *   },
 *   mixins: {
 *     tag: {
 *       root: {
 *         fontSize: 12,
 *         fontFamily: "'PT Sans Narrow', 'Helvetica Neue', Helvetica, Arial, sans-serif",
 *         color: outerPalette.textLight,
 *       },
 *     }
 *   }
 * }
 */

export { default } from './Tag';
