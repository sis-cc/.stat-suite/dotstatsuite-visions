import React from 'react';
import { TableFooter } from '../src';
import { render } from "./mockProvider";

describe('TableFooter component', () => {
  it('should render', () => {
    const { container } = render(<TableFooter />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
