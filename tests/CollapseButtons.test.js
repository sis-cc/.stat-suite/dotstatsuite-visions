import React from 'react';
import { render } from '@testing-library/react';
import { CollapseButtons } from '../src';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';

describe('CollapseButtons component', () => {
  it('should render', () => {
    const theme = createTheme();
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <CollapseButtons />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
