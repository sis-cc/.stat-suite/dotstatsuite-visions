import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { ApiQueries } from '../src';

const theme = createTheme();
const props = {
  queries: [
    {
      id: 'data',
      title: 'Data query',
      contents: [
        {
          id: 'flat',
          label: 'Flat',
          value: 'https://stats.oecd.org',
        },
        {
          id: 'time',
          label: 'Time Series',
          value: 'https://stats.oecd.org/TIME_SERIES',
        },
      ],
    },
    {
      id: 'structure',
      title: 'Structure query',
      contents: [
        {
          id: 'structure',
          value: 'https://stats.oecd.org/restsdmx/sdmx.ashx/GetDataStructure/CSPCUBE',
        },
      ],
    },
  ],
  labels: {
    title: 'Developer API query builder',
    copy: 'copy code',
    copied: 'copied',
    descriptions: [
      'The application programming interface (API) based on the SDMX standard ',
      'To get started check the API Documentation. For any questions Contact us.',
    ],
    notice: 'notice',
  },
};
describe('ApiQueries component', () => {
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <ApiQueries {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(getByTestId('apiqueries-test-id')).toBeInTheDocument();
  });
});
