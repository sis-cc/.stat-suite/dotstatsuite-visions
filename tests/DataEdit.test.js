import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { DataEdit } from '../src';

const theme = createTheme();
const props = {
  properties: {
    title: {
      id: 'title',
      isActive: true,
      onSubmit: () => {},
      onReset: () => {},
      value: 'value.title',
    },
    subtitle: {
      id: 'subtitle',
      isActive: true,
      onSubmit: () => {},
      onReset: () => {},
      value:' value.subtitle',
    },
    source: {
      id: 'source',
      isActive: true,
      onSubmit: () => {},
      onReset: () => {},
      value: 'source',
    },
    logo: {
      id: 'logo',
      isActive: true,
      onChange: () => {},
      checked: true,
    },
    copyright: {
      id: 'copyright',
      isActive: true,
      onChange: () => {},
      checked: false,
    }
  },
  labels: {
    logo: 'Remove logo',
    copyright: 'Remove copyright',
    title: 'Title',
    subtitle: 'Subtitle',
    source: 'Source',
    information: 'Information',
    resetLabel: 'reset label'
  },
};

describe('ChartsConfig component', () => {
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <DataEdit isBlank />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <DataEdit {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('data-edit-test-id')).toBeInTheDocument();
  });
});
