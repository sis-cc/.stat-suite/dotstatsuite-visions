import React from 'react';
import { NoData } from '../src';
import { render } from "./mockProvider";

describe('NoData component', () => {
  it('should render', () => {
    const { container } = render(<NoData />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
