import React from 'react';
import { act } from 'react-dom/test-utils';
import { Spotlight } from '../src';
import { useSpotlight } from '../src/Spotlight/useSpotlight';
import { render } from "./mockProvider";
import { testHook } from './utils/testHook';

describe('Spotlight component', () => {
  it('should render', () => {
    const { container } = render(<Spotlight />);
    expect(container.firstChild).toMatchSnapshot();
  });
  it('should render', () => {
    const { container } = render(<Spotlight hasClearAll defaultTerm={'toto'} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});

let apiSpotlight;
const defaultSpotlight = {
  fields: {
    label: {
      id: 'label',
      isSelected: false,
    },
  },
};
const action = v => v;
beforeEach(() => {
  testHook(() => {
    apiSpotlight = useSpotlight(true, undefined, defaultSpotlight, action);
  });
});

describe('useSpotlight', () => {
  test('should all api function function', () => {
    expect(apiSpotlight.onKeyPress).toBeInstanceOf(Function);
    expect(apiSpotlight.onClearTerm).toBeInstanceOf(Function);
    expect(apiSpotlight.onChangeTerm).toBeInstanceOf(Function);
    expect(apiSpotlight.onChangeField).toBeInstanceOf(Function);
    expect(apiSpotlight.onCommitTerm).toBeInstanceOf(Function);
    expect(apiSpotlight.setFocus).toBeInstanceOf(Function);
  });

  test('should have correct default ouput', () => {
    expect(apiSpotlight.value).toStrictEqual('');
    expect(apiSpotlight.spotlight).toStrictEqual(defaultSpotlight);
    expect(apiSpotlight.isFocus).toStrictEqual(false);
  });

  test('should update the term value when onChangeTerm is called', () => {
    expect(apiSpotlight.value).toBe('');
    act(() => {
      apiSpotlight.onChangeTerm({
        preventDefault: () => {},
        target: { value: 'toto' },
      });
    });
    expect(apiSpotlight.value).toStrictEqual('toto');
  });

  test('should erase the term value when onClearTerm is called', () => {
    act(() => {
      apiSpotlight.onChangeTerm({
        preventDefault: () => {},
        target: { value: 'toto' },
      });
      apiSpotlight.onClearTerm({
        preventDefault: () => {},
      });
    });
    expect(apiSpotlight.value).toStrictEqual('');
  });

  test('should udapte spotlight when onChangeField is called', () => {
    expect(apiSpotlight.spotlight).toStrictEqual(defaultSpotlight);
    act(() => {
      apiSpotlight.onChangeField({
        id: 'label',
        isSelected: false,
      })({
        preventDefault: () => {},
      });
    });
    expect(apiSpotlight.spotlight).toStrictEqual({
      fields: {
        label: {
          id: 'label',
          isSelected: true,
        },
      },
    });
  });

  //-----------------------------------Callback

  test('onKeyPress action callback', () => {
    act(() => {
      apiSpotlight.onChangeTerm({
        preventDefault: () => {},
        target: { value: 'toto' },
      });
    });
    expect(apiSpotlight.value).toStrictEqual('toto');
    expect(
      apiSpotlight.onKeyPress({
        key: 'Enter',
      }),
    ).toStrictEqual({ term: 'toto', spotlight: defaultSpotlight });
  });

  test('onCommitTerm action callback', () => {
    act(() => {
      apiSpotlight.onChangeTerm({
        preventDefault: () => {},
        target: { value: 'toto' },
      });
    });
    expect(apiSpotlight.value).toStrictEqual('toto');
    expect(
      apiSpotlight.onCommitTerm({
        preventDefault: () => {},
      }),
    ).toStrictEqual({ term: 'toto', spotlight: defaultSpotlight });
  });
});
