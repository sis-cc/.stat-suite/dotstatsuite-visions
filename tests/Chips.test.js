import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { DeleteAllChip, GroupedChips } from '../src';

const theme = createTheme();
const props = {

  itemProps: {
    id: 'TIME_PERIOD',
    label: 'Time period',
    values: [
      [{
        id: 'start',
        label: 'Start: 2018.07.28',
      }],
      [{
        id: 'end',
        label: 'End: 2018.07.28',
      }],
      [{
        id: 'lastn',
        label: 'Last 1 period(s)',
      }],
    ],
  },

  clearAllLabel: 'Clear all filters',
  onDeleteAll: jest.fn()
};

describe('Chips component', () => {
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <GroupedChips {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('chips-test-id')).toBeInTheDocument();
  });
  it('should render delete chip', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <DeleteAllChip {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('deleteChip-test-id')).toBeInTheDocument();
  });
});
