import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { Select } from '../src';

const theme = createTheme();
const props = {
  id: 'language',
  items: [
    { id: 'en', label: 'English' },
    { id: 'fr', label: 'Francais' },
    { id: 'jp', label: '日本' },
  ],
  selectProps: {},
  selectedId: 'en',
};

describe('Select component', () => {
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Select {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('select-testid')).toBeInTheDocument();
  });
});
