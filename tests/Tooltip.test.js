import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { Tooltip } from '../src';

const theme = createTheme();

describe('Tooltip component', () => {
  it('should render', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Tooltip title="title"><span>tooltip content</span></Tooltip>
        </ThemeProvider>
      </StyledEngineProvider>
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
