import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { Input } from '../src';

const theme = createTheme();
const props = {
  id: 'input_1',
  label: 'Width',
  placeholder: 'Enter some text',
  type: 'text',
};

describe('Input component', () => {
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Input isBlank />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Input {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('input-test-id')).toBeInTheDocument();
  });
});
