import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { Pagination } from '../src';

const theme = createTheme();
const props = {
  pages: 3,
  labels: {
    page: 'page',
    of: 'of',
  },
  page: 1,
  onChange: jest.fn(),
  onSubmit: jest.fn(),
};

describe('Pagination component', () => {
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Pagination isBlank />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });
  it('should render', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Pagination {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
