import React from 'react';
import { LabelDivider } from '../src';
import { render } from "./mockProvider";

describe('LabelDivider component', () => {
  it('should render', () => {
    const { container } = render(<LabelDivider />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
