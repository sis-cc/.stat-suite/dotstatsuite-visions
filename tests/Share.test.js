import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { Share } from '../src';

const theme = createTheme();
const props = {
  modes: [
    {
      label:
        'Snapshot of data for the period defined (data will not change even if updated on the site)',
      value: 'snapshot',
    },
    {
      label: 'Latest available data for the period defined',
      value: 'latest',
    },
  ],
  labels: {
    title: 'Data options for the Share',
    disclaimer: 'Please use this component',
    email: 'Email address (required)',
    submit: 'Request URL and embed code',
    successTitle: 'Thank you!',
    successMessage: 'We will send you the requested content shortly',
    errorTitle: 'Error',
    errorMessage: 'Something went wrong on our side',
  },
  mode: 'snapshot',
  isSharing: false,
  hasShared: false,
  hasError: false,
  changeMode: jest.fn(),
  changeMail: jest.fn(),
  share: jest.fn(),
  changeHasShared: jest.fn(),
};

describe('Dataflow component', () => {
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Share isBlank />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Share {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('share-test-id')).toBeInTheDocument();
  });
});
