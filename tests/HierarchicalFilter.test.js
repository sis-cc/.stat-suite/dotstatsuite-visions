import React from 'react';
import { render } from '@testing-library/react';
import { ThemeProvider, StyledEngineProvider, createTheme } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import R from 'ramda';
import { HierarchicalFilter } from '../src';

const theme = createTheme();

const disableAccessor = R.prop('isDisabled');
const labelRenderer = R.prop('label');
const props = {
  id: "filter",
  label: "Hierarchical Filter",
  disableAccessor,
  activePanelId: "filter",
  items: [
    { path: [], count: 22, id: 1, label: 'Aquitaine', isSelected: true },
    { path: [], count: 10, id: 2, label: 'Centre', isSelected: false },
  ],
  labelRenderer,
  onChangeActivePanel: jest.fn(),
  changeSelection: jest.fn(),
  toggleBulk: jest.fn(),
  labels: {
    apply: 'Apply',
    cancel: 'Cancel',
    colapseAll: 'Colapse All',
    expandAll: 'Expand All',
    disableItemLabel: 'Disable',
    placeholder: '...search',
    singleSelection: 'Single item',
    childrenSelection: 'Item and direct children underneath',
    branchSelection: 'Whole branch',
    levelSelection: 'All items on same level',
    allSelection: 'All items',
    selectionMode: 'Selection Mode: ',
    hint: "Did you already know that you can select several ?"
  }
};

describe('HierarchicalFilter basic render', () => {
  const { container, debug } = render(
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <div style={{ width: 300 }}>
          <HierarchicalFilter {...props} />
        </div>
      </ThemeProvider>
    </StyledEngineProvider>
  );
  it('should render', () => {
    expect(container.firstChild).toMatchSnapshot();
  });
});
