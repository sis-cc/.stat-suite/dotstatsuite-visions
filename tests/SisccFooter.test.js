import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { SisccFooter } from '../src';

const theme = createTheme();
const props = {
  leftLabel: 'Built by icon SIS-CC using .Stat Suite',
  rightLabel: [
    'This is a beta version of the .Stat Data Explorer.',
    'It covers a subset of the final expected features and is designed for test purposes only.',
  ],
};
describe('SisccFooter component', () => {
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <SisccFooter {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('footer-bar-test-id')).toBeInTheDocument();
  });
});
