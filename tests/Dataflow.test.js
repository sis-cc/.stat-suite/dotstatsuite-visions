import React from 'react';
import { render, fireEvent, getByTestId } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { Dataflow } from '../src';

const theme = createTheme();

describe('Dataflow component', () => {
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Dataflow isBlank />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });

  it('should render the title', () => {
    const title = 'SDG';

    const { getByText } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Dataflow title={title} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByText(title)).toBeInTheDocument();
  });

  it('should render and open collapse', () => {
    const title = 'SDG';
    const categories = [
      ['Reference Area', ['Italy', 'Maroc', 'Japan']],
      ['Trip type', ['Not Applicable', 'Overnights visitors', 'Total']],
    ];

    const highlights = [
      ['Reporting country', ['<em>France</em>']],
      ['Counterpart country', ['<em>France</em>']],
    ];
    const body = {
      description: 'Hello this is a description',
      highlights: 'toto',
    };

    const { getByText, container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Dataflow
            title={title}
            label='toto'
            body={body}
            categories={categories}
            highlights={highlights}
          >
            {"children"}
          </Dataflow>
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(getByText(title)).toBeInTheDocument();
    expect(container.querySelector('.MuiCollapse-hidden')).toBeInTheDocument();

    expect(getByTestId(container, "dataflow-expanded-test-id")).toBeDefined();
    const button = getByTestId(container, "dataflow-expand-button-test-id");
    fireEvent.click(button);
    expect(getByTestId(container, "dataflow-not-expand-test-id")).toBeDefined();
  });
});
