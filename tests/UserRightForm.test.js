import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { UserRightForm } from '../src';

const theme = createTheme();
const props = {
    cancel : (props) => console.log(props),
    apply: (props) => console.log(props),
    dataSpaces : [
      { value: '*', label: '* (Any)'},
      { value: 'reset', label: 'reset' },
      { value: 'stable', label: 'stable' },
    ],
    artefactTypes : [
      { value :'*', label: '* (Any)'},
      { value: 'AgencyScheme', label: 'AgencyScheme'},
      { value: 'Agency', label: 'Agency'},
      { value: 'CodeList', label: 'CodeList'},
    ],
    artefactID : {value: "*", disabled: true},
    user : { disabled: true},
    artefactVersion : { disabled: true},
    maintenanceAgencyID : { disabled: true},
    artefactType: { disabled: true},
    dataspace: {value: '*', disabled: false},
    tabs :{
      permissions: {
            id: 'basic',
            label: 'Basic Permissions',
            data: [
              {
                id: 3,
                label: 'Guest',
                definition: 'Allows retrieving structural information and non-embargoed data',
                options :[1,2]
              },
              {
                id: 2051,
                label: 'Reader',
                definition: 'Allows retrieving structural information and embargoed and non-embargoed data',
                options: [1,2,2048]
              },
              {
                id: 3363,
                label: 'Updater',
                definition: 'Allows retrieving structural information and retrieving, inserting, updating and deleting embargoed and non-embargoed data',
                options: [1,2,32,256,1024,2048]
              },
              {
                id: 4031,
                label: 'Manager',
                definition: 'Allows fully managing structural information and embargoed and non-embargoed data',
                options: [1,2,4,8,16,32,128,256,512,1024,2048]
              },
              {
                id: 4095,
                label: 'Super Exec',
                definition: 'Allows fully managing structural information, embargoed and non-embargoed data, and permissions',
                options: [1,2,4,8,16,32,64,128,256,512,1024,2048]
              },
              {
                id: 0,
                label: 'Special permissions',
                definition: 'Special combinations of advanced permissions',
                disabled: true,
              },

            ],
          },
      options:  {
            id: 'advanced',
            label: 'Advanced Permissions',
            data: [
              {
                id: 1,
                label: 'Read non-embargoed data',
                definition: 'Allows retrieving structural information',
                isSelected: false,
              },
              {
                id: 2,
                definition: 'Allows retrieving non-embargoed data',
                label: 'Read structures',
                isSelected: false,

              },
              {
                id: 4,
                definition: 'Allows ignoring production flag attribute (not used in .Stat Suite)',
                label: 'Ignore production flag',
                isSelected: false,
              },
              {
                id: 8,
                definition: 'Allows performing internal mapping configuration attribute (not used in .Stat Suite)',
                label: 'Perform internal mapping config',
                isSelected: false,
              },
              {
                id: 16,
                definition: 'Allows adding new structural information',
                label: 'Insert structures',
                isSelected: false,
              },
              {
                id: 32,
                definition: 'Allows adding new embargoed and non-embargoed data (Note: In .Stat Suite this permission currently also allows updating existing embargoed and non-embargoed data.)',
                label: 'Insert data',
                isSelected: false,
              },
              {
                id: 64,
                definition: `Allows modifying permissions (Note: In .Stat Suite this permission is currently ineffective. To allow managing permissions, 'full control' must be given.)`,
                label: 'Manage permissions',
                isSelected: false,
              },
              {
                id: 128,
                definition: 'Allows updating existing structural information',
                label:'Update structures',
                isSelected: false,
              },
              {
                id: 256,
                definition: 'Allows updating existing embargoed and non-embargoed data (Note: In .Stat Suite this permission is currently ineffective.)',
                label: 'Update data',
                isSelected: false,
              },
              {
                id: 512,
                definition: 'Allows deleting structural information',
                label: 'Delete structures',
                isSelected: false,
              },
              {
                id: 1024,
                definition: 'Allows deleting embargoed and non-embargoed data',
                label: 'Delete data',
                isSelected: false,
              },
              {
                id: 2048,
                definition: 'Allows retrieving embargoed data',
                label:'Read embargoed data',
                isSelected: false,
              }
            ],
          },
    },
    labels: {
      title: 'Permissions',
      user: 'User/group',
      dataspace: 'Dataspace',
      artefactType: 'Artefact Type',
      submit: 'Apply',
      cancel :'Cancel',
      maintenanceAgencyID: 'Maintenance agency ID',
      artefactID: 'Artefact ID',
      artefactVersion: 'Artefact version',
      permission: 'Permission',
      definition: 'Definition'
    },
  };

describe('Permission component', () => {
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <UserRightForm {...props} onCancel={props.cancel} permissionTabs={props.tabs}/>
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('permission-test-id')).toBeInTheDocument();
  });
});
