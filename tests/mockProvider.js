import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider } from '@mui/material/styles';

const theme = createTheme();
const MockProvider = () => ({ children }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>)



export const customRender = (ui, options) =>
  render(ui, { wrapper: MockProvider(options), options });
// re-export everything
export * from '@testing-library/react';
export { customRender as render };