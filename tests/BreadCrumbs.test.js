import React from 'react';
import { render } from '@testing-library/react';
import { Breadcrumbs } from '../src';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';

describe('Breadcrumbs component', () => {
  it('should render', () => {
    const theme = createTheme();
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Breadcrumbs />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
