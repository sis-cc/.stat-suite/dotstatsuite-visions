import React from 'react';
import { Loading } from '../src';
import { render } from "./mockProvider";

describe('NoData component', () => {
  it('should render', () => {
    const { container } = render(<Loading />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
