import { lt } from 'ramda';
import { getReducedContent } from '../src/DataHeader/utils';
import { T4_BREAKPOINTS } from '../src/theme';

const title = { label: 'DataHeader title', flags: [] };

const subtitle = [
  { header: 'Subtitle', label: 'entry 1' },
  { header: 'Subtitle', label: 'entry 2' }
];

const uprs = { header: 'Uprs', label: 'entry' };

const disclaimer = 'DataHeader Disclaimer';

describe('DataHeader getReducedContent tests', () => {
  it('no reduced content', () => {
    const isWidthLess = lt(T4_BREAKPOINTS.xs3);
    expect(getReducedContent({ isWidthLess, title, subtitle, uprs, disclaimer })).toEqual([])
  });
  it('subtitle uprs and disclaimer reduced content', () => {
    const isWidthLess = lt(T4_BREAKPOINTS.xs2 + 1);
    expect(getReducedContent({ isWidthLess, title, subtitle, uprs, disclaimer })).toEqual([
      { header: 'Subtitle', label: 'entry 1' },
      { header: 'Subtitle', label: 'entry 2' },
      { header: 'Uprs', label: 'entry' },
      { label: 'DataHeader Disclaimer' }
    ])
  });
  it('all reduced content', () => {
    const isWidthLess = lt(T4_BREAKPOINTS.xs2 - 1);
    expect(getReducedContent({ isWidthLess, title, subtitle, uprs, disclaimer })).toEqual([
      { label: 'DataHeader title', flags: [] },
      { header: 'Subtitle', label: 'entry 1' },
      { header: 'Subtitle', label: 'entry 2' },
      { header: 'Uprs', label: 'entry' },
      { label: 'DataHeader Disclaimer' }
    ])
  });
});
