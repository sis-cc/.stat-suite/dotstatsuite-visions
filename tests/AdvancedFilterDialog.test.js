import React from 'react';
import { render } from '@testing-library/react';
import { ThemeProvider, StyledEngineProvider, createTheme } from '@mui/material/styles';
import R from 'ramda';
import { AdvancedFilterDialog } from '../src';
import { getScopeGetters } from '../src/VirtualizedTree/utils';

const theme = createTheme();

const disableAccessor = R.prop('isDisabled');
const labelRenderer = R.prop('label');
const props = {
  id: "filter",
  label: "Hierarchical Filter",
  disableAccessor,
  activePanelId: "filter",
  items: [
    { path: [], count: 22, id: 1, label: 'Aquitaine', isSelected: true },
    { path: [], count: 10, id: 2, label: 'Centre', isSelected: false },
  ],
  labelRenderer,
  onChangeActivePanel: jest.fn(),
  changeSelection: jest.fn(),
  toggleBulk: jest.fn(),
  labels: {
    apply: 'Apply',
    cancel: 'Cancel',
    colapseAll: 'Colapse All',
    expandAll: 'Expand All',
    disableItemLabel: 'Disable',
    placeholder: '...search',
    singleSelection: 'Single item',
    childrenSelection: 'Item and direct children underneath',
    branchSelection: 'Whole branch',
    levelSelection: 'All items on same level',
    allSelection: 'Select All',
    selectionMode: 'Selection Mode: ',
    hint: "Did you already know that you can select several ?"
  }
};

describe('AdvancedFilter basic render', () => {
  it('should render (closed)', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <AdvancedFilterDialog {...props} isOpen={false} />
        </ThemeProvider>
      </StyledEngineProvider>
    );
    expect(container.firstChild).toMatchSnapshot();
  });
  it('should render (opened)', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <AdvancedFilterDialog {...props} isOpen={true} />
        </ThemeProvider>
      </StyledEngineProvider>
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});

describe('AdvancedFilterDialog utils tests', () => {
  const disable = R.propOr(false, 'isDisabled');
  const flatItems = [
    { id: '0' },
    { id: '1' },
    { id: '0.0', parentId: '0' },
    { id: '0.1', parentId: '0' },
    { id: '0.2', parentId: '0', isDisabled: true },
    { id: '0.2.0', parentId: '0.2' },
    { id: '0.2.1', parentId: '0.2' },
    { id: '0.2.2', parentId: '0.2' },
    { id: '1.0', parentId: '1' }
  ];
  const items = {
    indexedItemsById: R.indexBy(R.prop('id'), flatItems),
    groupedItemsByParentId: R.groupBy(R.propOr('#ROOT', 'parentId'), flatItems)
  };
  it('branch getter test', () => {
    const scopeGetter = getScopeGetters.branch(items);
    expect(scopeGetter({ id: '0' })).toEqual([
      { id: '0' },
      { id: '0.0', parentId: '0' },
      { id: '0.1', parentId: '0' },
      { id: '0.2.0', parentId: '0.2' },
      { id: '0.2.1', parentId: '0.2' },
      { id: '0.2.2', parentId: '0.2' },
    ]);
  });
  it('children getter test', () => {
    const scopeGetter = getScopeGetters.children(items);
    expect(scopeGetter({ id: '0' })).toEqual([
      { id: '0' },
      { id: '0.0', parentId: '0' },
      { id: '0.1', parentId: '0' },
    ]);
  });
  it('level getter test', () => {
    const scopeGetter = getScopeGetters.level(items);
    expect(scopeGetter({ id: '0.0', parentId: '0' })).toEqual([
      { id: '0.0', parentId: '0' },
      { id: '0.1', parentId: '0' },
      { id: '1.0', parentId: '1' }
    ]);
  });
});
