import React from 'react';
import { render } from '@testing-library/react';
import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import R from 'ramda';
import {
  getCurrentList,
  getPlaceholder,
  getList,
  withScopeList,
  getScopeList,
  getItemsIndexedByParentId,
  getIsBlank,
  getItemsIds,
  getItemsGroupByLevel,
  getIsAllSelected,
  setAllTo,
  setSelection,
} from '../src/ScopeList/with-scope-list';
import { reduceChildren, spotlightScopeListEngine } from '../src/utils';
import { ScopeList, theme } from '../src';

const disableAccessor = R.prop('isDisabled');
const props = {
  id: "scopeListId",
  label: "Scopelist",
  items: [
    { path: [], count: 22, id: 1, label: 'Aquitaine', isSelected: true },
    { path: [], count: 10, id: 2, label: 'Centre', isSelected: false },
  ],
  onChangeActivePanel: jest.fn(),
  changeSelection: jest.fn(),
};

describe('Pagination component', () => {
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <ScopeList isBlank />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <ScopeList {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('scopeList')).toBeInTheDocument();
  });
});


describe('indexByParentIdWithHasData', () => {
  it('error should level 3', () => {
    const items = [
      { id: 1, label: 'abc' },
      { id: 10, parentId: 1, label: 'abd' },
      { id: 100, parentId: 10, label: 'bcd' },
      { id: 1000, parentId: 100, label: 'bce' },
      { id: 10000, parentId: 1000, label: 'cde', hasData: true },
    ];
    const expected = {
      1: { id: 10, parentId: 1, label: 'abd' },
      10: { id: 100, parentId: 10, label: 'bcd' },
      100: { id: 1000, parentId: 100, label: 'bce' },
      1000: { id: 10000, parentId: 1000, label: 'cde', hasData: true },
      undefined: {
        id: 1000,
        label: 'bce',
        parentId: 100,
      },
    }; // undefined key will be never used
    expect(getItemsIndexedByParentId(R.prop('hasData'))(items)).toEqual(expected);
  });
});

describe('getItemsIds', () => {
  it('get ids on isSelected item', () => {
    const items = [
      { id: 1, label: 'abc' },
      { id: 10, parentId: 1, label: 'abd' },
      { id: 100, parentId: 10, label: 'bcd' },
      { id: 1000, parentId: 100, label: 'bce' },
      { id: 10000, parentId: 1000, label: 'cde', isSelected: true },
    ];
    expect(getItemsIds(disableAccessor, { isSelected: true })(items)).toEqual([10000]);
    expect(getItemsIds(disableAccessor, { isSelected: false })(items)).toEqual([1, 10, 100, 1000]);
  });
});

describe('getItemsGroupByLevel', () => {
  it('get items group by level, disabled should not appear', () => {
    const items = [
      { id: 1, label: 'abc' },
      { id: 10, parentId: 1, label: 'abd' },
      { id: 100, parentId: 10, label: 'bcd' },
      { id: 1000, parentId: 100, label: 'bce' },
      { id: 10000, parentId: 1000, label: 'cde', isSelected: true },
      { id: 10001, parentId: 1000, label: 'cde', isDisabled: true },
    ];
    const itemsById = R.indexBy(R.prop('id'), items);
    const expected = {
      0: [{ id: 1, isSelected: undefined, label: 'abc', level: 0  }],
      1: [{ id: 10, isSelected: undefined, parentId: 1, label: 'abd', level: 1  }],
      2: [{ id: 100, isSelected: undefined, parentId: 10, label: 'bcd', level: 2  }],
      3: [{ id: 1000, isSelected: undefined, parentId: 100, label: 'bce', level: 3  }],
      4: [{ id: 10000, isSelected: true, parentId: 1000, label: 'cde', level: 4 }],
    };

    expect(getItemsGroupByLevel(disableAccessor)(itemsById)(items)).toEqual(expected);
  });
});

describe('getIsAllSelected', () => {
  it('must return false or true regarding isSelected', () => {
    const items = [
      { id: 1, label: 'abc' },
      { id: 10, parentId: 1, label: 'abd' },
      { id: 100, parentId: 10, label: 'bcd' },
      { id: 1000, parentId: 100, label: 'bce' },
      { id: 10000, parentId: 1000, label: 'cde', isSelected: true },
    ];

    expect(getIsAllSelected(disableAccessor)(items)).toEqual(false);
    expect(getIsAllSelected(disableAccessor)([
      { id: 10000, parentId: 1000, label: 'cde', isSelected: true }
    ])).toEqual(true);
  });
});

describe('setAllTo', () => {
  it('define the next value isSelected for the all indexes', () => {
    expect(setAllTo([{ id: 1 }, { id: 2 }])([0])).toEqual(undefined);
    expect(setAllTo([{ id: 1 }, { id: 2, isSelected: true }])([0, 1])).toEqual(undefined);
    expect(setAllTo([{ id: 1, isSelected: true }])([0])).toEqual(true);
    expect(setAllTo([{ id: 1, isSelected: true }, { id: 2, isSelected: true }])([0, 1])).toEqual(true);
    expect(setAllTo([{ id: 1, isSelected: true }, { id: 2 }, { id: 3 }])([1, 2])).toEqual(undefined);
  });
});

describe('setSelection', () => {
  it('set isSelected value regarding first index and items function, isDiscrete false = range of indexes ', () => {
    const list = [{ id: 'A'}, { id : 'B'}, { id: 'C'}];
    expect(setSelection({ items: [[0,2], R.not] }, list )).toEqual([
      { id: 'A', isSelected: true},
      { id: 'B', isSelected: true},
      { id: 'C', isSelected: true},
    ]);
    expect(setSelection({ items: [[0,2]], isDiscrete: true }, list )).toEqual([
      { id: 'A', isSelected: true},
      { id: 'B' },
      { id: 'C', isSelected: true},
    ]);
  });
  it('set isSelected value regarding first index element', () => {
    const list = [{ id: 'A', isSelected: true}, { id : 'B'}, { id: 'C'}];
    expect(setSelection({ items: [[0,2]] }, list )).toEqual([
      { id: 'A', isSelected: true},
      { id: 'B', isSelected: true},
      { id: 'C', isSelected: true},
    ]);
  });
});

describe('getCurrentList', () => {
  it('root element', () => {
    const items = [{ id: 1 }, { id: 2 }, { id: 3 }];
    const itemsByParentId = [];
    const expected = [
      { id: 1, hasChild: false },
      { id: 2, hasChild: false },
      { id: 3, hasChild: false },
    ];
    expect(getCurrentList({ currentItemId: undefined, items, itemsByParentId })).toEqual(expected);
  });
  it('root element with child', () => {
    const items = [
      { id: 1 },
      { id: 10, parentId: 0 }, // does never must appear
      { id: 2 },
      { id: 3 },
      { id: 30, parentId: 3 },
    ];
    const itemsByParentId = {
      0: { id: 10, parentId: 0 },
      3: { id: 30, parentId: 3 },
    };
    const expected = [
      { id: 1, hasChild: false },
      { id: 2, hasChild: false },
      { id: 3, hasChild: true },
    ];
    expect(getCurrentList({ currentItemId: undefined, items, itemsByParentId })).toEqual(expected);
  });
  it('childs with parentId 3 must appear', () => {
    const nodeId = 3;
    const items = [
      { id: 1 },
      { id: 10, parentId: 0 }, // does never must appear
      { id: 2 },
      { id: 3 },
      { id: 30, parentId: 3 },
    ];
    const itemsByParentId = {
      0: { id: 10, parentId: 0 },
      3: { id: 30, parentId: 3 },
    };
    const expected = [{ id: 30, parentId: 3, hasChild: false }];
    expect(getCurrentList({ currentItemId: nodeId, items, itemsByParentId })).toEqual(expected);
  });
  it('should return list with display Accessor', () => {
    const items = [
      { id: 1, label: 'abc' },
      { id: 10, parentId: 1, label: 'abd' },
      { id: 100, parentId: 10, label: 'bcd' },
      { id: 1000, parentId: 100, label: 'bce' },
      { id: 10000, parentId: 1000, label: 'cde', hasData: true },
      { id: 2, label: 'abc2', hasData: true },
      { id: 20, parentId: 2, label: 'abd' },
    ];
    const itemsByParentId = {
      '1': { id: 10, parentId: 1, label: 'abd' },
      '2': { id: 20, parentId: 2, label: 'abd' },
      '10': { id: 100, parentId: 10, label: 'bcd' },
      '100': { id: 1000, parentId: 100, label: 'bce' },
      '1000': { id: 10000, parentId: 1000, label: 'cde', hasData: true },
    };
    const expected = [
      { id: 1, label: 'abc', isDisabled: true, hasChild: true },
      { id: 2, label: 'abc2', hasData: true, hasChild: true },
    ];
    expect(
      getScopeList({
        isActivePanel: true,
        displayAccessor: R.prop('hasData'),
        items,
        itemsByParentId,
      }),
    ).toEqual(expected);
  });
});

describe('getPlaceholder ', () => {
  it('default placeholder', () => {
    const placeholder = 'default placeholder';
    expect(getPlaceholder({ placeholder })).toEqual(placeholder);
  });
  it('should main', () => {
    const mainPlaceholder = 'main';
    const secondaryPlaceholder = 'secondary';
    expect(getPlaceholder({ mainPlaceholder, secondaryPlaceholder })).toEqual(mainPlaceholder);
  });
  it('should secondaryPlaceholder', () => {
    const mainPlaceholder = 'main';
    const secondaryPlaceholder = 'secondary';
    expect(getPlaceholder({ mainPlaceholder, secondaryPlaceholder, currentItem: {} })).toEqual(
      secondaryPlaceholder,
    );
  });
});

describe('getList  ', () => {
  const itemsById = {
    1: { id: 1, label: 'abc' },
    10: { id: 10, parentId: 1, label: 'abd' },
    100: { id: 100, parentId: 10, label: 'bcd' },
    1000: { id: 1000, parentId: 100, label: 'bce' },
    10000: { id: 10000, parentId: 1000, label: 'cde' },
  };
  const itemsByParentId = {
    1: { id: 10, parentId: 1, label: 'abd' },
    10: { id: 100, parentId: 10, label: 'bcd' },
    100: { id: 1000, parentId: 100, label: 'bce' },
    1000: { id: 10000, parentId: 1000, label: 'cde' },
  };
  const items = [
    { id: 1, label: 'abc' },
    { id: 10, parentId: 1, label: 'abd' },
    { id: 100, parentId: 10, label: 'bcd' },
    { id: 1000, parentId: 100, label: 'bce' },
    { id: 10000, parentId: 1000, label: 'cde' },
  ];
  const topElementProps = {
    defaultSpotlight: {
      engine: spotlightScopeListEngine,
      fields: {
        label: {
          id: 'label',
          accessor: 'label',
          isSelected: true,
        },
      },
    },
  };
  it('should only element without parent list', () => {
    const term = '';
    const expected = [{ id: 1, label: 'abc', hasChild: true }];
    expect(getList({ items, itemsByParentId, itemsById, topElementProps, term })).toEqual(expected);
  });
  it('should default list', () => {
    const term = '';
    const expected = [{ id: 1, label: 'abc', hasChild: true }];
    expect(getList({ items, itemsByParentId, itemsById, topElementProps, term })).toEqual(expected);
  });
  it('should default list', () => {
    const term = 'b';
    const expected = [{ id: 1000, parentId: 100, label: 'bce', hasChild: true }];
    const currentItem = { id: 100, parentId: 10, label: 'bcd' };
    expect(
      getList({ items, itemsByParentId, itemsById, topElementProps, term, currentItem }),
    ).toEqual(expected);
  });
  it('should spotlight in all list, path appear', () => {
    const term = 'a';
    const expected = [
      { id: 1, label: 'abc', nodePath: [] },
      { id: 10, parentId: 1, label: 'abd', nodePath: [{ id: 1, label: 'abc' }] },
    ];
    const currentItem = null;
    expect(
      getList({ items, itemsByParentId, itemsById, topElementProps, term, currentItem }),
    ).toEqual(expected);
  });
});

describe('utils | reduceChildren', () => {
  it('if parentId is root there has no path', () => {
    const node = { id: 1, label: '1' };
    const items = {
      1: { id: 1, label: '1' },
    };
    const expected = [];
    expect(reduceChildren(node, items)).toEqual(expected);
  });
  it('if parentId id undefined', () => {
    const node = { label: '1', parentId: 0 };
    const items = {
      10: { id: 10, label: '10', parentId: 1 },
    };
    const expected = [];
    expect(reduceChildren(node, items)).toEqual(expected);
  });
  it('one element in path', () => {
    const node = { id: 10, parentId: 1, label: '10' };
    const items = {
      1: { id: 1, label: '1' },
      10: { id: 10, parentId: 1, label: '10' },
    };
    const expected = [{ id: 1, label: '1' }];
    expect(reduceChildren(node, items)).toEqual(expected);
  });
  it('with many path', () => {
    const node = { id: 10000, parentId: 1000, label: '10000' };
    const items = {
      1: { id: 1, label: '1' },
      10: { id: 10, parentId: 1, label: '10' },
      100: { id: 100, parentId: 10, label: '100' },
      1000: { id: 1000, parentId: 100, label: '1000' },
      10000: { id: 10000, parentId: 1000, label: '10000' },
    };
    const expected = [
      { id: 1, label: '1' },
      { id: 10, parentId: 1, label: '10' },
      { id: 100, parentId: 10, label: '100' },
      { id: 1000, parentId: 100, label: '1000' }];
    expect(reduceChildren(node, items)).toEqual(expected);
  });
  it('end with personalize text', () => {
    const node = { id: 10000, parentId: 1000, label: '10000' };
    const items = {
      1: { id: 1, label: '1' },
      10: { id: 10, parentId: 1, label: '10' },
      100: { id: 100, parentId: 10, label: '100' },
      1000: { id: 1000, parentId: 100, label: '1000' },
      10000: { id: 10000, parentId: 1000, label: '10000' },
    };
    const expected = [
      { id: 1, label: '1' },
      { id: 10, parentId: 1, label: '10' },
      { id: 100, parentId: 10, label: '100' },
      { id: 1000, parentId: 100, label: '1000' }, 'end'];
    expect(reduceChildren(node, items, R.flip(R.prepend), ['end'])).toEqual(expected);
  });
});

describe('utils | spotlightScopeListEngine', () => {
  it('term is string and got nodePath', () => {
    const term = 'u';
    const fields = {
      label: {
        accessor: 'label',
        id: 'label',
        isSelected: true,
      },
    };
    const items = [
      { id: 1, label: 'Aquitaine' },
      { id: 2, label: 'Centre' },
      { id: 20, label: 'Manthelan', parentId: 2 },
      { id: 21, label: 'Loches', parentId: 2 },
      { id: 22, label: 'Ligueil', parentId: 2 },
    ];
    const itemsById = {
      1: { id: 1, label: 'Aquitaine' },
      2: { id: 2, label: 'Centre' },
      20: { id: 20, label: 'Manthelan', parentId: 2 },
      21: { id: 21, label: 'Loches', parentId: 2 },
      22: { id: 22, label: 'Ligueil', parentId: 2 },
    };
    const expected = [
      { nodePath: [], id: 1, label: 'Aquitaine' },
      { nodePath: [{ id: 2, label: 'Centre' }], id: 22, label: 'Ligueil', parentId: 2 },
    ];
    expect(spotlightScopeListEngine({ term, fields, items, itemsById })).toEqual(expected);
  });
  it('term search id, not in main list, nodePath must not appear', () => {
    const term = '1';
    const fields = {
      label: {
        accessor: 'id',
        id: 'label',
        isSelected: true,
      },
    };
    const items = [
      { id: 1, label: 'Aquitaine' },
      { id: 2, label: 'Centre' },
      { id: 20, label: 'Manthelan', parentId: 2 },
      { id: 21, label: 'Loches', parentId: 2 },
      { id: 22, label: 'Ligueil', parentId: 2 },
    ];
    const expected = [{ id: 1, label: 'Aquitaine' }, { id: 21, label: 'Loches', parentId: 2 }];
    const noPath = true;
    expect(spotlightScopeListEngine({ term, fields, items, noPath })).toEqual(expected);
  });
  it('term is not declared', () => {
    const fields = {
      label: {
        accessor: 'id',
        id: 'label',
        isSelected: true,
      },
    };
    const items = [
      { id: 1, label: 'Aquitaine' },
      { id: 2, label: 'Centre' },
      { id: 20, label: 'Manthelan', parentId: 2 },
      { id: 21, label: 'Loches', parentId: 2 },
      { id: 22, label: 'Ligueil', parentId: 2 },
    ];
    const noPath = true;
    expect(spotlightScopeListEngine({ fields, items, noPath })).toEqual(items);
  });
});

// scopelist is not used anymore, if it comes back, tests will be transposed into RTL
// (enzyme tests state while RTL tests the outcome,
// if the same feature is done with something else than the state,
// enzyme test should rewritten where RTL will stay,
// if a test needs to be rewritten because the implementation has changed,
// but not the feature, that's not ideal)
/* describe('hoc | withScopeList', () => {
  const Component = withScopeList(ScopeList);
  it('should have at least those properties', () => {
    const { container } = render(<Component items={[]} />);
    const props = wrapper.props();
    expect(props).toHaveProperty('items');
    expect(props).toHaveProperty('tagValue');
    expect(props).toHaveProperty('list');
    expect(props).toHaveProperty('changeList');
    expect(props).toHaveProperty('changeSelection');
    expect(props).toHaveProperty('currentItem');
    expect(props).toHaveProperty('action'); // changeSpotlight
    expect(props).toHaveProperty('term');
    expect(props).toHaveProperty('placeholder');
    expect(props).toHaveProperty('changeSelect');
  });
  it('should test changing props', () => {
    const items = [{ path: [], count: 23, id: 2, label: 'Centre', isSelected: true, level: 0 }];

    const { container } = render(<Component items={items} />);
    expect(wrapper.props().list).toEqual([
      { path: [], count: 23, id: 2, label: 'Centre', isSelected: true, level: 0, hasChild: false },
    ]);
    expect(wrapper.props().tagValue).toEqual('1/1');
    expect(wrapper.state().currentItem).toEqual(undefined);

    // change props
    wrapper.setProps({
      items: [{ path: [], count: 1, id: 5, label: 'Paris', isSelected: false, level: 0 }],
    });
    expect(wrapper.props().list).toEqual([
      { path: [], count: 1, id: 5, label: 'Paris', isSelected: false, level: 0, hasChild: false },
    ]);
    expect(wrapper.props().tagValue).toEqual('0/1');
    expect(wrapper.state().currentItem).toEqual(undefined);
  });
  it('should test changeList', () => {
    const items = [
      { path: [], count: 23, id: 2, label: 'Centre', isSelected: true, level: 0 },
      { path: [], count: 1, id: 20, parentId: 2, label: 'Ligueil', isSelected: false, level: 1 },
    ];

    const { container } = render(<Component items={items} />);
    expect(wrapper.props().list).toEqual([
      { path: [], count: 23, id: 2, label: 'Centre', isSelected: true, level: 0, hasChild: true },
    ]);
    expect(wrapper.props().tagValue).toEqual('1/2');
    expect(wrapper.state().currentItem).toEqual(undefined);

    // navigate into the list
    wrapper.instance().changeList(2);
    expect(wrapper.props().list).toEqual([
      {
        path: [],
        count: 1,
        id: 20,
        parentId: 2,
        label: 'Ligueil',
        isSelected: false,
        level: 1,
        hasChild: false,
      },
    ]);
    expect(wrapper.props().tagValue).toEqual('1/2');
    expect(wrapper.state().currentItem).toEqual({
      path: [],
      count: 23,
      id: 2,
      label: 'Centre',
      isSelected: true,
      level: 0,
    });
  });
  it('should test changeSelection', () => {
    const changeSelection = (panelId, id) => [panelId, id];
    const { container } = render(<Component items={[]} changeSelection={changeSelection} />);
    expect(wrapper.instance().changeSelection('toto')(42)({ preventDefault: () => {} })).toEqual([
      'toto',
      [42],
    ]);
  });
  it('should test changeSpotlight ', () => {
    const fields = {
      label: {
        accessor: 'label',
        id: 'label',
        isSelected: true,
      },
    };
    const items = [
      { id: 2, label: 'Centre' },
      { id: 20, parentId: 2, label: 'Ligueil' },
      { id: 200, parentId: 20, label: 'liegeois' },
      { id: 3, label: 'Tortuga' },
      { id: 30, parentId: 3, label: 'Lisbonne' },
    ];
    const { container } = render(
      <Component
        items={items}
        topElementProps={{ defaultSpotlight: { engine: spotlightScopeListEngine, fields } }}
      />,
    );
    wrapper.instance().changeSpotlight({ term: 'li' });
    expect(wrapper.props().list).toEqual([
      { id: 20, parentId: 2, label: 'Ligueil', nodePath: [{ id: 2, label: 'Centre' }] },
      { id: 200, parentId: 20, label: 'liegeois', nodePath: [{ id: 2, label: 'Centre' }, { id: 20, parentId: 2, label: 'Ligueil' }] },
      { id: 30, parentId: 3, label: 'Lisbonne', nodePath: [{ id: 3, label: 'Tortuga' }] },
    ]);
  });
}); */

describe('utils | getIsBlank', () => {
  it('isBlank scopelist without displayAccessor', () => {
    expect(getIsBlank([],undefined, 0)).toEqual(true);
    expect(getIsBlank([{}],undefined, 0)).toEqual(false);
    expect(getIsBlank([{}], undefined, 1)).toEqual(true);
    expect(getIsBlank([{}, {}], undefined, 1)).toEqual(false);
  });
  it('isBlank scopelist with displayAccessor', () => {
    expect(getIsBlank([], R.prop('hasData'), 0)).toEqual(true);
    expect(getIsBlank([{}], R.prop('hasData'), 1)).toEqual(true);
    expect(getIsBlank([{}, {}], R.prop('hasData'), 1)).toEqual(true);
    expect(getIsBlank([{ hasData: true }, {}], R.prop('hasData'), 1)).toEqual(true);
    expect(getIsBlank([{ hasData: true }, { hasData: true }], R.prop('hasData'), 1)).toEqual(false);
  });
});
