import { getEnhancedValuesHeaderData } from '../src/TableHtml5/TableHtml5';

describe('TableHtml5 getEnhancedValuesHeaderData tests', () => {
  it('simple transpose without merge', () => {
    const headerData = [
      { data: [{ value: { id: 'a' } }, { value: { id: '0' } }] },
      { data: [{ value: { id: 'b' } }, { value: { id: '1' } }] },
      { data: [{ value: { id: 'c' } }, { value: { id: '2' } }] },
      { data: [{ value: { id: 'd' } }, { value: { id: '3' } }] },
    ];

    expect(getEnhancedValuesHeaderData(headerData)).toEqual([
      [
        { key: 'a', spanCount: 1, value: { id: 'a' }, isEmpty: false },
        { key: 'b', spanCount: 1, value: { id: 'b' }, isEmpty: false },
        { key: 'c', spanCount: 1, value: { id: 'c' }, isEmpty: false },
        { key: 'd', spanCount: 1, value: { id: 'd' }, isEmpty: false },
      ],
      [
        { key: 'a-0', spanCount: 1, value: { id: '0' }, isEmpty: false },
        { key: 'b-1', spanCount: 1, value: { id: '1' }, isEmpty: false },
        { key: 'c-2', spanCount: 1, value: { id: '2' }, isEmpty: false },
        { key: 'd-3', spanCount: 1, value: { id: '3' }, isEmpty: false },
      ]
    ]);
  });
  it('simple transpose with merge', () => {
    const headerData = [
      { data: [{ value: { id: 'a' } }, { value: { id: '0' } }] },
      { data: [{ value: { id: 'a' } }, { value: { id: '1' } }] },
      { data: [{ value: { id: 'b' } }, { value: { id: '0' } }] },
      { data: [{ value: { id: 'b' } }, { value: { id: '1' } }] },
    ];

    expect(getEnhancedValuesHeaderData(headerData)).toEqual([
      [
        { key: 'a', spanCount: 2, value: { id: 'a' }, isEmpty: false },
        { key: 'b', spanCount: 2, value: { id: 'b' }, isEmpty: false },
      ],
      [
        { key: 'a-0', spanCount: 1, value: { id: '0' }, isEmpty: false },
        { key: 'a-1', spanCount: 1, value: { id: '1' }, isEmpty: false },
        { key: 'b-0', spanCount: 1, value: { id: '0' }, isEmpty: false },
        { key: 'b-1', spanCount: 1, value: { id: '1' }, isEmpty: false },
      ]
    ]);
  });
  it('no merge on the last row', () => {
    const headerData = [
      { data: [{ value: { id: 'a' } }, { value: { id: '0' } }] },
      { data: [{ value: { id: 'b' } }, { value: { id: '0' } }] },
      { data: [{ value: { id: 'c' } }, { value: { id: '1' } }] },
      { data: [{ value: { id: 'd' } }, { value: { id: '1' } }] },
    ];

    expect(getEnhancedValuesHeaderData(headerData)).toEqual([
      [
        { key: 'a', spanCount: 1, value: { id: 'a' }, isEmpty: false },
        { key: 'b', spanCount: 1, value: { id: 'b' }, isEmpty: false },
        { key: 'c', spanCount: 1, value: { id: 'c' }, isEmpty: false },
        { key: 'd', spanCount: 1, value: { id: 'd' }, isEmpty: false },
      ],
      [
        { key: 'a-0', spanCount: 1, value: { id: '0' }, isEmpty: false },
        { key: 'b-0', spanCount: 1, value: { id: '0' }, isEmpty: false },
        { key: 'c-1', spanCount: 1, value: { id: '1' }, isEmpty: false },
        { key: 'd-1', spanCount: 1, value: { id: '1' }, isEmpty: false },
      ]
    ]);
  });
  it('merge with combinations handling', () => {
    const headerData = [
      { data: [{ values: [{ id: 'FRA' }, { id: 'TOT' }, { id: 'M' }] }, { values: [{ id: 'EUR' }, { id: 'MILL' }] }] },
      { data: [{ values: [{ id: 'FRA' }, { id: 'TOT' }, { id: 'M' }] }, { values: [{ id: 'DOL' }, { id: 'MILL' }] }] },
      { data: [{ values: [{ id: 'GER' }, { id: 'TOT' }, { id: 'M' }] }, { values: [{ id: 'EUR' }, { id: 'MILL' }] }] },
      { data: [{ values: [{ id: 'GER' }, { id: 'TOT' }, { id: 'M' }] }, { values: [{ id: 'DOL' }, { id: 'MILL' }] }] },
    ];

    expect(getEnhancedValuesHeaderData(headerData)).toEqual([
      [
        { key: 'FRA,TOT,M', spanCount: 2, values: [{ id: 'FRA' }, { id: 'TOT' }, { id: 'M' }], isEmpty: false },
        { key: 'GER,TOT,M', spanCount: 2, values: [{ id: 'GER' }, { id: 'TOT' }, { id: 'M' }], isEmpty: false },
      ],
      [
        { key: 'FRA,TOT,M-EUR,MILL', spanCount: 1, values: [{ id: 'EUR' }, { id: 'MILL' }], isEmpty: false },
        { key: 'FRA,TOT,M-DOL,MILL', spanCount: 1, values: [{ id: 'DOL' }, { id: 'MILL' }], isEmpty: false },
        { key: 'GER,TOT,M-EUR,MILL', spanCount: 1, values: [{ id: 'EUR' }, { id: 'MILL' }], isEmpty: false },
        { key: 'GER,TOT,M-DOL,MILL', spanCount: 1, values: [{ id: 'DOL' }, { id: 'MILL' }], isEmpty: false },
      ]
    ]);
  });
  it('merge with empty values', () => {
    const headerData = [
      { data: [{ value: { id: 'p_a' } }] },
      { data: [{ value: { id: 'a' } }, { value: { id: '0' } }] },
      { data: [{ value: { id: 'a' } }, { value: { id: '1' } }] },
      { data: [{ value: { id: 'p_b' } }] },
      { data: [{ value: { id: 'b' } }, { value: { id: '0' } }] },
      { data: [{ value: { id: 'b' } }, { value: { id: '1' } }] },
    ];

    expect(getEnhancedValuesHeaderData(headerData)).toEqual([
      [
        { key: 'p_a', spanCount: 1, value: { id: 'p_a' }, isEmpty: false },
        { key: 'a', spanCount: 2, value: { id: 'a' }, isEmpty: false },
        { key: 'p_b', spanCount: 1, value: { id: 'p_b' }, isEmpty: false },
        { key: 'b', spanCount: 2, value: { id: 'b' }, isEmpty: false },
      ],
      [
        { key: 'p_a-missing1', spanCount: 1, isEmpty: true },
        { key: 'a-0', spanCount: 1, value: { id: '0' }, isEmpty: false },
        { key: 'a-1', spanCount: 1, value: { id: '1' }, isEmpty: false },
        { key: 'p_b-missing1', spanCount: 1, isEmpty: true },
        { key: 'b-0', spanCount: 1, value: { id: '0' }, isEmpty: false },
        { key: 'b-1', spanCount: 1, value: { id: '1' }, isEmpty: false },
      ]
    ]);
  });
});
