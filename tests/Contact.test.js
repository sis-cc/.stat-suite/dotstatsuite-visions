import React from 'react';
import { render} from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import Contact from '../src/Contact';

const theme = createTheme();
const props = {
    send: jest.fn(),
    modes: [
      {
        label: 'Content question',
        value: 'question',
        message:"I cannot find the data I'm looking for, the data I found doesn't correspond to what I expected, or I have another question concerning the data."
      },
      {
        label: 'Technical problem',
        value: 'problem',
        message:"I have a technical problem to access, select, configure, view, share or download data,{br} or with any other feature of this web site."
      },
      {
        label:'Feedback',
        value:'feedback',
        message:"I wish to give some feedback on this site to help to improve it or to thank the teams behind it."
      }
    ],
    labels: {
      contactForHelp:'I agree to be contacted to help improving the web site',
      title: 'Contact us about :',
      email: 'Your email address (required)',
      name :'Your full name',
      organisation : 'Your organisation',
      titleForm : 'Your title',
      submit: 'Send',
      organisationPrivacyPolicy: 'OECD privacy policy'
    },
    captchaKey:'6LcBbIghAAAAAPrMHLJBlCxmFozu11fM_D9fsm7u',
    lang: 'en',
    theme:'light',
  };

describe('Contact component', () => {
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <Contact {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('contact-test-id')).toBeInTheDocument();
  });
});
