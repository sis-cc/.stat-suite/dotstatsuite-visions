import React from 'react';
import { render } from '@testing-library/react';
import { forEach } from 'ramda';
import * as Icons from '../src/Icons';

const icons = [
  'VSymbol',
  'Scatter',
  'HSymbol',
  'Row',
  'Bar',
  'Timeline',
  'StackedBar',
  'StackedRow',
  'Cross',
  'Asterisk',
  'Api',
  'AccessibilityFilled',
  'AccessibilityOutlined',
  'AccountFilled',
  'AccountOutlined',
  'Warning',
  'CopyContent',
];

describe('icons', () => {
  forEach(icon => {
    it(`should render ${icon}`, () => {
      const Icon = Icons[icon];
      const { container } = render(<Icon />);
      expect(container.firstChild).toMatchSnapshot();
    });
  }, icons);
});
