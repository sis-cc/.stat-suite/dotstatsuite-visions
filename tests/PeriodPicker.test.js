import React from 'react';
import * as R from 'ramda';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { PeriodPicker } from '../src';
import { getDate, getRange, getFrequencyOptions } from '../src/PeriodPicker/lib';
import { newDate } from '../src/utils';

const theme = createTheme();
const props = {
  defaultFrequency: 'B',
  availableFrequencies: ['S', 'Q', 'M', 'W', 'D', 'H', 'N', 'B'],
  boundaries: [new Date('2000-01-01T00:00:00.000Z'), new Date('2020-01-01T00:00:00.000Z')],
  period: [new Date('2000-01-01T00:00:00.000Z'), new Date('2020-01-01T00:00:00.000Z')],
  labels: {
    frequency: 'Frequency',
    year: 'year',
    semester: 'semester',
    quarter: 'quarter',
    month: 'month',
    week: 'week',
    hour: 'hour',
    minute: 'minute',
    full_hour: 'full_hour',
    day: 'day',
    end: 'end',
    start: 'start',
    A: 'Annual',
    S: 'Half-Yearly',
    Q: 'Quarterly',
    M: 'Monthly',
    W: 'Weekly',
    D: 'Daily',
    H: 'Hourly',
    N: 'Minutely',
    B: 'Business',
  },
};

describe('PeriodPicker component', () => {
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PeriodPicker {...props} isBlank />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PeriodPicker {...props} periodDisabled frequencyDisabled />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });
  it('should render only period part', () => {
    const { getByTestId, queryByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PeriodPicker {...props} frequencyDisabled />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(getByTestId('period-picker-start-period-test-id')).toBeInTheDocument();
    expect(getByTestId('period-picker-end-period-test-id')).toBeInTheDocument();
    expect(queryByTestId('period-picker-frequency-test-id')).not.toBeInTheDocument()
  });
  it('should render only frequency part', () => {
    const { getByTestId, queryByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PeriodPicker {...props} periodDisabled />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(queryByTestId('period-picker-period-test-id')).not.toBeInTheDocument();
    expect(getByTestId('period-picker-frequency-test-id')).toBeInTheDocument()
  });
  it('should render annual', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PeriodPicker
            {...props}
            defaultFrequency='A'
            period={[newDate('2018-01-01T15:00:00.000Z'), newDate('2019-12-31T00:00:00.000Z')]}
          />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(container.querySelector('#year-start').textContent).toEqual('2018');
    expect(container.querySelector('#year-end').textContent).toEqual('2019');
  });
  it('should render undefined value', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <PeriodPicker
            {...props}
            defaultFrequency='W'
            period={[undefined, undefined]}
          />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container.querySelector('#year-start').textContent).toEqual('----');
    expect(container.querySelector('#week-start').textContent).toEqual('--');
    expect(container.querySelector('#year-end').textContent).toEqual('----');
    expect(container.querySelector('#week-end').textContent).toEqual('--');
  });
});

describe('getDate', () => {
  const dateValues = {
    "year": 2019,
    "month": 1,
    "day": 7,
    "hour": 0,
    "minute": 0,
    "full_hour": 0,
    "quarter": 1,
    "week": 2,
    "semester": 1
  };
  it('get week date', () => {
    expect(getDate('W', dateValues, undefined)).toEqual(undefined);
    expect(getDate('A', dateValues, { year: 2018 })).toEqual(newDate("2018-01-07T00:00:00.000Z"));
    expect(getDate('A', dateValues, { year: 2018 }, true)).toEqual(newDate("2018-01-07T00:00:00.000Z"));
    expect(getDate('A', {}, { year: 2018 }, true)).toEqual(newDate("2018-12-31T23:59:00.000Z"));
    expect(getDate('S', dateValues, { semester: 2 })).toEqual(newDate("2019-06-31T00:00:00.000Z"));
    expect(getDate('S', dateValues, { semester: 2 }, true)).toEqual(newDate("2019-06-31T00:00:00.000Z"));
    expect(getDate('S', {}, { year: 2019 }, true)).toEqual(newDate("2019-06-31T00:00:00.000Z"));
    expect(getDate('Q', dateValues, { quarter: 2 })).toEqual(newDate("2019-04-01T00:00:00.000Z"));
    expect(getDate('Q', dateValues, { quarter: 2 }, true)).toEqual(newDate("2019-04-01T00:00:00.000Z"));
    expect(getDate('Q', {}, { year: 2019 }, true)).toEqual(newDate("2019-09-31T00:00:00.000Z"));
    expect(getDate('M', dateValues, { month: 2 })).toEqual(newDate("2019-02-07T00:00:00.000Z"));
    expect(getDate('M', dateValues, { month: 2 }, true)).toEqual(newDate("2019-02-07T00:00:00.000Z"));
    expect(getDate('M', {}, { year: 2019 }, true)).toEqual(newDate("2019-12-31T23:59:00.000Z"));
    expect(getDate('W', dateValues, { week: 1 })).toEqual(newDate("2019-01-06T23:59:59.999Z"));
    expect(getDate('W', dateValues, { week: 1 }, true)).toEqual(newDate("2019-01-06T23:59:59.999Z"));
    expect(getDate('W', dateValues, { week: 53 })).toEqual(newDate("2019-12-23T00:00:00.000Z"));
    expect(getDate('W', dateValues, { week: 53 }, true)).toEqual(newDate("2019-12-23T00:00:00.000Z"));
    expect(getDate('W', {}, { year: 2018 })).toEqual(newDate("2018-01-01T00:00:00.000Z"));
    expect(getDate('W', {}, { year: 2019 }, true)).toEqual(newDate("2019-12-23T00:00:00.000Z"));
    expect(getDate('D', dateValues, { day: 15 })).toEqual(newDate("2019-01-15T00:00:00.000Z"));
    expect(getDate('D', dateValues, { day: 15 }, true)).toEqual(newDate("2019-01-15T00:00:00.000Z"));
    expect(getDate('H', dateValues, { full_hour: 15 })).toEqual(newDate("2019-01-07T15:00:00.000Z"));
    expect(getDate('H', dateValues, { full_hour: 15 }, true)).toEqual(newDate("2019-01-07T15:00:00.000Z"));
    expect(getDate('M', dateValues, { hour: 14 })).toEqual(newDate("2019-01-07T14:00:00.000Z"));
    expect(getDate('M', dateValues, { hour: 14 }, true)).toEqual(newDate("2019-01-07T14:00:00.000Z"));
    expect(getDate('M', dateValues, { minute: 15 })).toEqual(newDate("2019-01-07T00:15:00.000Z"));
    expect(getDate('M', dateValues, { minute: 15 }, true)).toEqual(newDate("2019-01-07T00:15:00.000Z"));
  });
});

describe('getRange', () => {
  const list = [
    [
      ["2018-05-15T10:25:00.000Z", "2020-01-01T00:00:00.000Z"],
      "2018-05-15T10:52:00.000Z",
      { expectedYears: [2018, 2020], expectedMonths: [5, 12], expectedDays: [15,31], expectedHours: [10, 23], expectedMinutes: [25, 59] }
    ],
    [
      ["2018-05-15T10:25:00.000Z", "2020-01-01T00:00:00.000Z"],
      "2019-05-15T10:52:00.000Z",
      { expectedYears: [2018, 2020], expectedMonths: [1, 12], expectedDays: [1,31], expectedHours: [0, 23], expectedMinutes: [0, 59] }
    ],
    [
      ["2018-01-01T00:00:00.000Z", "2020-01-01T00:32:00.000Z"],
      "2020-01-01T00:00:00.000Z",
      { expectedYears: [2018, 2020], expectedMonths: [1, 1], expectedDays: [1, 1], expectedHours: [0, 0], expectedMinutes: [0, 32] }
    ],
    [
      ["2018-01-01T00:00:00.000Z", "2020-01-01T00:00:00.000Z"],
      "2018-01-01T00:00:00.000Z",
      { expectedYears: [2018, 2020], expectedMonths: [1, 12], expectedDays: [1, 31], expectedHours: [0, 23], expectedMinutes: [0, 59] }
    ],
    [
      ["2018-06-10T10:00:00.000Z", "2018-06-10T12:00:00.000Z"],
      "2018-06-10T11:00:00.000Z",
      { expectedYears: [2018, 2018], expectedMonths: [6, 6], expectedDays: [10, 10], expectedHours: [10, 12], expectedMinutes: [0, 59] }
    ],
    [
      ["2018-06-10T10:10:00.000Z", "2018-06-10T10:30:00.000Z"],
      "2018-06-10T10:15:00.000Z",
      { expectedYears: [2018, 2018], expectedMonths: [6, 6], expectedDays: [10, 10], expectedHours: [10, 10], expectedMinutes: [10, 30] }
    ],
  ];

  it('years', () => {
    R.forEach(([boundaries, stringDate, { expectedYears }]) => {
      const datesBoundaries = R.map(date => newDate(date), boundaries);
      const date = newDate(stringDate);
      const { years } = getRange(datesBoundaries, date);
      expect(years).toEqual(expectedYears);
    })(list)
  });

  it('months', () => {
    R.forEach(([boundaries, stringDate, { expectedMonths }]) => {
      const datesBoundaries = R.map(date => newDate(date), boundaries);
      const date = newDate(stringDate);
      const { months } = getRange(datesBoundaries, date);
      expect(months).toEqual(expectedMonths);
    })(list)
  });

  it('days', () => {
    R.forEach(([boundaries, stringDate, { expectedDays }]) => {
      const datesBoundaries = R.map(date => newDate(date), boundaries);
      const date = newDate(stringDate);
      const { days } = getRange(datesBoundaries, date);
      expect(days).toEqual(expectedDays);
    })(list)
  });

  it('hours', () => {
    R.forEach(([boundaries, stringDate, { expectedHours }]) => {
      const datesBoundaries = R.map(date => newDate(date), boundaries);
      const date = newDate(stringDate);
      const { hours, full_hours } = getRange(datesBoundaries, date);
      expect(hours).toEqual(expectedHours);
      expect(full_hours).toEqual(expectedHours);
    })(list)
  });

  it('minutes', () => {
    R.forEach(([boundaries, stringDate, { expectedMinutes }]) => {
      const datesBoundaries = R.map(date => newDate(date), boundaries);
      const date = newDate(stringDate);
      const { minutes } = getRange(datesBoundaries, date);
      expect(minutes).toEqual(expectedMinutes);
    })(list)
  });
});

describe('getFrequencyOptions', () => {
  const labels = {
    'Y': 'year', // not supported check layoutFrequencies src/PeriodPicker/constants.js
    'M': 'month',
    'D': 'day',
    'A': 'annual',
  };

  it('should return correct frequency options', () => {
    expect(getFrequencyOptions(['M', 'Y', 'D'], labels)).toEqual([
      { value: 'M', label: 'month' },
      { value: 'D', label: 'day' },
    ]);
  });
  it('should fallback to annual', () => {
    expect(getFrequencyOptions(['toto'], labels)).toEqual([
      { value: 'A', label: 'annual' },
    ]);
  });
});
