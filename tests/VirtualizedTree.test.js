import React from 'react';
import R from 'ramda';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { render } from "./mockProvider";
import Item from '../src/VirtualizedTree/Item';
import { getDeprecatedIds, getDepth, getEvolvedDisableAccessor, getHierarchicalId } from '../src/VirtualizedTree/utils';
import { getDisplayedIds, getHasChildrenOnLevel } from '../src/VirtualizedTree/VirtualizedTree';
import { spotlightFilter } from "../src/VirtualizedTree/withSpotlight";


describe('VirtualizedTree Item render', () => {
  const listeners = {
    onMouseUp: jest.fn(),
    onMouseDown: jest.fn(),
    onMouseEnter: jest.fn()
  };
  const list = document.createElement('ul');
  const { container } = render(
    <Item
      id="item"
      classes={{}}
      label="selected item"
      depth={2}
      hasChild={true}
      isSelected={false}
      NavigateIcon={ExpandMoreIcon}
      eventsListeners={listeners}
    />,
  { container: document.body.appendChild(list) });
  it('should render', () => {
    expect(container.firstChild).toMatchSnapshot();
  });
});

describe('VirtualizedTree utils tests', () => {
  const items = {
    '#ROOT': [
      { id: '0' },
      { id: '1' }
    ],
    '0': [
      { id: '0.0', parentId: '0' },
      { id: '0.1', parentId: '0' },
      { id: '0.2', parentId: '0' },
    ],
    '0.2': [
      { id: '0.2.0', parentId: '0.2' },
      { id: '0.2.1', parentId: '0.2' },
      { id: '0.2.2', parentId: '0.2' },
    ],
  };
  it('getDisplayedIds no expanded case', () => {
    const displayed = getDisplayedIds(R.prop('#ROOT', items), items, {});
    expect(displayed).toEqual(['0', '1']);
  });
  it('getDisplayedIds first parent expanded', () => {
    const displayed = getDisplayedIds(R.prop('#ROOT', items), items, { '0': '0' });
    expect(displayed).toEqual(['0', '0.0', '0.1', '0.2', '1']);
  });
  it('getHasChildrenOnLevel children on level', () => {
    expect(getHasChildrenOnLevel(items)({ id: '0' })).toEqual(true);
  });
  it('getHasChildrenOnLevel no children on level', () => {
    expect(getHasChildrenOnLevel(items)({ id: '0.2.2', parentId: '0.2' })).toEqual(false);
  });
  const indexedItems = {
    '0': { id: '0' },
    '0.0': { id: '0.0', parentId: '0' },
    '0.1': { id: '0.1', parentId: '0' },
    '0.2': { id: '0.2', parentId: '0' },
    '0.2.0': { id: '0.2.0', parentId: '0.2' },
    '0.2.1': { id: '0.2.1', parentId: '0.2' },
    '0.2.2': { id: '0.2.2', parentId: '0.2' },
    '1': { id: '1' },
  };
  it('getDepth root level', () => {
    expect(getDepth(indexedItems)({ id: '0' })).toEqual(0);
  });
  it('getDepth second level', () => {
    expect(getDepth(indexedItems)({ id: '0.2.2', parentId: '0.2' })).toEqual(2);
  });
  const disableAccessor = R.propOr(false, 'disabled');

  const groupedItems = {
    '#ROOT': [
      { id: 'ROOT1', disabled: true, parentId: undefined },
      { id: 'ROOT2', disabled: true, parentId: undefined },
    ],
    ROOT2: [
      { id: 'CHILD1', parentId: 'ROOT2' },
      { id: 'CHILD2', parentId: 'ROOT2' },
      { id: 'CHILD3', disabled: true, parentId: 'ROOT2' },
    ],
    CHILD2: [
      { id: 'CHILD2.1', parentId: 'CHILD2' },
      { id: 'CHILD2.2', parentId: 'CHILD2' },
      { id: 'CHILD2.3', parentId: 'CHILD2' },
    ],
    CHILD3: [
      { id: 'CHILD3.1', disabled: true, parentId: 'CHILD3' },
      { id: 'CHILD3.2', disabled: true, parentId: 'CHILD3' },
      { id: 'CHILD3.3', disabled: true, parentId: 'CHILD3' },
    ]
  };

  const selectedIds = {};
  const singleModeDisableAccessor = getEvolvedDisableAccessor(groupedItems, disableAccessor, R.always(false));
  const childrenModeDisableAccessor = getEvolvedDisableAccessor(groupedItems, disableAccessor, (child, anc) => child.parentId === getHierarchicalId(anc));
  it('singleModeDisableAccessor ---> disabled item without children stay disabled', () => {
    expect(singleModeDisableAccessor({ id: 'ROOT1', disabled: true }, selectedIds)).toEqual(true);
  });
  it('singleModeDisableAccessor ---> disabled item with children but none selected stay disabled', () => {
    expect(singleModeDisableAccessor({ id: 'ROOT2', disabled: true }, selectedIds)).toEqual(true);
  });
  it('singleModeDisableAccessor ---> disabled item with selected child not disabled', () => {
    expect(singleModeDisableAccessor({ id: 'ROOT2', disabled: true }, { CHILD1: 'CHILD1' })).toEqual(false);
  });
  it('childrenModeDisableAccessor ---> disabled item without children stay disabled', () => {
    expect(childrenModeDisableAccessor({ id: 'ROOT1', disabled: true }, selectedIds)).toEqual(true);
  });
  it('childrenModeDisableAccessor ---> disabled item with direct children in scope not disabled', () => {
    expect(childrenModeDisableAccessor({ id: 'ROOT2', disabled: true }, selectedIds)).toEqual(false);
  });
  it('childrenModeDisableAccessor ---> disabled item without children in scope and non selected stay disabled', () => {
    const spotlightedItems = {
      ROOT1: { id: 'ROOT1', disabled: true },
      ROOT2: { id: 'ROOT2', disabled: true },
    };
    const accessor = getEvolvedDisableAccessor(groupedItems, disableAccessor, (child, anc) => 
      R.has(getHierarchicalId(child), spotlightedItems) && child.parentId === getHierarchicalId(anc));
    expect(accessor({ id: 'ROOT2', disabled: true }, {})).toEqual(true);
  });
  it('childrenModeDisableAccessor ---> disabled item without children in scope but some selected not disabled', () => {
    const spotlightedItems = {
      ROOT1: { id: 'ROOT1', disabled: true },
      ROOT2: { id: 'ROOT2', disabled: true },
    };
    const accessor = getEvolvedDisableAccessor(groupedItems, disableAccessor, (child, anc) => 
      R.has(getHierarchicalId(child, spotlightedItems)) && child.parentId === getHierarchicalId(anc));
    expect(accessor({ id: 'ROOT2', disabled: true }, { CHILD1: 'CHILD1' })).toEqual(false);
  });
  it('childrenModeDisableAccessor ---> disabled item with children in scope but all disabled and none selected stay disabled', () => {
    const spotlightedItems = {
      ROOT1: { id: 'ROOT1', disabled: true },
      ROOT2: { id: 'ROOT2', disabled: true },
      CHILD1: { id: 'CHILD1' },
      CHILD2: { id: 'CHILD2' },
      CHILD3: { id: 'CHILD3', disabled: true },
      'CHILD3.1': { id: 'CHILD3.1', disabled: true },
      'CHILD3.2': { id: 'CHILD3.2', disabled: true },
      'CHILD3.3': { id: 'CHILD3.3', disabled: true },
    };
    const accessor = getEvolvedDisableAccessor(groupedItems, disableAccessor, (child, anc) => 
      R.has(getHierarchicalId(child, spotlightedItems)) && child.parentId === getHierarchicalId(anc));
    expect(accessor({ id: 'CHILD3', disabled: true }, {})).toEqual(true);
  });
  it('fullHierarchyModeDisableAccessor ---> disabled item without children stay disabled', () => {
    const accessor = getEvolvedDisableAccessor(groupedItems, disableAccessor, R.always(true));
    expect(accessor({ id: 'ROOT1', disabled: true }, selectedIds)).toEqual(true);
  });
   it('fullHierarchyModeDisableAccessor ---> disabled item with descendants not disabled', () => {
    const accessor = getEvolvedDisableAccessor(groupedItems, disableAccessor, R.always(true));
    expect(accessor({ id: 'ROOT2', disabled: true }, selectedIds)).toEqual(false);
  });
  it('fullHierarchyModeDisableAccessor ---> disabled item with children in scope but all disabled and none selected stay disabled', () => {
    const spotlightedItems = {
      ROOT1: { id: 'ROOT1', disabled: true },
      ROOT2: { id: 'ROOT2', disabled: true },
      CHILD1: { id: 'CHILD1' },
      CHILD2: { id: 'CHILD2' },
      CHILD3: { id: 'CHILD3', disabled: true },
      'CHILD3.1': { id: 'CHILD3.1', disabled: true },
      'CHILD3.2': { id: 'CHILD3.2', disabled: true },
      'CHILD3.3': { id: 'CHILD3.3', disabled: true },
    };
    const accessor = getEvolvedDisableAccessor(groupedItems, disableAccessor, child => R.has(getHierarchicalId(child), spotlightedItems));
    expect(accessor({ id: 'CHILD3', disabled: true }, {})).toEqual(true);
  });
  it('getDeprecatedIds', () => {
    const items = [
      { id: 'ROOT', isGreyed: true },
      { id: 'CHILD', parentId: 'ROOT', isGreyed: true },
      { id: 'GRANDCHILD', parentId: 'CHILD' },
      { id: 'OTHER_GRANDCHILD', parentId: 'CHILD' }
    ];
    const indexedItemsById = R.indexBy(R.prop('id'), items);
    const groupedItemsByParentId = R.groupBy(R.propOr('#ROOT', 'parentId'), items);
    expect(getDeprecatedIds(['GRANDCHILD'], { indexedItemsById, groupedItemsByParentId }, { GRANDCHILD: 'GRANDCHILD' })).toEqual([]);
    expect(getDeprecatedIds(['GRANDCHILD'], { indexedItemsById, groupedItemsByParentId }, { GRANDCHILD: 'GRANDCHILD', ROOT: 'ROOT' })).toEqual(['ROOT']);
    expect(getDeprecatedIds(['GRANDCHILD'], { indexedItemsById, groupedItemsByParentId }, { GRANDCHILD: 'GRANDCHILD', ROOT: 'ROOT', CHILD: 'CHILD' })).toEqual(['CHILD', 'ROOT']);
    expect(getDeprecatedIds(['GRANDCHILD'], { indexedItemsById, groupedItemsByParentId }, { GRANDCHILD: 'GRANDCHILD', ROOT: 'ROOT', OTHER_GRANDCHILD: 'OTHER_GRANDCHILD' })).toEqual([]);
  });
  it('should return all children associated to the item matching the term when it is a parent', () => {
    const spotlightedItems = [
      { id: 'ROOT1', label: 'ROOT1' },
      { id: 'ROOT2', label: 'ROOT2' },
      { id: 'CHILD1', label: 'CHILD1', parentId: 'ROOT1' },
      { id: 'CHILD2', label: 'CHILD2', parentId: 'ROOT2' },
      { id: 'CHILD3', label: 'CHILD3', parentId: 'ROOT1' },
      { id: 'CHILD3.1', label: 'CHILDA3.1', parentId: 'CHILD3' },
      { id: 'CHILD3.2', label: 'CHILDB3.2', parentId: 'CHILD3' },
      { id: 'CHILD3.3', label: 'CHILDC3.3', parentId: 'CHILD3', disabled: true }]
    const res = [
      { id: 'ROOT1', label: 'ROOT1' },
      { id: 'CHILD3', label: 'CHILD3', parentId: 'ROOT1', "highlightedLabel": "<span style=background-color:yellow;border-bottom:2px solid #8CC841>CHILD3</span>" },
      { id: 'CHILD3.1', label: 'CHILDA3.1', parentId: 'CHILD3' },
      { id: 'CHILD3.2', label: 'CHILDB3.2', parentId: 'CHILD3' },
      { id: 'CHILD3.3', label: 'CHILDC3.3', parentId: 'CHILD3', disabled: true }]

    expect(spotlightFilter('CHILD3', R.prop('label'), true)(spotlightedItems)).toEqual(res);

  }),
    it('should return all children associated to the item matching the term when it is a parent and highlighted matching terms', () => {
      const spotlightedItems = [
        { id: 'ROOT1', label: 'ROOT1' },
        { id: 'ROOT2', label: 'ROOT2' },
        { id: 'CHILD1', label: 'CHILD1', parentId: 'ROOT1' },
        { id: 'CHILD2', label: 'CHILD2', parentId: 'ROOT2' },
        { id: 'CHILD3', label: 'CHILD3', parentId: 'ROOT1' },
        { id: 'CHILD3.1', label: 'CHILDA3.1', parentId: 'CHILD3' },
        { id: 'CHILD3.2', label: 'CHILDB3.2', parentId: 'CHILD3' },
        { id: 'CHILD3.3', label: 'CHILDC3.3', parentId: 'CHILD3', disabled: true },
        { id: 'CHILD3.4', label: 'CHILD3.4', parentId: 'CHILD3' },
      ]
      const res = [
        { id: 'ROOT1', label: 'ROOT1' },
        { id: 'CHILD3', label: 'CHILD3', parentId: 'ROOT1', "highlightedLabel": "<span style=background-color:yellow;border-bottom:2px solid #8CC841>CHILD3</span>" },
        { id: 'CHILD3.1', label: 'CHILDA3.1', parentId: 'CHILD3' },
        { id: 'CHILD3.2', label: 'CHILDB3.2', parentId: 'CHILD3' },
        { id: 'CHILD3.3', label: 'CHILDC3.3', parentId: 'CHILD3', disabled: true },
        { id: 'CHILD3.4', label: 'CHILD3.4', parentId: 'CHILD3', "highlightedLabel": "<span style=background-color:yellow;border-bottom:2px solid #8CC841>CHILD3</span>.4" },
      ]

      expect(spotlightFilter('CHILD3', R.prop('label'), true)(spotlightedItems)).toEqual(res);

    }),
  it('should NOT return children associated to the item matching the term when it is a parent', () => {
    const spotlightedItems = [
      { id: 'ROOT1', label: 'ROOT1' },
      { id: 'ROOT2', label: 'ROOT2' },
      { id: 'CHILD1', label: 'CHILD1', parentId: 'ROOT1' },
      { id: 'CHILD2', label: 'CHILD2', parentId: 'ROOT2' },
      { id: 'CHILD3', label: 'CHILD3', parentId: 'ROOT1' },
      { id: 'CHILD3.1', label: 'CHILDA3.1', parentId: 'CHILD3' },
      { id: 'CHILD3.2', label: 'CHILDB3.2', parentId: 'CHILD3' },
      { id: 'CHILD3.3', label: 'CHILDC3.3', parentId: 'CHILD3', disabled: true }]
    const res = [
      { id: 'ROOT1', label: 'ROOT1' },
      { id: 'CHILD3', label: 'CHILD3', parentId: 'ROOT1', highlightedLabel: "<span style=background-color:yellow;border-bottom:2px solid #8CC841>CHILD3</span>" },]

    expect(spotlightFilter('CHILD3', R.prop('label'), false)(spotlightedItems)).toEqual(res);

  }),
    it('should NOT return children associated to the item matching the term when it is a parent and should return and highlighted matching terms', () => {
      const spotlightedItems = [
        { id: 'ROOT1', label: 'ROOT1' },
        { id: 'ROOT2', label: 'ROOT2' },
        { id: 'CHILD1', label: 'CHILD1', parentId: 'ROOT1' },
        { id: 'CHILD2', label: 'CHILD2', parentId: 'ROOT2' },
        { id: 'CHILD3', label: 'CHILD3', parentId: 'ROOT1' },
        { id: 'CHILD3.1', label: 'CHILDA3.1', parentId: 'CHILD3' },
        { id: 'CHILD3.2', label: 'CHILDB3.2', parentId: 'CHILD3' },
        { id: 'CHILD3.3', label: 'CHILDC3.3', parentId: 'CHILD3', disabled: true },
        { id: 'CHILD3.4', label: 'CHILD3.4', parentId: 'CHILD3', },
        { id: 'CHILD3.4.1', label: 'CHILD3.4.1', parentId: 'CHILD3.4', },

      ]
      const res = [
        { id: 'ROOT1', label: 'ROOT1' },
        { id: 'CHILD3', label: 'CHILD3', parentId: 'ROOT1', highlightedLabel: "<span style=background-color:yellow;border-bottom:2px solid #8CC841>CHILD3</span>" },
        { id: 'CHILD3.4', label: 'CHILD3.4', parentId: 'CHILD3', highlightedLabel: "<span style=background-color:yellow;border-bottom:2px solid #8CC841>CHILD3</span>.4" },
        { id: 'CHILD3.4.1', label: 'CHILD3.4.1', parentId: 'CHILD3.4', highlightedLabel: "<span style=background-color:yellow;border-bottom:2px solid #8CC841>CHILD3</span>.4.1" },
      ]

    expect(spotlightFilter('CHILD3', R.prop('label'), false)(spotlightedItems)).toEqual(res);

  })
});
