import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { TableLayout } from '../src';

const theme = createTheme();
const props = {
  items: [
    {
      id: 'REF_AREA',
      value: 'REF_AREA',
      name: 'Reference area',
      count: 3,
      values: [
        {
          id: 'ISR',
          name: 'Israel',
          label: 'Xxxx',
        },
        {
          id: 'RWA',
          name: 'Rwanda',
          label: 'Xxxx',
        },
        {
          id: 'BLR',
          name: 'Belarus',
          label: 'Xxxx',
        },
      ],
    },
    {
      id: 'SURVEY',
      value: 'SURVEY',
      name: 'Survey',
      count: 3,
      values: [
        {
          id: '706',
          name: 'Labour Force Survey',
          label: 'Xxxx',
        },
        {
          id: '1590',
          name: 'Labour force survey',
          label: 'Xxxx',
        },
        {
          id: '486',
          name: 'Employment office records',
          label: 'Xxxx',
        },
      ],
    },
    {
      id: 'SEX',
      value: 'SEX',
      name: 'Sex',
      count: 2,
      values: [
        {
          id: 'SEX_T',
          name: 'Total',
          label: 'Xxxx',
        },
        {
          id: 'SEX_M',
          name: 'Male',
          label: 'Xxxx',
        },
      ],
    },
    {
      id: 'TIME_PERIOD',
      value: 'TIME_PERIOD',
      name: 'Time period',
      count: 2,
      values: [
        {
          id: '2017',
          name: '2017',
          label: 'Xxxx',
        },
        {
          id: '2018',
          name: '2018',
          label: 'Xxxx',
        },
      ],
    },
    {
      id: 'DSB',
      name: 'Disability status',
      count: 3,
      values: [
        {
          id: 'DSB_STATUS_TOTAL',
          name: 'Total',
          label: 'Xxxx',
        },
        {
          id: 'DSB_STATUS_DIS',
          name: 'Persons with disability',
          label: 'Xxxx',
        },
        {
          id: 'DSB_STATUS_NODIS',
          order: 2,
          name: 'Persons without disability',
          label: 'Xxxx',
        },
      ],
    },
  ],
  layout: {
    sections: ['REF_AREA', 'SURVEY'],
    rows: ['SEX', 'TIME_PERIOD'],
    header: ['DSB'],
  },
  labels: {
    commit: 'Apply layout',
    cancel: 'Cancel changes',
    row: 'Rows',
    column: 'Columns',
    section: 'Sections',
    asc: 'asc',
    desc: 'desc',
    help: 'Information DnD',
    one: 'done',
    table: 'Table Preview',
  },
  itemButtonProps: { TIME_PERIOD: { options: ['asc', 'desc'], value: 'asc', onChange: jest.fn() } },
  commit: jest.fn(),
};

describe('TableLayout component', () => {
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <TableLayout isBlank />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <TableLayout {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('table-layout-test-id')).toBeInTheDocument();
  });
});
