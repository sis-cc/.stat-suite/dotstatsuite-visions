import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { CollapsibleTree } from '../src';
import { length } from 'ramda';

const theme = createTheme();
const data  = [
  {
    id: 1,
    label: 'Inward activity of multinationals by industrial sector - ISIC Rev 4',
    children: [
      {
        id: 10,
        label: 'Data Characteristics',
        children: [
          {
            id: 100,
            label: 'Date last updated',
            value: 'August 2017',
          },
          {
            id: 101,
            label: 'Unit of measure used',
            value: 'The units used to present data in AFA are millions of national currency for monetary variables and units for the other variables. Monetary variables are in current prices. Euro-area countries: national currency data is expressed in euro beginning with the year of entry into the Economic and Monetary Union (EMU). For years prior to the year of entry into EMU, data have been converted from the former national currency using the appropriate irrevocable conversion rate. This presentation facilitates comparisons within a country over time and ensures that the historical evolution is preserved. Please note, however, that pre-EMU euro are a notional unit and should not be used to form area aggregates or to carry out cross-country comparisons.',
          }
        ]
      },
      {
        id: 11,
        label: 'Database Specific',
        children: [
          {
            id: 110,
            label: 'Date last updated',
            value: 'August 2015',
          },
          {
            id: 111,
            label: 'Abstract',
            value: 'This table contains figures on the activity of affiliates under foreign control and all firms by industry according to the International Standard Industrial Classification (ISIC Revision 4).'
          }
        ]
      }
    ],
  },
  {
    id: 2,
    label: 'Health spending',
    children: [
      {
        id: 20,
        label: 'Data Characteristics',
        children: [
          {
            id: 200,
            label: 'Date last updated',
            value: 'August 2017',
          },
          {
            id: 201,
            label: 'Unit of measure used',
            value: 'The units used to present data in AFA are millions of national currency for monetary variables and units for the other variables. Monetary variables are in current prices. Euro-area countries: national currency data is expressed in euro beginning with the year of entry into the Economic and Monetary Union (EMU). For years prior to the year of entry into EMU, data have been converted from the former national currency using the appropriate irrevocable conversion rate. This presentation facilitates comparisons within a country over time and ensures that the historical evolution is preserved. Please note, however, that pre-EMU euro are a notional unit and should not be used to form area aggregates or to carry out cross-country comparisons.',
          }
        ]
      },
      {
        id: 21,
        label: 'Database Specific',
        children: [
          {
            id: 210,
            label: 'Date last updated',
            value: 'August 2015',
          },
          {
            id: 211,
            label: 'Abstract',
            value: 'This table contains figures on the health spending of the European Union by industry according to the International Standard Industrial Classification (ISIC Revision 4).'
          }
        ]
      }
    ]
  },
  {
    id: 3,
    label: 'Health spending by sector',
    children: [
      {
        id: 30,
        label: 'Data Characteristics',
        children: [
          {
            id: 300,
            label: 'Date last updated',
            value: 'August 2017',
          },
          {
            id: 301,
            label: 'Unit of measure used',
            value: 'The units used to present data in AFA are millions of national currency for monetary variables and units for the other variables. Monetary variables are in current prices. Euro-area countries: national currency data is expressed in euro beginning with the year of entry into the Economic and Monetary Union (EMU). For years prior to the year of entry into EMU, data have been converted from the former national currency using the appropriate irrevocable conversion rate. This presentation facilitates comparisons within a country over time and ensures that the historical evolution is preserved. Please note, however, that pre-EMU euro are a notional unit and should not be used to form area aggregates or to carry out cross-country comparisons.',
          }
        ]
      }
    ]
  }
];

describe('CollapsibleTree', () => {
  it('renders without crashing', () => {
    render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <CollapsibleTree />
        </ThemeProvider>
      </StyledEngineProvider>
    );
  });
  it('renders without crashing', () => {
    render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <CollapsibleTree data={data} />
        </ThemeProvider>
      </StyledEngineProvider>
    );
  });
});

