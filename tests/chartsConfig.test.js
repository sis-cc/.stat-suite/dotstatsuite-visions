import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { ChartsConfig } from '../src';

const theme = createTheme();
const props = {
  properties: {
    highlight: {
      id: 'highlight',
      isActive: true,
      options: [
        {
          value: 'focus-id-1',
          label: 'Focus-label-1',
        },
        {
          value: 'focus-id-2',
          label: 'Focus-label-2',
        },
        {
          value: 'focus-id-3',
          label: 'Focus-label-3',
        },
        {
          value: 'focus-id-4',
          label: 'Focus-label-4',
        },
        {
          value: 'focus-id-5',
          label: 'Focus-label-5',
        },
      ],
      value: [
        {
          value: 'focus-id-1',
          label: 'focus-label-1',
        },
        {
          value: 'focus-id-2',
          label: 'focus-label-2',
        },
      ],
    },
    baseline: {
      id: 'baseline',
      isActive: true,
      options: [
        {
          value: 'focus-id-1',
          label: 'Focus-label-1',
        },
        {
          value: 'focus-id-2',
          label: 'Focus-label-2',
        },
        {
          value: 'focus-id-3',
          label: 'Focus-label-3',
        },
        {
          value: 'focus-id-4',
          label: 'Focus-label-4',
        },
        {
          value: 'focus-id-5',
          label: 'Focus-label-5',
        },
      ],
      value: {
        value: 'focus-id-3',
        label: 'focus-label-3',
      },
    },
    width: {
      id: 'width',
      isActive: true,
      value: null,
    },
    height: {
      id: 'height',
      isActive: true,
      value: null,
    },
    freqStep: {
      id: 'freqStep',
      isActive: true,
      value: null,
    },
    scatterDimension: {
      id: 'scatterDimension',
      isActive: true,
      options: [
        {
          value: 'dim-id-1',
          label: 'Dim-label-1',
        },
        {
          value: 'dim-id-2',
          label: 'Dim-label-2',
        },
        {
          value: 'dim-id-3',
          label: 'Dim-label-3',
        },
        {
          value: 'dim-id-4',
          label: 'Dim-label-4',
        },
        {
          value: 'dim-id-5',
          label: 'Dim-label-5',
        },
      ],
      value: {
        value: 'dim-id-1',
        label: 'dim-label-1',
      },
    },
    scatterX: {
      id: 'scatterX',
      isActive: true,
      options: [
        {
          value: 'dim-val-id-1',
          label: 'Dim-val-label-1',
        },
        {
          value: 'dim-val-id-2',
          label: 'Dim-val-label-2',
        },
        {
          value: 'dim-val-id-3',
          label: 'Dim-val-label-3',
        },
        {
          value: 'dim-val-id-4',
          label: 'Dim-val-label-4',
        },
        {
          value: 'dim-val-id-5',
          label: 'Dim-val-label-5',
        },
      ],
      value: {
        value: 'dim-val-id-1',
        label: 'dim-val-label-1',
      },
    },
    scatterY: {
      id: 'scatterY',
      isActive: true,
      options: [
        {
          value: 'dim-val-id-1',
          label: 'Dim-val-label-1',
        },
        {
          value: 'dim-val-id-2',
          label: 'Dim-val-label-2',
        },
        {
          value: 'dim-val-id-3',
          label: 'Dim-val-label-3',
        },
        {
          value: 'dim-val-id-4',
          label: 'Dim-val-label-4',
        },
        {
          value: 'dim-val-id-5',
          label: 'Dim-val-label-5',
        },
      ],
      value: {
        value: 'dim-val-id-2',
        label: 'dim-val-label-2',
      },
    },
    symbolDimension: {
      id: 'symbolDimension',
      isActive: true,
      options: [
        {
          value: 'dim-id-1',
          label: 'Dim-label-1',
        },
        {
          value: 'dim-id-2',
          label: 'Dim-label-2',
        },
        {
          value: 'dim-id-3',
          label: 'Dim-label-3',
        },
        {
          value: 'dim-id-4',
          label: 'Dim-label-4',
        },
        {
          value: 'dim-id-5',
          label: 'Dim-label-5',
        },
      ],
      value: {
        value: 'dim-id-2',
        label: 'dim-label-2',
      },
    },
    stackedDimension: {
      id: 'stackedDimension',
      isActive: true,
      options: [
        {
          value: 'dim-id-1',
          label: 'Dim-label-1',
        },
        {
          value: 'dim-id-2',
          label: 'Dim-label-2',
        },
        {
          value: 'dim-id-3',
          label: 'Dim-label-3',
        },
        {
          value: 'dim-id-4',
          label: 'Dim-label-4',
        },
        {
          value: 'dim-id-5',
          label: 'Dim-label-5',
        },
      ],
      value: {
        value: 'dim-id-3',
        label: 'dim-label-3',
      },
    },
    stackedMode: {
      id: 'stackedMode',
      isActive: true,
      options: [
        {
          value: 'values',
        },
        {
          value: 'percent',
        },
      ],
      value: 'percent',
    },
    maxX: {
      id: 'maxX',
      isActive: true,
      value: null,
    },
    minX: {
      id: 'minX',
      isActive: true,
      value: null,
    },
    pivotX: {
      id: 'pivotX',
      isActive: true,
      value: null,
    },
    stepX: {
      id: 'stepX',
      isActive: true,
      value: null,
    },
    maxY: {
      id: 'maxY',
      isActive: true,
      value: null,
    },
    minY: {
      id: 'minY',
      isActive: true,
      value: null,
    },
    pivotY: {
      id: 'pivotY',
      isActive: true,
      value: null,
    },
    stepY: {
      id: 'stepY',
      isActive: true,
      value: null,
    },
  },
  labels: {
    focus: 'Chart Focus',
    highlight: 'Highlights',
    baseline: 'Baseline',
    select: 'Select',
    size: 'Chart Size',
    width: 'Width',
    height: 'Height',
    series: 'Chart Series',
    scatterDimension: 'Scatter Dimension',
    scatterX: 'Scatter X',
    scatterY: 'Scatter Y',
    symbolDimension: 'Symbol Dimension',
    stackedDimension: 'Stacked Dimension',
    stackedMode: 'Stacked Mode',
    stackedModeOptions: {
      values: 'Values',
      percent: 'Percent',
    },
    axisX: 'Chart X Axis',
    axisY: 'Chart Y Axis',
    max: 'Max',
    min: 'Min',
    pivot: 'Pivot',
    step: 'Step',
    frequency: 'Frequency',
    freqStep: 'Frequency Step',
  },
};

describe('ChartsConfig component', () => {
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <ChartsConfig isBlank />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <ChartsConfig {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('charts-config-test-id')).toBeInTheDocument();
  });
});
