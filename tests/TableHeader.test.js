import React from 'react';
import { TableHeader } from '../src';
import { render } from "./mockProvider";


describe('TableHeader component', () => {
  it('should render', () => {
    const { container } = render(
      <TableHeader />
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
