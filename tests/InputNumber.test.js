import React from 'react';
import { render } from '@testing-library/react';
import { createTheme, ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { InputNumber } from '../src';

const theme = createTheme();
const props = {
  beforeLabel: 'Last',
  afterLabel: 'Period',
  popperLabel: 'Tooltip for more explantions',
  placeholder: '000',
  textFieldProps: {
    fullWidth: true,
  },
};

describe('InputNumber component', () => {
  it('should render nothing', () => {
    const { container } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <InputNumber isBlank />
        </ThemeProvider>
      </StyledEngineProvider>,
    );
    expect(container).toMatchInlineSnapshot('<div />');
  });
  it('should render', () => {
    const { getByTestId } = render(
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <InputNumber {...props} />
        </ThemeProvider>
      </StyledEngineProvider>,
    );

    expect(getByTestId('input-number-test-id')).toBeInTheDocument();
  });
});
