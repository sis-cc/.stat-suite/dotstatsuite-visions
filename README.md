# dotstatsuite-visions

Visions is a lib of React components.  
The goal is to collect pure visual UI components, decoupled from business logic.

## setup (dev)

1.  run `yarn`
2.  run `yarn build` to build demo website
3.  run `yarn start` to dev with hot reloading on http://localhost:3000

Others commands are describe in `package.json` below `scripts`.

## usage

1.  run `yarn add @sis-cc/dotstatsuite-ui-components`
1.  `import { MyComponent } from '@sis-cc/dotstatsuite-ui-components`;

## folder structure

```
.
├── demo/src
│   ├── components                    # demo website interface
│   ├── wrappers                      # component specific folder
│   │   ├── SomeComponent.js          # story for to view the component
├── src                               # source code
│   components                        # components
│   ├── SomeComponent                 # component specific folder
│   │   ├── index.js                  # component documentation
│   │   ├── SomeComponent.js          # component definition
│   ├── index.js                      # library interface
│   index.js                          # visions module interface (and entry point)
├── tests                             # tests
|   ├── SomeComponent.test.js         # test implementation of component
```
