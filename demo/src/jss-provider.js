import React from 'react';
import { create } from 'jss';
import rtl from 'jss-rtl';
import { StylesProvider, jssPreset } from '@mui/styles';

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

export const Rtl = props => <StylesProvider jss={jss}>{props.children}</StylesProvider>;
