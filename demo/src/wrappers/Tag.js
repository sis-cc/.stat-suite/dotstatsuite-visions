import React from 'react';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { Tag } from '../../../src';

const Wrapper = () => (
  <React.Fragment>
    <Divider />
    <Tag />
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {`<Tag />`}
    </SyntaxHighlighter>
    <Divider />
    <Tag>Hello World</Tag>
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {`<Tag>Hello World</Tag>`}
    </SyntaxHighlighter>
  </React.Fragment>
);

export default Wrapper;
