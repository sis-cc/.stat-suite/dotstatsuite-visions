import React, { useState, Fragment, useCallback } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import makeStyles from '@mui/styles/makeStyles';
import MuiInfoIcon from '@mui/icons-material//Info';
import IconButton from '@mui/material/IconButton';
import Usecase from '../components/Usecase';
import { TableHtml5 } from '../../../src';
import tableData from '../../../fixtures/table.json';
// import tableMetaDataFlagsValues from '../../../fixtures/table_flags_metadata.json';
import table4Rows from '../../../fixtures/table_4_rows.json';
import table3Sections from '../../../fixtures/table_3_sections.json';
import tableHeader from '../../../fixtures/table_header.json';
import tableFlags from '../../../fixtures/table_flags.json';
import tableFull from '../../../fixtures/table_full.json';
import table400 from '../../../fixtures/table_400.json';
import tableHierarchy from '../../../fixtures/table_hierarchy.json';
import tableLargeWidth from '../../../fixtures/table_large_width.json';
import tableLargeWidth3Cols from '../../../fixtures/table_large_width_3_cols.json';
import tableLong from '../../../fixtures/table_long.json';
import tableVertical from '../../../fixtures/table_large_vertical.json'
import tableLongTexts from '../../../fixtures/table_test-long-texts.json'
const useStyles = makeStyles(theme => ({
  icon: {
    color: theme.palette.primary.main,
    backgroundColor: 'inherit',
    padding: 0,
  },
  selected: {
    color: theme.palette.highlight.hl1,
    backgroundColor: 'black',
    padding: 0,
  }
}));

const SideIcon = ({ sideProps }) => {
  const classes = useStyles();
  if (R.isNil(sideProps)) {
    return null;
  }
  const onClick = () => {
    console.log('sideToggle', sideProps);
  };
  const isSelected = R.equals(R.prop('coordinates', sideProps || {}), { '1:0': '1:0' });
  return (
    <IconButton size="small" className={isSelected ? classes.selected : classes.icon} onClick={onClick} data-testid="ref-md-info">
      <MuiInfoIcon fontSize="small" />
    </IconButton>
  );
};

SideIcon.propTypes = {
  sideProps: PropTypes.object
};


const SanitizedInnerHTML = ({ html, ...rest }) => {
  return (
    <span dangerouslySetInnerHTML={{ __html: html }} {...rest} />
  );
};

SanitizedInnerHTML.propTypes = {
  html: PropTypes.string,
};

const snippets = [
  `<TableHtml5}
    cells={cells}
    headerData={headerData}
    sectionsData={sectionsData}
    isRtl={isRtl}
  />`,
  ];

const Wrapper = ({ isRtl }) => {
  const [activeCellIds, activeCellHandler] = useState({});
  const [loadAllUseCases, setLoadAllUseCases] = useState(false);
  const [renderSwitch, setRenderSwitch] = useState(false);
  const [HTMLRenderer, setHTMLRenderer] = useState(() => SanitizedInnerHTML);
  const labelAccessor = useCallback(R.prop('label'), []);
  const nameAccessor = useCallback(R.prop('name'), []);
  const { headerData, sectionsData, cells } = tableData;

  return (
    <Fragment>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Paper>
            <Button
              size="small"
              color="primary"
              variant='contained'
              style={{ margin: 4 }}
              onClick={() => setLoadAllUseCases(!loadAllUseCases)}
              >
              {loadAllUseCases ? 'Load few use cases' : 'Load all use cases (take some time)'}
            </Button>
            <Button
              size="small"
              color="primary"
              variant='contained'
              style={{ margin: 4 }}
              onClick={() => setHTMLRenderer(R.is(Function, HTMLRenderer) ? undefined : () => SanitizedInnerHTML)}
              >
              {R.is(Function, HTMLRenderer) ? 'Disable html renderer' : 'Enable html renderer'}
            </Button>
            <Button
              size="small"
              color="primary"
              variant='contained'
              style={{ margin: 4 }}
              onClick={() => setRenderSwitch(R.not)}
              >
              re-render!
            </Button>
          </Paper>
        </Grid>
      </Grid>
      <Usecase title="Usecase #1 1header/2sections/1row with active cell" snippet={snippets[0]}>
        <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableData} isRtl={isRtl} activeCellIds={activeCellIds} activeCellHandler={activeCellHandler} SideIcon={SideIcon} labelAccessor={labelAccessor} />
      </Usecase>
      <Usecase title="Usecase #1b 1header/2sections/1row with a cell handler">
        <TableHtml5 HTMLRenderer={HTMLRenderer} cells={cells} headerData={headerData} sectionsData={sectionsData} isRtl={isRtl} cellHandler={console.log} SideIcon={SideIcon} labelAccessor={labelAccessor} />
      </Usecase> 
      <Usecase defaultExpanded title="Usecase #2 rows">
        <TableHtml5 HTMLRenderer={HTMLRenderer} {...table4Rows} isRtl={isRtl} SideIcon={SideIcon} labelAccessor={labelAccessor} />
      </Usecase>
      <Usecase title="Usecase #3 sections">
        <TableHtml5 HTMLRenderer={HTMLRenderer} {...table3Sections} isRtl={isRtl} SideIcon={SideIcon} labelAccessor={labelAccessor} />
      </Usecase>
      <Usecase title="Usecase #4 header">
        <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableHeader} isRtl={isRtl} SideIcon={SideIcon} labelAccessor={labelAccessor} />
      </Usecase>
      <Usecase title="Usecase Hierarchy">
        <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableHierarchy} SideIcon={SideIcon} labelAccessor={labelAccessor} />
      </Usecase>
      <Usecase title="Usecase Hierarchy Vertical">
        <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableVertical} SideIcon={SideIcon} labelAccessor={labelAccessor} />
      </Usecase>
      {loadAllUseCases && (
        <Fragment>
          <Usecase title="Usecase #5 full with active cell">
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableFull} isRtl={isRtl} activeCellIds={activeCellIds} activeCellHandler={activeCellHandler}  SideIcon={SideIcon} labelAccessor={labelAccessor} />
          </Usecase>
          <Usecase title="Usecase #6 400" defaultExpanded>
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...table400} isRtl={isRtl}  SideIcon={SideIcon} labelAccessor={labelAccessor} />
          </Usecase>
          <Usecase title="Usecase #7 flags">
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableFlags} isRtl={isRtl}  SideIcon={SideIcon} labelAccessor={labelAccessor} />
          </Usecase>
          <Usecase title="Usecase #8 single obs">
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableData} cells={{'': {'': {'': [R.pathOr({}, [0, '0:0', 0, 0], cells)]}}}} isRtl={isRtl}  SideIcon={SideIcon} labelAccessor={labelAccessor} />
          </Usecase>
          <Usecase title="Usecase #8b single obs with a cell handler">
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableData} cells={{'': {'': {'': [R.pathOr({}, [0, '0:0', 0, 0], cells)]}}}} isRtl={isRtl} cellHandler={console.log}  SideIcon={SideIcon} labelAccessor={labelAccessor} />
          </Usecase>
          <Usecase title="Usecase #9 single obs with flags">
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableFlags} cells={{'': {'': {'': [R.pathOr({}, [0, 0, 0, 0], tableFlags.cells)]}}}} isRtl={isRtl}  SideIcon={SideIcon} labelAccessor={labelAccessor} />
          </Usecase>
          <Usecase title="Usecase #9b single obs with flags with a cell handler">
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableFlags} cells={{'': {'': {'': [R.pathOr({}, [0, 0, 0, 0], tableFlags.cells)]}}}} isRtl={isRtl} cellHandler={console.log}  SideIcon={SideIcon} labelAccessor={labelAccessor} />
          </Usecase>
          <Usecase title="Usecase #10 large width table">
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableLargeWidth} isRtl={isRtl} cellHandler={console.log} labelAccessor={labelAccessor} />
          </Usecase>
          <Usecase title="Usecase #10 large width table three columns">
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableLargeWidth3Cols} isRtl={isRtl} cellHandler={console.log} labelAccessor={labelAccessor} />
          </Usecase>
          <Usecase title="Usecase #11 long table">
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableLong} cellHandler={console.log} labelAccessor={labelAccessor} />
          </Usecase>
          <Usecase title="Usecase #11 long table with long texts">
            <TableHtml5 HTMLRenderer={HTMLRenderer} {...tableLongTexts} labelAccessor={nameAccessor} />
          </Usecase>
        </Fragment>
      )}
    </Fragment>
  );
};

Wrapper.propTypes = {
  isRtl: PropTypes.bool,
};

export default Wrapper;
