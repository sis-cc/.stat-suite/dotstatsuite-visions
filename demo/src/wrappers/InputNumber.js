import React, { useState } from 'react';
import Usecase from '../components/Usecase';
import Container from '@mui/material/Container';
import { InputNumber } from '../../../src';

const props = {
  beforeLabel: 'Last',
  afterLabel: 'Period',
  popperLabel: 'Tooltip for more explantions',
  placeholder: '000',
  textFieldProps: {
    fullWidth: true,
  },
  ariaLabel: "ariaLabel"
};

const snippet1 = `<InputNumber value={nb} onChange={setNb} />`;
const snippet2 = `<InputNumber
  value={nb}
  onChange={setNb}
  beforeLabel={beforeLabel}
  afterLabel={afterLabel}
  popperLabel={popperLabel}
  placeholder={placeholder}
  textFieldProps={textFieldProps}
/>`;

const Wrapper = () => {
  const { beforeLabel, afterLabel, popperLabel, placeholder, textFieldProps, ariaLabel } = props;
  const [nb, setNb] = useState(undefined);

  return (
    <React.Fragment>
      <Usecase title="Input Number" snippet={snippet1} defaultExpanded>
        <Container>
          <InputNumber value={nb} onChange={setNb} />
        </Container>
      </Usecase>
      <Usecase title="last n period" snippet={snippet2} defaultExpanded>
        <Container>
          <InputNumber
            value={nb}
            onChange={setNb}
            beforeLabel={beforeLabel}
            afterLabel={afterLabel}
            popperLabel={popperLabel}
            placeholder={placeholder}
            textFieldProps={textFieldProps}
            ariaLabel={ariaLabel}
          />
        </Container>
      </Usecase>
    </React.Fragment>
  );
};

export default Wrapper;
