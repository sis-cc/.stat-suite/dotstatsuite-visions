import React from 'react';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import { Loading} from '../../../src';
import Usecase from '../components/Usecase';

const Wrapper = () => {
  const [toggleVariant, setToggleVariant] = React.useState(false);
  const [toggleMessage, setToggleMessage] = React.useState(true);
  const [toggleColor, setToggleColor] = React.useState(false);
  const [toggleWarning, setToggleWarning] = React.useState(false);

  return (
    <Usecase title="Demo" defaultExpanded>
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant={toggleVariant ? 'contained' : 'outlined'}
              color="primary"
              style={{ margin: 4 }}
              onClick={() => setToggleVariant(!toggleVariant)}
            >
              Toggle variant
            </Button>
            <Button
              size="small"
              variant={toggleMessage ? 'contained' : 'outlined'}
              color="primary"
              style={{ margin: 4 }}
              onClick={() => setToggleMessage(!toggleMessage)}
            >
              Toggle message
            </Button>
            <Button
              size="small"
              variant={toggleColor ? 'contained' : 'outlined'}
              color="primary"
              style={{ margin: 4 }}
              onClick={() => setToggleColor(!toggleColor)}
            >
              Toggle color '#10d5a8'
            </Button>
            <Button
              size="small"
              variant={toggleWarning ? 'contained' : 'outlined'}
              color="primary"
              style={{ margin: 4 }}
              onClick={() => setToggleWarning(!toggleWarning)}
            >
              Toggle warning
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Loading 
              message={toggleMessage ? "hello" : undefined} 
              variant={toggleVariant ? "primary" : "secondary"} 
              color={toggleColor ? '#10d5a8' : undefined}
              isWarning={toggleWarning}
            /> 
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
