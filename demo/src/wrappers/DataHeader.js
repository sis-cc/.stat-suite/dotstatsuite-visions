import React from 'react';
import * as R from 'ramda';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Usecase from '../components/Usecase';
import { DataHeader, T4_BREAKPOINTS } from '../../../src';

const title = {
  "label": "Table 0103 - Annual GDP identity from the income side",
  "flags": [{ "label": "Time format: Annual" }]
};

const subtitle = [
  {
    "header": "Frequency:",
    "label": "Annual",
    "flags": [{ "label": "Time format: Annual" }]
  },
  {
    "header": "Adjustment indicator:",
    "label": "Neither seasonally adjusted nor calendar adjusted data",
    "flags": []
  },
  {
    "header": "Reference area:",
    "label": "Belgium",
    "flags": [{
      "header": "Time format:",
      "label": "Annual",
      "sub": [{ "label": "Sub 1" }, { "label": "Sub 2" }]
    }]
  },
  {
    "header": "Counterpart area:",
    "label": "Domestic (home or reference area)",
    "flags": []
  },
  {
    "header": "Reporting institutional sector:",
    "label": "Total economy",
    "flags": []
  }
];

const uprs = {
  "header": "Unit of Measure",
  "label": "Domestic currency (incl. conversion to current currency made using a fixed parity), Millions"
};

const disclaimer = 'Nullam vitae nunc posuere, vestibulum augue eu, fermentum nisi, Etiam eget nunc sed metus egestas rutrum';

const btnFactory = (label, handler) => (
  <Button style={{width: 125}} size="small" color="primary" onClick={handler}>{label}</Button>
);

const Wrapper = () => {
  const [hasTitle, setHasTitle] = React.useState(true);
  const [hasSubtitle, setHasSubtitle] = React.useState(true);
  const [hasUprs, setHasUprs] = React.useState(true);
  const [hasDisclaimer, setHasDisclaimer] = React.useState(true);
  const [isBordered, setIsBordered] = React.useState(true);


  const snippet = `<DataHeader
  title={title}
  subtitle={subtitle}
  uprs={uprs}
  disclaimer={disclaimer}
/>`;

  const breakpoints = R.pipe(R.omit([]), R.toPairs)(T4_BREAKPOINTS);

  return (
    <Usecase title="Demo" snippet={snippet} defaultExpanded>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            {btnFactory(
              `${hasTitle ? 'unset' : 'set'} title`,
              () => setHasTitle(!hasTitle)
            )}
            &nbsp;
            {btnFactory(
              `${hasSubtitle ? 'unset' : 'set'} subtitle`,
              () => setHasSubtitle(!hasSubtitle)
            )}
            &nbsp;
            {btnFactory(
              `${hasUprs ? 'unset' : 'set'} uprs`,
              () => setHasUprs(!hasUprs)
            )}
            &nbsp;
            {btnFactory(
              `${hasDisclaimer ? 'unset' : 'set'} disclaimer`,
              () => setHasDisclaimer(!hasDisclaimer)
            )}
            &nbsp;
            {btnFactory(
              `${isBordered ? 'unset' : 'set'} borders`,
              () => setIsBordered(!isBordered)
            )}
          </Grid>
          <Grid item xs={12}>
            <Box border={isBordered ? 1 : 0}>
              <DataHeader
                title={hasTitle ? title : null}
                subtitle={hasSubtitle ? subtitle : null}
                uprs={hasUprs ? uprs : null}
                disclaimer={hasDisclaimer ? disclaimer : null}
              />
            </Box>
          </Grid>
          {R.map(([key, val]) => (
            <Grid item xs={12} key={key}>
              <Typography display="block" variant="body2">{key}: {val}px</Typography>
              <Box style={{ width: val + (isBordered ? 2 : 0), border: isBordered ? '1px solid' : 0 }}>
                <DataHeader
                  title={hasTitle ? title : null}
                  subtitle={hasSubtitle ? subtitle : null}
                  uprs={hasUprs ? uprs : null}
                  disclaimer={hasDisclaimer ? disclaimer : null}
                />
              </Box>
            </Grid>
          ), breakpoints)}
        </Grid>
      </Container>
    </Usecase>
  );
}

export default Wrapper;
