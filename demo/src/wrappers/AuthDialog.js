import React from 'react';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Usecase from '../components/Usecase';
import { AuthDialog } from '../../../src';

const onClose = () => console.log('onClose');
const onSubmit = (data) => console.log('onSubmit', data);

const labels = {
  anonymous: 'connect anonymously',
  error: 'invalid entries',
  header: 'Authenticate',
  password: 'Password',
  user: 'Username',
  submit: 'Login'
}

const Wrapper = () => {
  const [open, setOpen] = React.useState(false);
  return (
    <Usecase title="AuthDialog demo" snippet="<AuthDialog isOpen={isOpen} onClose={onClose} onSubmit={onSubmit} labels={labels} />" defaultExpanded>
      <Container>
        <Grid item xs={12}>
          <Button
            size="small"
            color="primary"
            variant='contained'
            style={{ margin: 4 }}
            onClick={() => setOpen(true)}
          >
            open 
          </Button>
        </Grid>
        <Grid item xs={12}>
          <AuthDialog isOpen={open} onClose={() => setOpen(false)} onSubmit={onSubmit} labels={labels} />
        </Grid>
      </Container>
    </Usecase>  
  ); 
};

export default Wrapper;

