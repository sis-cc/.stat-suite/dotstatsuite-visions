import React from 'react';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { TableFooter } from '../../../src';

const logoLink =
  'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/250px-Google_2015_logo.svg.png';

const Wrapper = () => (
  <React.Fragment>
    <Divider />
    <TableFooter
      tooltip={{ linkLabel: 'label', link: '#link', label: 'owner' }}
      logo={logoLink}
      linkLabel="google"
      link="#google"
    />
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {`<TableFooter
  tooltip={{ linklabel: 'label', link: '#link', label: 'owner' }}
  logo={logoLink}
  linkLabel='google'
  link='#google'
/>`}
    </SyntaxHighlighter>
  </React.Fragment>
);

export default Wrapper;
