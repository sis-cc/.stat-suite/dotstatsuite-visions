import React, { useState } from 'react';
import * as R from 'ramda';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Usecase from '../components/Usecase';
import { DataEdit } from '../../../src';
import ListButtons from '../components/ListButtons';
import Divider from '../components/Divider';

const props = {
  labels: {
    logo: 'Remove logo',
    copyright: 'Remove copyright',
    title: 'Title',
    subtitle: 'Subtitle',
    source: 'Source',
    reset: 'reset label',
  },
};

const listButtons = [
  [
    {
      isActiveTitle: true,
      isActiveSubtitle: true,
      isActiveSource: true,
      isActiveLogo: true,
      isActiveCopyright: true,
      isOnReset: true,
    },
  ],
];

const orginalValue = {
  title: 'My Title',
  subtitle: 'My Subtitle',
  source: 'Source',
  logo: false,
  copyright: false,
}

const Wrapper = () => {
  const { labels } = props;
  const [toggle, setToggle] = useState(R.pipe(R.flatten, R.mergeAll)(listButtons));
  const [value, setValue] = useState(orginalValue);

  const properties = {
    title: {
      id: 'title',
      isActive: toggle.isActiveTitle,
      onSubmit: v => console.log('title:', v),
      onReset: toggle.isOnReset ? () => {
        console.log('reset: null')
        setValue({ ...value, title: orginalValue.title })
      } : null,
      value: value.title,
    },
    subtitle: {
      id: 'subtitle',
      isActive: toggle.isActiveSubtitle,
      onSubmit: v => console.log('subtitle:', v),
      onReset: toggle.isOnReset ? () => {
        console.log('reset: null')
        setValue({ ...value, subtitle: orginalValue.subtitle })
      } : null,
      value: value.subtitle,
    },
    source: {
      id: 'source',
      isActive: toggle.isActiveSource,
      onSubmit: v => console.log('source:', v),
      onReset: toggle.isOnReset ? () => {
        console.log('reset: null')
        setValue({ ...value, source: orginalValue.source })
      } : null,
      value: value.source,
    },
    logo: {
      id: 'logo',
      isActive: toggle.isActiveLogo,
      onChange: () => setValue({ ...value, logo: !value.logo  }),
      checked: value.logo,
    },
    copyright: {
      id: 'copyright',
      isActive: toggle.isActiveCopyright,
      onChange: () => setValue({ ...value, copyright: !value.copyright  }),
      checked: value.copyright,
    }
  };


  return (
    <Usecase title="DataEdit - (used by ChartConfig)" defaultExpanded snippet={`<DataEdit properties={properties} labels={labels} />`}>
      <Container>
        <Grid container>
          <Grid item xs={12}>
            <Grid container>
              <ListButtons listOfButtons={listButtons} setToggle={setToggle} toggle={toggle} />
            </Grid>
          </Grid>
          <Divider />
          <Grid item xs={12}>
            <Grid container>
              <DataEdit properties={properties} labels={labels} />
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
