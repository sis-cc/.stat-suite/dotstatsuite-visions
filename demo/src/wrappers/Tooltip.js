import React from 'react';
import Button from '@mui/material/Button';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { Tooltip } from '../../../src';

const Wrapper = () => (
  <React.Fragment>
    <Divider />
    <Tooltip interactive placement="top" title="World">
      <Button>Hello</Button>
    </Tooltip>
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {`<Tooltip interactive placement="top" title="World">
  <Button>Hello</Button>
</Tooltip>`}
    </SyntaxHighlighter>
    <Divider />
  </React.Fragment>
);

export default Wrapper;
