import React from 'react';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { Pagination, ExpansionPanel } from '../../../src';

const Wrapper = () => {
  const pages = 3;
  const labels = {
    page: 'page',
    of: 'of',
    startPage: 'startPage',
    previousPage: 'previousPage',
    nextPage: 'nextPage',
    endPage: 'endPage',
  };
  const [page, setPage] = React.useState(1);

  return (
    <React.Fragment>
      <ExpansionPanel id="expectedProps" label="Expected props" maxHeight={false}>
        <SyntaxHighlighter language="json" style={okaidia}>
          {JSON.stringify({ pages, page, labels }, null, '\t')}
        </SyntaxHighlighter>
      </ExpansionPanel>
      <Divider />
      <Pagination />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {'<Pagination />'}
      </SyntaxHighlighter>
      <Divider />
      <Pagination page={page} pages={pages} onChange={setPage} onSubmit={setPage} labels={labels} />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<Pagination
  page={page}
  pages={pages}
  onChange={setPage}
  onSubmit={v => console.log(v)}
  labels={labels}
/>
`}
      </SyntaxHighlighter>
      <Divider />
    </React.Fragment>
  );
};
export default Wrapper;
