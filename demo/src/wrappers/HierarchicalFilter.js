import React, { useState } from 'react';
import * as R from 'ramda';
import Usecase from '../components/Usecase';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import { HierarchicalFilter } from '../../../src';

const tree = [
  { id: 'AUS', label: 'AUS-label', hasData: true, isEnabled: true, order: 4, parentId: undefined },
  { id: 'AUT', label: 'AUT-label', hasData: false, isEnabled: true, order: 5, parentId: undefined },
  { id: 'OECD', label: 'OECD-label', hasData: true, isEnabled: true, hierarchicalId: 'OECD', order: 0, parentId: undefined },
  { id: 'EU', label: 'EU-label', hasData: false, isEnabled: false, hierarchicalId: 'EU', order: 1, parentId: undefined },
  { id: 'FRA', label: 'FRA-label', hasData: true, isEnabled: true, order: 3, parentId: 'EU', hierarchicalId: 'EU.FRA', isSelected: false },
  { id: 'GER', label: 'GER-label', hasData: true, isEnabled: true, order: 6, parentId: 'EU', hierarchicalId: 'EU.GER' },
  { id: 'GER', label: 'GER-label', hasData: true, isEnabled: true, order: 6, parentId: 'OECD', hierarchicalId: 'OECD.GER' },
  { id: 'JPN', label: 'JPN-label', hasData: true, isEnabled: true, order: 2, parentId: 'OECD', hierarchicalId: 'OECD.JPN' },
  { id: 'FRA', label: 'FRA-label', hasData: true, isEnabled: true, order: 3, parentId: 'OECD', hierarchicalId: 'OECD.FRA', isSelected: false },
  { id: 'WOR', label: 'World', hasData: false, isEnabled: false, order: 7, parentId: undefined, isSelected: true },
  { id: 'THW', label: 'Third World', hasData: false, isEnabled: false, order: 7, parentId: 'WOR', isSelected: false, hierarchicalId: 'WOR.THW', isSelected: false },
  { id: 'POO', label: 'Poor Country', hasData: true, isEnabled: true, order: 7, parentId: 'WOR.THW', isSelected: false, hierarchicalId: 'WOR.THW.POO', isSelected: true },
];

const smallList = [
  { count: 22, id: 1, label: 'Blue', isSelected: true, description: 'Green is the color of the sky' },
  { count: 2, id: 2, label: 'Green', isSelected: true, description: 'Green is the color of the tree' },
  {
    count: 8,
    id: 3,
    label: 'The MuiListItemSecondaryAction name can be used for providing default props or style overrides at the theme level',
    isSelected: true,
    description: `<span><strong>This indicator is based on harmonized definitions used by the ILO in the context of producing global estimates. Data may differ from nationally reported figures. </strong>
    Children refer to persons ages 5-17 years old. For more information, refer to the
    <a href="https://ilostat.ilo.org/resources/concepts-and-definitions/">concepts and definitions</a> page.</span>`
  },
  { count: 9, id: 4, label: 'Yellow' },
  { count: 14, id: 5, label: 'Pink', svgPath: 'M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z' },
  { id: 6, label: 'Purple' },
];

const addChildren = list => {
  const children = R.reduce((acc, item) => {
    if (R.isNil(item.parentId)) return acc;
    return R.append(
      R.times(
        num => ({
          count: Math.floor(Math.random() * 100) + 1,
          id: R.pipe(R.multiply(100000), R.add(num))(item.id),
          parentId: R.add(item.id, 10),
          label: `label #${R.pipe(R.multiply(100000), R.add(num))(item.id)}`,
        }),
        100,
      ),
    )(acc);
  }, [])(list);
  return R.pipe(R.append(list), R.flatten)(children);
};

const largeList = R.pipe(
  R.times(id => ({ count: Math.floor(Math.random() * 100) + 1, id, label: `label #${id}`, isEnabled: true, hasData: true })),
  R.map(item => {
    if (item.id <= 10) return item;
    if (item.id <= 20) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 30) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 40) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 50) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 60) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    return R.assoc('parentId', item.id - 10)(item);
  }),
  addChildren,
)(100);

const snippet = '';

const SanitizedInnerHTML = ({ html, ...rest }) => {
  return (
    <span dangerouslySetInnerHTML={{ __html: html }} {...rest} />
  );
};

const labels = {
  selection: {
    title: 'Bulk selections',
    currentLevel: 'Entire currently displayed list',
    level: 'Entire hierarchy level'
  },
  childrenNavigateDesc: 'Drill-down to next level',
  backHelper: 'back to this level',
  disableItemLabel: 'Disable',
};

const Wrapper = () => {
  const [filterOpenId, setFilterOpenId] = useState('spotlight');
  const originals = { small: smallList, tree: tree, large: largeList };
  const [lists, setLists] = useState(originals);
  const [accessibility, setAccessibility] = useState(false);
  const [disableAccessor, setDisableAccessor] = useState(true);
  const [renderLabel, setRenderLabel] = useState(true);

  const changeSelection = (selection, ids) => {
    console.log(selection, ids) // demo purpose
    setLists({
      ...lists,
      [selection]: R.map((item) => {
        if (R.includes(item.id, ids)) return R.over(R.lensProp('isSelected'), R.not)(item)
        return item;
      })(R.prop(selection)(lists))
    })
  }
  const tagAriaLabel = (count, total) => `${count} active against ${total}`
  return (
    <Usecase title="Demo" defaultExpanded snippet={snippet}>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant="text"
              color="primary"
              onClick={() => setLists(originals)}
            >
              Back to default
            </Button>
            &nbsp;&nbsp;
            <Button
              size="small"
              variant={accessibility ? "contained" : "outlined"}
              color="primary"
              onClick={() => setAccessibility(!accessibility)}
            >
              Accessibility
            </Button>
            &nbsp;&nbsp;
            <Button
              size="small"
              variant={disableAccessor ? "contained" : "outlined"}
              color="primary"
              onClick={() => setDisableAccessor(!disableAccessor)}
            >
              disable Accessor
            </Button>
            &nbsp;&nbsp;
            <Button
              size="small"
              variant={renderLabel ? "contained" : "outlined"}
              color="primary"
              onClick={() => {
                setRenderLabel(!disableAccessor)
              }}
            >
              Label Renderer
            </Button>
            &nbsp;&nbsp;
          </Grid>
          <Grid item xs={12}>
            <HierarchicalFilter
              id="tree"
              label="tree Filter"
              items={R.prop('tree')(lists)}
              disableAccessor={disableAccessor ? R.pipe(R.prop('isEnabled'), R.not) : undefined}
              activePanelId={filterOpenId}
              onChangeActivePanel={() => setFilterOpenId(!!filterOpenId ? null : 'tree')}
              changeSelection={changeSelection}
              accessibility={accessibility}
              tagAriaLabel={tagAriaLabel}
              labels={{
                disableItemLabel: 'Disable',
                placeholder: (a) => `${a} items`,
                iconLabel: 'Use advanced selection modes'

              }}
              labelRenderer={renderLabel ? R.prop('label') : R.prop('id')}
              toggleBulk={() => {
                console.log('tree advance toggle');
              }}
              HTMLRenderer={SanitizedInnerHTML}
            />
          </Grid>
          <Grid item xs={12}>
            <HierarchicalFilter
              id="large"
              label="large list Filter"
              items={R.prop('large')(lists)}
              disableAccessor={disableAccessor ? R.pipe(R.prop('isEnabled'), R.not) : undefined}
              activePanelId={filterOpenId}
              onChangeActivePanel={() => setFilterOpenId(!!filterOpenId ? null : 'large')}
              changeSelection={changeSelection}
              accessibility={accessibility}
              tagAriaLabel={tagAriaLabel}
              labels={{
                disableItemLabel: 'Disable',
                placeholder: (a) => `${a} items`,
                iconLabel: 'Use advanced selection modes'

              }}
              labelRenderer={renderLabel ? R.prop('label') : R.prop('id')}
              toggleBulk={() => {
                console.log('large advance toggle');
              }}
              HTMLRenderer={SanitizedInnerHTML}
            />
          </Grid>
          <Grid item xs={12}>
            <HierarchicalFilter
              id="small"
              label="small list Filter"
              items={R.prop('small')(lists)}
              disableAccessor={disableAccessor ? R.pipe(R.propOr(true, 'isEnabled'), R.not) : undefined}
              activePanelId={filterOpenId}
              onChangeActivePanel={() => setFilterOpenId(!!filterOpenId ? null : 'small')}
              changeSelection={changeSelection}
              accessibility={accessibility}
              tagAriaLabel={tagAriaLabel}
              labels={{
                disableItemLabel: 'Disable',
                placeholder: (a) => `${a} items`,
                iconLabel: 'Use advanced selection modes'

              }}
              labelRenderer={renderLabel ? R.prop('label') : R.prop('id')}
              toggleBulk={() => {
                console.log('small advance toggle');
              }}
              HTMLRenderer={SanitizedInnerHTML}
            />
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
