import React, { useState } from 'react';
import * as R from 'ramda';
import AccessAlarm from '@mui/icons-material/AccessAlarm';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import People from '@mui/icons-material/People';
import CheckCircleOutline from '@mui/icons-material/CheckCircleOutline';
import Button from '@mui/material/Button';
import Divider from '../components/Divider';
import { Input } from '../../../src';
import { action } from '../utils';

const Wrapper = () => {
  const [value, setValue] = useState('');
  const [isControlled, setControlled] = useState(false);
  const [withValidationIcon, setwithValidationIcon] = useState(false);
  const [fullWidth, setfullWidth] = useState(false);
  const [withCustomRightIcon, setwithCustomRightIcon] = useState(false);
  const [withCustomLeftIcon, setwithCustomLeftIcon] = useState(false);

  const onChange = value => {
    console.log(value);
    setValue(value);
  };

  return (
    <React.Fragment>
      <Divider />
      <Button style={{ margin: 4 }} variant="outlined" onClick={() => setControlled(!isControlled)}>
        Is Controlled
      </Button>
      <Button style={{ margin: 4 }} variant="outlined" onClick={() => setfullWidth(!fullWidth)}>
        Full Width
      </Button>
      <Button
        style={{ margin: 4 }}
        variant="outlined"
        onClick={() => setwithValidationIcon(!withValidationIcon)}
      >
        With Validation Icon
      </Button>
      <Button
        style={{ margin: 4 }}
        variant="outlined"
        onClick={() => setwithCustomRightIcon(!withCustomRightIcon)}
      >
        With Custom Right Icon
      </Button>
      <Button
        style={{ margin: 4 }}
        variant="outlined"
        onClick={() => setwithCustomLeftIcon(!withCustomLeftIcon)}
      >
        With Custom Left Icon
      </Button>
      <Divider />
      <Divider />
      <Input
        id="input_1"
        label="Width"
        onSubmit={action}
        isControlled={isControlled}
        placeholder={'Enter some text'}
        type="text"
        value={value}
        onChange={onChange}
        fullWidth={fullWidth}
        withValidationIcon={withValidationIcon}
        rightIcon={withCustomRightIcon ? CheckCircleOutline : undefined}
        leftIcon={withCustomLeftIcon ? People : undefined}
      />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<Input
  id="input_1"
  label="Width"
  onSubmit={action}
  isControlled={isControlled}
  placeholder={'Enter some text'}
  type="text"
  value={value}
  onChange={onChange}
  fullWidth={fullWidth}
  withValidationIcon={withValidationIcon}
  rightIcon={withCustomRightIcon ? CheckCircleOutline : null}
  leftIcon={withCustomLeftIcon ? People : null}
/>`}
      </SyntaxHighlighter>
    </React.Fragment>
  );
};

export default Wrapper;
