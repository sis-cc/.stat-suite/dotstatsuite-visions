import React from 'react';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import HelpIcon from '@mui/icons-material/EmojiObjects';
import Usecase from '../components/Usecase';
import { Mode } from '../../../src';

const Wrapper = () => {
  const [disabled, setDisbled] = React.useState(false);
  const [mode, setMode] = React.useState("LASTNPERIODS");
  const props = {
    modes: [
      {
        value: "LASTNPERIODS",
        label: 'periods',
        popperLabel: "help LASTNPERIODS",
        disabled: disabled,
      },
      {
        value: "LASTNOBSERVATIONS",
        label: 'timeSeries',
        popperLabel: "help LASTNOBSERVATIONS",
        disabled: disabled,
      },
    ],
    mode,
    placement: 'start',
    changeMode: (val) => setMode(val)
  }

  return (
    <Usecase title="Demo" defaultExpanded>
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant={disabled ? 'contained' : 'outlined'}
              color="primary"
              style={{ margin: 4 }}
              onClick={() => setDisbled(!disabled)}
            >
              Disable Modes
            </Button>
          </Grid>
          <Grid item xs={12}>
            <Mode {...props}>
              <HelpIcon fontSize="small" />
            </Mode>
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
