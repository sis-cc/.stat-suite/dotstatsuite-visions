import React, { useMemo, useState } from 'react';
import * as R from 'ramda';
import Usecase from '../components/Usecase';
import { ChartsConfig } from '../../../src';
import Divider from '../components/Divider';
import { Button, Container, Grid, Typography } from '@mui/material';
import labels from './ChartsConfig/labels';
import propertiesFactory, { dataEdit } from './ChartsConfig/properties';
import mapping from './ChartsConfig/mapping';

const Wrapper = () => {
  const properties = propertiesFactory();

  const types = ['all properties', ...R.keys(mapping)];
  const [chartType, setChartType] = useState('bar');
  const handleChartType = newType => () => setChartType(newType);

  const [hasDataEdit, setHasDataEdit] = useState(false);
  const handleHasDataEdit = () => setHasDataEdit(prev => !prev);

  const [hasDisplayMode, setHasDisplayMode] = useState(false);
  const handleHasDisplayMode = () => setHasDisplayMode(prev => !prev);

  const pKeys = useMemo(() => {
    return R.prop(chartType, mapping);
  }, [chartType]);

  const _properties = useMemo(() => {
    if (R.isNil(pKeys)) return properties;
    return R.pipe(
      R.over(
        R.lensPath(['baseline', 'value', 0, 'value']),
        value => chartType === 'row' ? 'baseline-id-2' : value,
      ),
      R.over(
        R.lensPath(['highlight', 'value', 0, 'value']),
        value => chartType === 'timeline' ? 'highlight-id-2' : value,
      ),
      R.pick(hasDisplayMode ? R.append('displayMode', pKeys) : pKeys),
      R.ifElse(
        R.always(hasDataEdit),
        R.mergeRight(dataEdit),
        R.identity,
      ),
    )(properties);
  }, [pKeys, hasDataEdit, hasDisplayMode]);

  return (
    <Usecase
      title="Chart config demo"
      defaultExpanded
      snippet={`<ChartConfig properties={properties} labels={labels} />`}
    >
      <Container>
        <Grid container spacing={1}>
          {R.map(type => (
            <Grid item>
              <Button
                key={type}
                color="primary"
                variant={chartType === type ? 'contained' : 'outlined'}
                onClick={handleChartType(type)}
              >
                {type}
              </Button>
            </Grid>
          ), types)}
          <Grid item>
            <Button
              variant={hasDataEdit ? 'contained' : 'outlined'}
              onClick={handleHasDataEdit}
            >
              data edit
            </Button>
          </Grid>
          <Grid item>
            <Button
              variant={hasDisplayMode ? 'contained' : 'outlined'}
              onClick={handleHasDisplayMode}
            >
              display mode
            </Button>
          </Grid>
        </Grid>
        <br /><br />
        <Typography color="primary" display="block" variant="subtitle2">
          {R.join(' + ', R.defaultTo([chartType], pKeys))}
        </Typography>
      </Container>
      <Divider />
      <Container>
        <ChartsConfig
          properties={_properties}
          labels={labels}
          onSubmit={console.log}
        />
      </Container>
    </Usecase>
  );
};

export default Wrapper;
