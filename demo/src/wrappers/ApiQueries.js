import React from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Usecase from '../components/Usecase';
import { ApiQueries } from '../../../src';

const props = {
  queries: [
    {
      id: 'data',
      title: 'Data query',
      contents: [
        {
          id: 'flat',
          label: 'Flat',
          value:
            'https://stats.oecd.org/SDMX-JSON/data/CSPCUBE/POPMIG+DEMOGRAPHY+EVOPOP+EVOPOP_T1+EVOPOP_G1+FERTILITY+FERTILITY_T1+POPGEO+POPGEO_G1+POPGEO_G2A+POPGEO_G2B+POPGEO_G2C+POPGEO_G3A+POPGEO_G3B+POPGEO_G3C+POPGEO_G4A+POPGEO_G4B+POPGEO_G4C+AGEDPOPGEO+AGEDPOPGEO_G1+AGEDPOPGEO_G2A+AGEDPOPGEO_G2B+AGEDPOPGEO_G2C+AGEDPOPGEO_G3A+AGEDPOPGEO_G3B+AGEDPOPGEO_G3C',
        },
        {
          id: 'time',
          label: 'Time Series',
          value: 'error message',
          isError: true
        },
      ],
    },
    {
      id: 'structure',
      title: 'Structure query',
      contents: [
        {
          id: 'structure',
          value: 'https://stats.oecd.org/restsdmx/sdmx.ashx/GetDataStructure/CSPCUBE'
        },
      ],
    },
  ],
  labels: {
    title: 'Developer API query builder',
    copy: 'copy code',
    copied: 'copied',
    descriptions: [
      'The application programming interface (API) based on the SDMX standard allows a developer to programmatically access the data using simple RESTful URL and HTTP header options for various choices of response formats including JSON.',
      'To get started check the API Documentation. For any questions Contact us.',
    ],
    notice: 'notice',
    buttonsLabel: 'SDMX Flavour',
  },
};
const snippet = `<ApiQueries queries={queries} labels={labels} />`;

const Wrapper = () => {
  const { queries, labels } = props;
  return (
    <Usecase title="Demo" snippet={snippet} defaultExpanded>
       <Container>
       <Grid item xs={12}>
          <ApiQueries queries={queries} labels={labels} />
        </Grid>
      </Container>
    </Usecase>  
  ); 
};

export default Wrapper;
