import React from 'react';
import * as R from 'ramda';
import Usecase from '../components/Usecase';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import { useTheme } from '@mui/styles';
import { VSymbol, Scatter, Bar, Row, Timeline, StackedBar, Api, AccountFilled, AccountOutlined } from '../../../src';

const snippet = `<StackedBar style={{ color: theme.palette.highlight.hl2 }} />
<VSymbol fontSize="large" />
<Scatter color="primary" />
<Bar color="secondary" />
<Row style={{ fontSize: 60 }} />
<Timeline color="disabled" />
<HSymbol style={{ color: theme.palette.highlight.hl1 }} />
<Asterisk fontSize="small" />
<Api />`;

const Wrapper = () => {
  const theme = useTheme();

  return (
    <Usecase title="Demo" snippet={snippet} defaultExpanded>
      <Container>
        <Grid container spacing={3} justifyContent="space-around" alignItems="center">
          <StackedBar style={{ color:R.path(['palette', 'highlight', 'hl2'], theme)|| theme.palette.secondary.dark }} />
          <VSymbol fontSize="large" />
          <Scatter color="primary" />
          <Bar color="secondary" />
          <Row style={{ fontSize: 60 }} />
          <Timeline color="disabled" />
          <AccountOutlined style={{ color: R.path(['palette', 'highlight', 'hl1'], theme) || theme.palette.primary.dark }} />
          <AccountFilled fontSize="small" />
          <Api />
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
