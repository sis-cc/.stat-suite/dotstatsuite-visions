import React from 'react';
import Usecase from '../components/Usecase';
import { ShortUrls, VerticalButton } from '../../../src';
import { useState } from "react";
import { Container } from "@mui/material";

const props = {
  labels: {
    title: 'Short URL for this page',
    generateUrl: 'Generate short URL',
    errorMessage: 'Something went wrong on our side',
  },
  contentValue: "https://www.google.com/",
};


const Wrapper = () => {
  const { labels, contentValue } = props;
  const [copied, setCopied] = useState(false)
  const [loading, setLoading] = useState(false)
  const [isNotifOpen, setIsNotifOpen] = useState(false)

  const handleClick = () => {
    navigator.clipboard.writeText(contentValue)
    setCopied(true)
    console.log('copied')
  }

  return (
    <Usecase title="Demo" defaultExpanded>
      <Container>
        <VerticalButton
          style={{ margin: 4 }}
          selected={loading}
          variant="outlined"
          onClick={() => setLoading(!loading)}
        >
          is Generating a short URL
        </VerticalButton>
        <VerticalButton
          style={{ margin: 4 }}
          selected={isNotifOpen}
          variant="outlined"
          onClick={() => setIsNotifOpen(!isNotifOpen)}
        >
          Error Message
        </VerticalButton>
        <ShortUrls labels={labels} onClick={handleClick} isLoading={loading} contentValue={contentValue} isNotifOpen={isNotifOpen} setIsNotifOpen={setIsNotifOpen} />
      </Container>
    </Usecase>
  );
};

export default Wrapper;
