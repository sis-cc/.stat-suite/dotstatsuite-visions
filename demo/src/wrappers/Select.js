import React from 'react';
import * as R from 'ramda';
import { Select } from '../../../src';
import Usecase from '../components/Usecase';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import AirplanemodeActiveIcon from '@mui/icons-material/AirplanemodeActive';

const locales = [
  { value: 'en', label: 'English' },
  { value: 'fr', label: 'Francais' },
  { value: 'jp', label: '日本' },
];

const Wrapper = () => {
  const [localeId, changeLocaleId] = React.useState();
  const [colors, changeColors] = React.useState(['r', 'b']);

  return (
    <Usecase title="Demo" defaultExpanded>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={() => changeLocaleId()}
            >
              Reset locale
            </Button>
            &nbsp;&nbsp;
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={() => changeColors(['r', 'b'])}
            >
              Apply red & blue
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Select value={localeId} items={locales} onChange={changeLocaleId} />
          </Grid>
          <Grid item xs={4}>
            <Select label="Label" value={localeId} items={locales} onChange={changeLocaleId} />
          </Grid>
          <Grid item xs={4}>
            <Select
              label="Default value"
              value={R.defaultTo('en', localeId)}
              items={locales}
              onChange={changeLocaleId}
            />
          </Grid>
          <Grid item xs={4}>
            <Select
              label="Value accessor"
              value={localeId}
              valueAccessor={R.prop('id')}
              items={[
                { id: 'en', label: 'English' },
                { id: 'fr', label: 'Francais' },
                { id: 'jp', label: '日本' },
              ]}
              onChange={changeLocaleId}
            />
          </Grid>
          <Grid item xs={4}>
            <Select
              label="Tokens"
              value={localeId}
              items={['en', 'fr', 'jp']}
              onChange={changeLocaleId}
            />
          </Grid>
          <Grid item xs={4}>
            <Select
              label={
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <AirplanemodeActiveIcon style={{ fontSize: '1rem' }} />
                  <em>Custom label</em>
                </div>
              }
              value={localeId}
              items={locales}
              onChange={changeLocaleId}
            />
          </Grid>
          <Grid item xs={12}>
            <Select
              label="Multiple colors"
              value={colors}
              items={[
                { value: 'r', label: 'red' },
                { value: 'b', label: 'blue' },
                { value: 'g', label: 'green' },
              ]}
              onChange={changeColors}
              isMultiple
            />
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
