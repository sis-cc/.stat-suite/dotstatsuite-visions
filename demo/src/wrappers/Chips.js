import React from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import * as R from 'ramda'
import Usecase from '../components/Usecase';
import { DeleteAllChip, GroupedChips } from '../../../src';

const props = {
  items1: [
    {
      id: 'REF_AREA',
      label: 'Reference area ',
      values: [
        [{ id: 'X01', label: 'World' }, { id: 'X02', label: 'France' }, { id: 'X03', label: 'Paris' }],
        [{ id: 'X04', label: 'World: Upper-middle income' }],
        [{ id: 'X05', label: 'World: High income' }],
        [
          { parents: [[{ id: 'X06', label: 'EU' }, { id: 'X09', label: 'France' }], [{ id: 'X07', label: 'DAC' }, { id: 'X09', label: 'France' }]], id: 'X012', label: 'France' }
        ]
      ],
    },
    {
      id: 'SURVEY',
      label: 'Survey',
      values: [
        [{ id: '1407', label: 'Community survey' }],
        [{ id: '549', label: 'Continuous Sample Survey of the Population' }],
        [{ id: '548', label: 'Continuous Sample Survey of the Populatio' }],
        [{ id: '547', label: 'Continuous Sample Survey of the Populati' }],
        [{ id: '546', label: 'Continuous Sample Survey of the Populat' }],
        [{ id: '545', label: 'Continuous Sample Survey of the Popula' }],
        [{ id: '544', label: 'Continuous Sample Survey of the Popul' }],
        [{ id: '543', label: 'Continuous Sample Survey of the Popu' }],
        [{ id: '542', label: 'Continuous Sample Survey of the Pop' }],
        [{ id: '541', label: 'Continuous Sample Survey of the Po' }],
        [{ id: '540', label: 'Continuous Sample Survey of the' }],
        [{ id: '539', label: 'Continuous Sample Survey of th' }],
        [{ id: '538', label: 'Continuous Sample Survey of t' }],
        [{ id: '537', label: 'Continuous Sample Survey of' }],
        [{ id: '536', label: 'Continuous Sample Survey o' }],
        [{ id: '535', label: 'Continuous Sample Survey' }],
      ],
    },
    {
      id: 'SEX',
      label: 'Sex',
      values: [
        [{ id: 'SEX_M', label: 'Male' }],
      ],
    },
    {
      id: 'AGE',
      label: 'Age',
      values: [
        [{ id: 'AGE_YTHADULT_Y15-24', label: '15-24' }],
        [{ id: 'AGE_YTHADULT_YGE25', label: '25+' }],
      ],
    },
  ],
  items2: [
    {
      id: 'TIME_PERIOD',
      label: 'Time period',
      values: [
        [{ id: 'start', label: 'Start: 2018.07.28' }],
        [{ id: 'end', label: 'End: 2018.07.28' }],
        [{ id: 'lastn', label: 'Last 1 period(s)' }],
      ],
    },
    {
      id: 'FREQUENCY',
      label: 'Frequency',
      isNotRemovable: true,
      values: [
        [{ id: 'start', label: 'Daily', isNotRemovable: true }],
      ],
    },
  ],
};

const snippet = `<GroupedChips
itemProps={items1}
  onDelete={(parentId, id) => console.log(parentId, id)}
  labels={{
    reducingChip: 'items',
  }}
/>
  <DeleteAllChip
    onDeleteAll={(parentId, id) => console.log(parentId, id)}
    clearAllLabel={'Clear all filters'}
/>
`

const Wrapper = () => {
  const { items1, items2 } = props;
  return (
    <Usecase title="Demo" defaultExpanded snippet={snippet}>
      <Container>
        <Grid container>
          {R.map(itemProps =>
            <Grid item xs={12} key={itemProps.id}>
              <GroupedChips
                itemProps={itemProps}
                onDelete={(parentId, id) => console.log(parentId, id)}
                labels={{ reducingChip: 'items' }}
              />
            </Grid>)(items1)}
          <DeleteAllChip
            onDeleteAll={(parentId, id) => console.log(parentId, id)}
            clearAllLabel={'Clear all filters'}

          />
        </Grid>
        <Grid item xs={12}>
          {R.map(itemProps =>
            <GroupedChips
              key={itemProps.id}
              itemProps={itemProps}
              onDelete={(parentId, id) => console.log(parentId, id)}
              labels={{ reducingChip: 'items' }}
            />)(items2)}
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
