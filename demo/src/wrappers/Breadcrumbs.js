import React from 'react';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { Breadcrumbs } from '../../../src';

const items = [
  {
    text: 'Home',
  },
  {
    text: 'Search results',
    action: () => console.log('breadcrumb-click'),
  },
  {
    text: 'Title of something',
    isDisabled: true,
  },
];

const Wrapper = ({ isRtl }) => {
  return (
    <React.Fragment>
      <Divider />
      <Breadcrumbs />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<Breadcrumbs />`}
      </SyntaxHighlighter>
      <Divider />
      <Breadcrumbs items={items} isRtl={isRtl} />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<Breadcrumbs items={items} />`}
      </SyntaxHighlighter>
    </React.Fragment>
  );
};

export default Wrapper;
