import React, { useState } from 'react';
import * as R from 'ramda';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Usecase from '../components/Usecase';
import { TableLayout } from '../../../src';

const props = {
  optionalItem: {
    id: 'OBS_ATTRIBUTES',
    name: 'Values and related characteristics',
    activationLabel: 'Show values and related characteristics in separate table cells',
    count: 3
  },
  items: [
    {
      id: 'SURVEY',
      name: 'Survey',
      count: 3
    },
    {
      id: 'SEX',
      name: 'Sex',
      count: 2
    },
    {
      id: 'TIME_PERIOD',
      name: 'Time Period',
      count: 2
    },
    {
      id: 'DSB',
      name: 'Disability status',
      count: 3
    },
  ],
  header: ['DSB'],
  sections: ['SURVEY'],
  rows: ['SEX', 'TIME_PERIOD'],
  labels: {
    commit: 'Apply layout',
    cancel: 'Cancel changes',
    row: 'Lignes',
    column: 'Colonnes',
    section: 'Sections',
    asc: 'asc',
    desc: 'desc',
    help: 'Information DnD',
    one: 'done',
    table: 'Table Preview',
    wcagDragExplanation: 'Explanation',
    wcagDragStart: ({ dragZone, index, dragLabel }) =>
      `Vous avez soulevé l'élément ${dragLabel} de la liste ${dragZone} a la position ${index}.`,
    wcagDragUpdate: ({ length, index, dropZone, dragZone }) =>
      `Vous avez deplacé l'élément depuis la liste ${dragZone} dans la liste ${dropZone} a la position ${index}, la longueur de la liste ${dropZone} est ${length}.`,
    wcagDragEnd: ({ index, dropZone, dragZone }) =>
      `Vous avez laché l'élément dans la liste ${dropZone} a la position ${index}, il a été déplacé depuis la liste ${dragZone}`,
    wcagDragCancell: 'Movement cancelled.',
  },
};

const snippet = `<TableLayout
  layout={layout}
  labels={labels}
  itemButtonProps={itemButtonProps}
  commit={onCommit}
  noPreview={noPreview}
  accessibility={accessibility}
/>`;

const Wrapper = () => {
  /* eslint-disable react/prop-types */
  const { sections, header, rows, labels, items, optionalItem } = props;
  const options = [labels.asc, labels.desc];
  /* eslint-enable react/prop-types */
  const [layout, setLayout] = useState({ header, sections, rows });
  const [value, setValue] = useState(R.head(options));
  const [noPreview, setNoPreview] = useState(false);
  const [accessibility, setAccessibility] = useState(false);

  const onCommit = props => setLayout(R.tap(console.log)(props));
  // eslint-disable-next-line no-console
  const onChange = props => setValue(R.tap(console.log)(props));

  const itemButtonProps = { TIME_PERIOD: { options, value, onChange } };

  return (
    <Usecase title="Demo" defaultExpanded snippet={snippet}>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={() => setNoPreview(!noPreview)}
            >
              No preview
            </Button>
            &nbsp;
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={() => setAccessibility(!accessibility)}
            >
              Accessibility
            </Button>
          </Grid>
          <Grid item xs={12}>
            <TableLayout
              layout={layout}
              labels={labels}
              itemButtonProps={itemButtonProps}
              commit={onCommit}
              noPreview={noPreview}
              accessibility={accessibility}
      	      items={items}
              optionalItem={optionalItem}
            />
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
