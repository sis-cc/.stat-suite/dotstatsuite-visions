import React, { useState, Fragment } from 'react';
import * as R from 'ramda';
import Container from '@mui/material/Container';
import Usecase from '../components/Usecase';
import Button from '@mui/material/Button';
import GetAppIcon from '@mui/icons-material/GetApp';
import { Dataflow, VerticalButton } from '../../../src';
import { Divider } from '@mui/material';

const props = {
  title: 'SDG indicator 8.5.2 - Unemployment rate by disability status',
  highlightDescription: '...mprise all persons <em>force</em> of working age...',
  datasourceId: 'staging:SIS-CC-stable',
  dataflowId: 'DF_SDG_ALL_SDG_0852_SEX_DSB_RT',
  version: '1.0',
  agencyId: 'ILO',
  indexationDate: '2020-02-14T14:20:37.447Z',
  id: 'staging:SIS-CC-stable:DF_SDG_ALL_SDG_0852_SEX_DSB_RT',
  dimensions: ["<em>Frequency</em>", "<em>Time period</em>", "Location", "Subject"],
  highlights: [
    ['Territory', ['Italy > Nord-ouest > Piemonte > Torino > San <em>Francesco</em> al Campo']],
  ],
  categories: [
    ['None H category (long)', ['Chile', 'China', 'Colombia', 'Czechia', 'Germany', 'Denmark', 'Estonia', 'Egypt', 'Spain', 'Finland', 'France', 'Greece', 'Croatia', 'Hungary', 'Indonesia', 'Ireland', 'Israel', 'India', 'Iceland', 'Italy', 'Japan', 'Korea', 'Kazakhstan', 'Lithuania', 'Luxembourg', 'Latvia', 'Morocco', 'Malta', 'Mexico', 'Netherlands', 'Norway', 'Peru', 'Poland', 'Portugal', 'Romania', 'Serbia']],
    ['None H category (short)', ['Not Applicable', 'Overnights visitors', 'Total']],

    ['H category (2 entries, short)', [
      ['Italy', 'Nord-ouest', 'Piemonte', 'Torino', 'San Francesco al Campo'],
      ['Belgium', 'Flanders'],
    ]],

    ['H category (3 entries, short with a long one)', [
      ['Italy', 'Nord-ouest', 'Piemonte', 'Torino', 'San Francesco al Campo'],
      ['Belgium', 'Flanders'],
      ['GPT suite', ...R.times(n => `item#${n}`, 20)],
    ]],

    ['H category (x entries, ellispsis triggered)', [
      ...R.times(n => ([`G-suite#${n}`, ...R.times(m => `item#${m}`, 4)]), 20),
    ]],
  ],
  url: '#your_link',
  labels: {
    source: 'Source',
    lastUpdated: 'Last updated:',
    date: '14th of February 2020',
    note: 'ILO:DF_SDG_ALL_SDG_0852_SEX_DSB_RT(1.0)',
    dimensions: 'Dimensions'
  },
  label: 'staging:SIS-CC-stable',
  body: {
    description: "<strong>Data may differ from nationally reported figures and the Global SDG Indicators Database due to differences in sources and/or reference years.</strong> <em>Estimates</em> on economic activity among children aged 5-17 refer to: (a) children 5&ndash;11 years old who, during the reference week, did at least one hour of economic activity, (b) children 12&ndash;14 years old who, during the reference week, did at least 14 hours of economic activity, (c) children 15&ndash;17 years old who, during the reference week, did at least 43 hours of economic activity. For more information, refer to the <a href = \"https://ilostat.ilo.org/resources/concepts-and-definitions/description-sustainable-development-labour-market-indicators/\">Labour Market-related SDG Indicators (ILOSDG) database description</a>.",
  },
};

const snippets = [
  `<Dataflow
  title={title}
  body={body}
  url={url}
  handleUrl={() => {}}
  label={label}
  labels={labels}
  highlights={highlights}
  dimensions={dimensions}
  categories={categories}
/>`,
];

const Wrapper = () => {
  const [hasDownload, setHasDownload] = useState(false);

  const downloadButton = (
    <VerticalButton color="primary" startIcon={<GetAppIcon />}>Download</VerticalButton>
  );

  const _props = hasDownload ? { ...props, children: downloadButton } : props;

  return (
    <Fragment>
      <Usecase title="Usecases" snippet={snippets[0]} defaultExpanded>
        <Container>
          <Button
            size="small"
            variant="contained"
            color={hasDownload ? 'primary' : 'inherit'}
            onClick={() => setHasDownload(!hasDownload)}
          >
            {hasDownload ? 'hide' : 'show'} download button
          </Button>
          <Divider style={{ margin: '20px 0' }} />
          <Dataflow {..._props} />
          <Dataflow {..._props} title="Two lines description" body={{ description: 'M-theory is a theory in physics that unifies all consistent versions of superstring theory. Edward Witten first conjectured the existence of such a theory at a string theory conference at the University of Southern California in 1995.' }} />
          <Dataflow {..._props} title="Small description" body={{ description: 'One of the deepest problems in modern physics is the problem of quantum gravity.' }} />
          <Dataflow {..._props} title="No description" body={{}} />
        </Container>
      </Usecase>
    </Fragment>
  );
};

export default Wrapper;
