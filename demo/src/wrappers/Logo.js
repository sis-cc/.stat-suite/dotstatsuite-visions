import React from 'react';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Typography from '@mui/material/Typography';
import Divider from '../components/Divider';
import { Logo } from '../../../src';

const logo =
  'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/250px-Google_2015_logo.svg.png';

const Wrapper = () => (
  <React.Fragment>
    <Divider />
    <Logo logo={logo} />
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {'<Logo logo={logo} />'}
    </SyntaxHighlighter>
    <Divider />
    <Logo logo={logo}>
      <Typography>For a better world</Typography>
    </Logo>
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {`<Logo logo={logo}>
  <Typography>For a better world</Typography>
</Logo>`}
    </SyntaxHighlighter>
  </React.Fragment>
);

export default Wrapper;
