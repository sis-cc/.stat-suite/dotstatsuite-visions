import React, { useState } from 'react';
import AccessAlarm from '@mui/icons-material/AccessAlarm';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { ToggleButton } from '../../../src';

const Wrapper = () => {
  const [isOpen, setIsOpen] = useState();
  const toggle = () => setIsOpen(!isOpen);
  return (
    <React.Fragment>
      <Divider />
      <ToggleButton label="toto" isOpen={isOpen} toggle={toggle} />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<ToggleButton label="toto" isOpen={isOpen} toggle={toggle} />`}
      </SyntaxHighlighter>
      <Divider />
      <div style={{ backgroundColor: '#0965c1', padding: 10 }}>
        <ToggleButton label="toto" isOpen={isOpen} toggle={toggle} />
      </div>
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<div style={{ backgroundColor: '#0965c1', padding: 10 }} >
  <ToggleButton label="toto" isOpen={isOpen} toggle={toggle} />
</div>`}
      </SyntaxHighlighter>
    </React.Fragment>
  );
};

export default Wrapper;
