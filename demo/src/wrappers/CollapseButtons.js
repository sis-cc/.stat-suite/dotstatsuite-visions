import React, { useState } from 'react';
import * as R from 'ramda';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Usecase from '../components/Usecase';
import { CollapseButtons } from '../../../src';

const items = [
  {
    "values": [
      {
        "id": "0|Categorisation for DF_DOMESTIC_TOURISM#IND#",
        "label": "Categorisation for DF_DOMESTIC_TOURISM",
        "subtopics": [
          {
            "id": "1|Categorisation for DF_DOMESTIC_TOURISM#IND#|Categorisation for\n\t\t\t\t\t\t\tDF_DOMESTIC_TOURISM#IND_TOUR#",
            "label": "Categorisation for\n\t\t\t\t\t\t\tDF_DOMESTIC_TOURISM",
            "code": "IND_TOUR",
          }
        ]
      },
      {
        "id": "0|Economy#ECO#",
        "label": "Economy",
        "subtopics": []
      },
      {
        "id": "Environment",
        "label": "Environment",
        "code": "ENV",
        "subtopics": []
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General3",
            "label": "General government Canada",
            "code": "GOV_GENERAL_CAN",
          },
          {
            "id": "General4",
            "label": "General government France",
            "code": "GOV_GENERAL_FRA",
          },
          {
            "id": "General5",
            "label": "General government Germany",
            "code": "GOV_GENERAL_GER",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      },
      {
        "id": "GOV",
        "code": "GOV",
        "label": "Government",
        "svgPath": "M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z",
        "subtopics": [
          {
            "id": "General1",
            "label": "General government",
            "code": "GOV_GENERAL",
          },
          {
            "id": "General2",
            "label": "Head General government Australia and New Zealand",
            "code": "GOV_GENERAL_AUS_NZ",
          },
          {
            "id": "General6",
            "label": "General government Italy",
            "code": "GOV_GENERAL_ITA",
          },
          
        ]
      }
    ],
    "id": "Topics",
    "label": "Topics"
  },
  {
    "values": [
      {
        "id": "staging:SIS-CC-stable",
        "label": "staging:SIS-CC-stable",
        "code": "SIS_CC_STAGING",
        "subtopics": []
      }
    ],
    "id": "datasourceId",
    "label": "Data source"
  },
  {
    "values": [
      {
        "id": "0|Quarterly#Q#",
        "label": "Quarterly",
        "code": "Q",
        "subtopics": []
      },
      {
        "id": "0|Annual#A#",
        "label": "Annual",
        "subtopics": []
      },
      {
        "id": "0|Monthly#M#",
        "label": "Monthly",
        "subtopics": []
      }
    ],
    "id": "Frequency",
    "label": "Frequency"
  }
];

const specificRenderIds = {
  "Topics": {
    "GOV": 'code',
    "GOV_GENERAL": 'name',
  }
};


export const getAccessor = R.cond([
  [R.equals('code'), () => R.prop('code')],
  [R.equals('name'), () => R.prop('label')],
  [R.T, () => ({ label, code }) => R.isNil(code) ? label : `${label} (${code})`],
]);

const Wrapper = () => {
  const [isSecondLevelClikable, setIsSecondLevelClikable] = useState(true);

  return (
    <Usecase title="Demo" defaultExpanded>
      <Container>
        <Grid container spacing={4} rowSpacing={2}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant={isSecondLevelClikable ? 'contained' : 'outlined'}
              color="primary"
              onClick={() => setIsSecondLevelClikable(!isSecondLevelClikable)}
            >
              Set second level {isSecondLevelClikable ? 'not ' : ' '} clickable
            </Button>
          </Grid>
          <Grid item xs={12}>
          <CollapseButtons
            items={items}
            action={console.log}
            isSecondLevelClikable={isSecondLevelClikable}
          />
        </Grid>
        <Grid item xs={12}>
          <CollapseButtons
            items={items}
            action={console.log}
            selectedItemId={R.path([0, 'id'], items)}
            isSecondLevelClikable={isSecondLevelClikable}
            labelAccessor={(props, id) => getAccessor(specificRenderIds[id][props.code] || 'both')(props)}
          />
        </Grid>
        </Grid>

      </Container>
    </Usecase>
  );
};

export default Wrapper;
