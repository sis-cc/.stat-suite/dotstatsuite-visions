import React from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Usecase from '../components/Usecase';
import { CollapsibleTree } from '../../../src';
import { SvgIcon } from '@mui/material';

const snippet = `
<CollapsibleTree
  data={data}
  isLoading={isLoading}
  defaultDepthLevel={1}
  labels={{
    collapseAll: 'Collapse all',
    expandAll: 'Expand all',
  }}
>
  <Button>Download</Button>
</CollapsibleTree>`;

const data  = [
  {
    id: 1,
    label: 'Inward activity of multinationals by industrial sector - ISIC Rev 4',
    children: [
      {
        id: 10,
        label: 'Data Characteristics',
        children: [
          {
            id: 100,
            label: 'Date last updated',
          },
          {
            id: 101,
            label: 'Unit of measure used',
            value: 'The units used to present data in AFA are millions of national currency for monetary variables and units for the other variables. Monetary variables are in current prices. Euro-area countries: national currency data is expressed in euro beginning with the year of entry into the Economic and Monetary Union (EMU). For years prior to the year of entry into EMU, data have been converted from the former national currency using the appropriate irrevocable conversion rate. This presentation facilitates comparisons within a country over time and ensures that the historical evolution is preserved. Please note, however, that pre-EMU euro are a notional unit and should not be used to form area aggregates or to carry out cross-country comparisons.',
          }
        ]
      },
      {
        id: 110,
        label: 'Unit of measure used',
        value: 'The units used to present data in AFA are millions of national currency for monetary variables and units for the other variables. Monetary variables are in current prices. Euro-area countries: national currency data is expressed in euro beginning with the year of entry into the Economic and Monetary Union (EMU). For years prior to the year of entry into EMU, data have been converted from the former national currency using the appropriate irrevocable conversion rate. This presentation facilitates comparisons within a country over time and ensures that the historical evolution is preserved. Please note, however, that pre-EMU euro are a notional unit and should not be used to form area aggregates or to carry out cross-country comparisons.',
      }
    ],
  },
  {
    id: 2,
    label: 'Health spending',
    children: [
      {
        id: 20,
        label: 'Data Characteristics',
        children: [
          {
            id: 200,
            label: 'Date last updated',
            value: 'August 2017',
          },
          {
            id: 201,
            label: 'Unit of measure used',
            value: 'The units used to present data in AFA are millions of national currency for monetary variables and units for the other variables. Monetary variables are in current prices. Euro-area countries: national currency data is expressed in euro beginning with the year of entry into the Economic and Monetary Union (EMU). For years prior to the year of entry into EMU, data have been converted from the former national currency using the appropriate irrevocable conversion rate. This presentation facilitates comparisons within a country over time and ensures that the historical evolution is preserved. Please note, however, that pre-EMU euro are a notional unit and should not be used to form area aggregates or to carry out cross-country comparisons.',
          },
          {
            id: 202,
            label: 'References',
            value: '<a href=\"https://de-demo.siscc.org/vis?lc=en&amp;df[ds]=staging%3ASIS-CC-reset&amp;df[id]=MG&amp;df[ag]=UNICEF&amp;df[vs]=5.0&amp;av=true&amp;pd=2018%2C2020&amp;dq=.MG_ASYLM_CNTRY_ASYLM.&amp;vw=tb\" target=\"_blank\" rel=\"noopener\">https://de-demo.siscc.org/vis?lc=en&amp;df[ds]=staging%3ASIS-CC-reset&amp;df[id]=MG&amp;df[ag]=UNICEF&amp;df[vs]=5.0&amp;av=true&amp;pd=2018%2C2020&amp;dq=.MG_ASYLM_CNTRY_ASYLM.&amp;vw=tb</a>'         
          },
        ]
      },
      {
        id: 21,
        label: 'Database Specific',
        children: [
          {
            id: 210,
            label: 'Date last updated',
            value: 'August 2015',
          },
          {
            id: 211,
            label: 'Abstract',
            value: 'This table contains figures on the health spending of the European Union by industry according to the International Standard Industrial Classification (ISIC Revision 4).'
          }
        ]
      }
    ]
  },
  {
    id: 3,
    label: 'Health spending by sector',
    children: [
      {
        id: 30,
        label: 'Data Characteristics',
        children: [
          {
            id: 300,
            label: 'Date last updated',
            value: 'August 2017',
          },
          {
            id: 301,
            label: 'Unit of measure used',
            value: 'The units used to present data in AFA are millions of national currency for monetary variables and units for the other variables. Monetary variables are in current prices. Euro-area countries: national currency data is expressed in euro beginning with the year of entry into the Economic and Monetary Union (EMU). For years prior to the year of entry into EMU, data have been converted from the former national currency using the appropriate irrevocable conversion rate. This presentation facilitates comparisons within a country over time and ensures that the historical evolution is preserved. Please note, however, that pre-EMU euro are a notional unit and should not be used to form area aggregates or to carry out cross-country comparisons.',
          }
        ]
      }
    ]
  }
]

const Wrapper = () => {
  const [isLoading, setIsLoading] = React.useState(false);

  return (
    <Usecase title="Demo" snippet={snippet} defaultExpanded>
      <Container>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Button
              style={{ margin: 4 }}
              size="small"
              variant="contained"
              color="primary"
              onClick={() => setIsLoading(!isLoading)}
            >
              Loading
            </Button>
          </Grid>
          <Grid item xs={12}>
            <CollapsibleTree
              data={data}
              isLoading={isLoading}
              defaultDepthLevel={1}
              icon={<SvgIcon
                focusable="false"
                aria-hidden="true"
                data-testid="ZoomInMapIcon"
                tabindex="-1"
                viewBox="0 0 24 24"
                title="ZoomInMapIcon"
              >
                <path d="M9 9V3H7v2.59L3.91 2.5 2.5 3.91 5.59 7H3v2zm12 0V7h-2.59l3.09-3.09-1.41-1.41L17 5.59V3h-2v6zM3 15v2h2.59L2.5 20.09l1.41 1.41L7 18.41V21h2v-6zm12 0v6h2v-2.59l3.09 3.09 1.41-1.41L18.41 17H21v-2z"></path>
              </SvgIcon>}
              labels={{
                collapseAll: 'Collapse all',
                expandAll: 'Expand all',
                noValue: '[No data]',
              }}
            >
              <Button>Download</Button>
            </CollapsibleTree>
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
