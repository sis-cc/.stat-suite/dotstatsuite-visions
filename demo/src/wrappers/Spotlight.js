import React from 'react';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { Spotlight } from '../../../src';

const fields = {
  label: {
    id: 'label',
    accessor: 'label',
    isSelected: true,
  },
  code: {
    id: 'code',
    accessor: 'code',
    disabled: true,
  },
};

const spotlightOptions = (isSearch, hasOneOrLessFields) => ({
  fields: hasOneOrLessFields ? {} : fields,
  searchLabel: isSearch ? 'search' : null,
  iconName: isSearch ? null : 'filter-list',
  FieldLabelRenderer: v => v.id,
});

const Wrapper = () => {
  const [item, changeTerm] = React.useState({ term: 'toto' });
  return (
    <React.Fragment>
      <Divider />
      <Spotlight action={changeTerm} />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<Spotlight action={action} isRtl={isRtl}/>`}
      </SyntaxHighlighter>
      <Divider />
      <Spotlight
        term={item.term}
        hasClearAll
        hasCommit
        withBorder
        spotlight={spotlightOptions(true, true)}
        action={changeTerm}
        placeholder="placeholder"
      />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<Spotlight
    term={item.term}
    hasClearAll hasCommit withBorder
    spotlight={spotlightOptions(true, true)}
    action={changeTerm}
    placeholder='placeholder'
  />`}
      </SyntaxHighlighter>
      <Divider />
      <Spotlight
        hasClearAll
        hasCommit
        withBorder
        fullWidth
        defaultSpotlight={spotlightOptions(true, false)}
        placeholder="placeholder"
        action={changeTerm}
        withAutoFocus
      />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<Spotlight
    hasClearAll hasCommit withBorder fullWidth
    defaultSpotlight={spotlightOptions(true, false)}
    placeholder="placeholder"
    action={changeTerm}
  />`}
      </SyntaxHighlighter>
      <Divider />
    </React.Fragment>
  );
};

export default Wrapper;
