import React from 'react';
import * as R from 'ramda';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import Usecase from '../components/Usecase';
import { Share, Accordion, VerticalButton } from '../../../src';
import { Container } from '@mui/material';

const props = {
  share: () => console.log('share submit'),
  modes: [
    {
      label:
        'Snapshot of data for the period defined (data will not change even if updated on the site)',
      value: 'snapshot',
      disabled: true,
      warningMessage: 'This mode is not available for the moment',
    },
    {
      label: 'Latest available data for the period defined',
      value: 'latest',
    },
  ],
  labels: {
    title: 'Data options for the Share',
    disclaimer: 'Please use this component',
    email: 'Email address (required)',
    submit: 'Share',
    successTitle: 'Thank you!',
    successMessage: 'We will send you the requested content shortly',
    errorTitle: 'Error',
    errorMessage: 'Something went wrong on our side',
  },
};

const snippets = [
  `<Share
  share={share}
  modes={modes}
  labels={labels}
  changeMode={setMode}
  changeMail={setMail}
  mode={mode}
  mail={mail}
  isSharing={isSharing}
  hasError={hasError}
  hasShared={hasShared}
  changeHasShared={setHasShared}
/>`,
];

const Wrapper = () => {
  const { share, modes, labels } = props;
  const [mode, setMode] = React.useState('snapshot');
  const [mail, setMail] = React.useState();
  const [isSharing, setSharing] = React.useState(false);
  const [hasShared, setHasShared] = React.useState(false);
  const [hasError, setError] = React.useState(true);

  return (
    <Usecase title="Demo" snippet={snippets[0]} defaultExpanded>
      <Container>
        <VerticalButton
          style={{ margin: 4 }}
          selected={isSharing}
          variant="outlined"
          onClick={() => setSharing(!isSharing)}
        >
          is Sharing
        </VerticalButton>
        <VerticalButton
          style={{ margin: 4 }}
          selected={hasShared}
          variant="outlined"
          onClick={() => setHasShared(!hasShared)}
        >
          has Shared
        </VerticalButton>
        <VerticalButton
          style={{ margin: 4 }}
          selected={R.not(hasError)}
          variant="outlined"
          onClick={() => setError(!hasError)}
        >
          has Error
        </VerticalButton>
      </Container>
      <Share
        share={share}
        modes={modes}
        labels={labels}
        changeMode={setMode}
        changeMail={setMail}
        mode={mode}
        mail={mail}
        isSharing={isSharing}
        hasError={hasError}
        hasShared={hasShared}
        changeHasShared={setHasShared}
      />
    </Usecase>
  );
};

export default Wrapper;
