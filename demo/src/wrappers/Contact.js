import React, { useState, Fragment } from 'react';
import * as R from 'ramda';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Usecase from '../components/Usecase';
import Contact from '../../../src/Contact';

const props = {
  send: R.tap(console.log),
  modes: [
    {
      label: 'Content question',
      value: 'question',
      message:"I cannot find the data I'm looking for, the data I found doesn't correspond to what I expected, or I have another question concerning the data."
    },
    {
      label: 'Technical problem',
      value: 'problem',
      message:"I have a technical problem to access, select, configure, view, share or download data,{br} or with any other feature of this web site."
    },
    {
      label:'Feedback',
      value:'feedback',
      message:"I wish to give some feedback on this site to help to improve it or to thank the teams behind it."
    }
  ],
  labels: {
    details : 'More details (required)',
    checkbox: 'I agree to be contacted to help improving the web site',
    personalTitle: 'Your title',
    email: 'Your email address (required)',
    name :'Your full name',
    organisation : 'Your organisation',
    titleForm : 'Your title',
    submit: 'Send',
    organisationPrivacyPolicy: 'OECD privacy policy',
    subject: 'Subject'
  },
  
};

const Wrapper = () => {
  const { send, modes, labels } = props;
  const [isSubmitDisable, setIsSubmitDisable] = useState(true);
  return (
    <Fragment>
      <Usecase title="Demo Page Contact" defaultExpanded>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant={isSubmitDisable ? 'contained' : 'outlined'}
              color="primary"
              style={{ margin: 4 }}
              onClick={() => setIsSubmitDisable(!isSubmitDisable)}
            >
              submit enable/disable
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Contact 
              send={send} 
              modes={modes}
              labels={labels}
              isSubmitDisable={isSubmitDisable}
              defaultFullName="Mike"
              defaultEmail="google@gmail.com"
            >
              <p>children captcha</p>
            </Contact>
          </Grid>
        </Grid>
      </Usecase>
    </Fragment>
  );
};

export default Wrapper;
