import React from 'react';
import AccessAlarm from '@mui/icons-material/AccessAlarm';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { NoData } from '../../../src';

const Wrapper = () => (
  <React.Fragment>
    <Divider />
    <NoData />
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {'<NoData />'}
    </SyntaxHighlighter>
    <Divider />
    <NoData message="custom message" />
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {'<NoData message="custom message" />'}
    </SyntaxHighlighter>
    <Divider />
    <NoData message="custom message" icon={<AccessAlarm color="secondary" fontSize="large" />} />
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {`<NoData
  message="custom message"
  icon={<AccessAlarm color="disabled" fontSize="large" />}
/>`}
    </SyntaxHighlighter>
  </React.Fragment>
);

export default Wrapper;
