import React from 'react';
import Container from '@mui/material/Container';
import MicIcon from '@mui/icons-material/Mic';
import Usecase from '../components/Usecase';
import { Button } from '../../../src';

const snippet = [`
<Button
  selected={isSelected}
  onClick={() => setSelected(!isSelected)}
  color="primary"
  startIcon={<MicIcon />}
>
    Mute
</Button>
`,
`
<Button
  selected={isSelected}
  onClick={() => setSelected(!isSelected)}
  alternative="siscc"
  variant="contained"
  color="primary"
  startIcon={<MicIcon />}
>
  siscc
</Button>
`
];

const Wrapper = () => {
  const [isSelected, setSelected] = React.useState(false);
  return (
    <React.Fragment>
      <Usecase title="• default alternative" snippet={snippet[0]} defaultExpanded>
        <Container>
          <Button
            selected={isSelected}
            onClick={() => setSelected(!isSelected)}
            color="primary"
            startIcon={<MicIcon />}
          >
            Mute
          </Button>
        </Container>
      </Usecase>
      <Usecase title="• siscc alternative" snippet={snippet[1]} defaultExpanded>
        <Container>
        <Button
          selected={isSelected}
          onClick={() => setSelected(!isSelected)}
          alternative="siscc"
          variant="contained"
          color="primary"
          startIcon={<MicIcon />}
        >
          siscc
        </Button>
        </Container>
      </Usecase>
    </React.Fragment>
  );
};

export default Wrapper;
