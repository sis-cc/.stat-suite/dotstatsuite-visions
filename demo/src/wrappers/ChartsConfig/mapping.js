import * as R from 'ramda';

const invariant = ['width', 'height'];
const focus = ['highlight', 'baseline'];
const scatter = ['scatterDimension', 'scatterX', 'scatterY'];
const symbol = ['symbolDimension'];
const stack = ['stackedDimension', 'stackedMode'];
export const axisFactory = key => ([`min${key}`, `max${key}`, `step${key}`]);

const mapping = {
  bar: [...invariant, ...focus, ...R.insert(2, 'pivotY', axisFactory('Y'))],
  row: [...invariant, ...focus, ...R.insert(2, 'pivotX', axisFactory('X'))],
  timeline: [...invariant, ...focus, ...axisFactory('Y'), 'freqStep'],
  scatter: [
    ...invariant,
    ...focus,
    ...axisFactory('Y'),
    ...axisFactory('X'),
    ...scatter,
  ],
  horizontalSymbol: [...invariant, ...focus, ...axisFactory('X'), ...symbol],
  verticalSymbol: [...invariant, ...focus, ...axisFactory('Y'), ...symbol],
  stackedBar: [...invariant, ...focus, ...axisFactory('Y'), ...stack],
  stackedRow: [...invariant, ...focus, ...axisFactory('X'), ...stack],
  choropleth: [...invariant],
};

export default mapping;
