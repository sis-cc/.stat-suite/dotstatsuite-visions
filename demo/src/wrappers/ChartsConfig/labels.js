const labels = {
  focus: 'Chart Focus',
  highlight: 'Highlights',
  baseline: 'Baseline',
  select: 'Select',
  size: 'Chart Size',
  width: 'Width',
  height: 'Height',
  series: 'Chart Series',
  scatterDimension: 'Dimension for X and Y axes',
  scatterX: 'Dimension value for X-axis',
  scatterY: 'Dimension value for Y-axis',
  symbolDimension: 'Symbol Dimension',
  stackedDimension: 'Stacked Dimension',
  stackedMode: 'Stacked Mode',
  stackedModeOptions: {
    values: 'Values',
    percent: 'Percent',
  },
  axisX: 'Chart X Axis',
  axisY: 'Chart Y Axis',
  max: 'Max',
  min: 'Min',
  pivot: 'Pivot',
  step: 'Step (period)',
  frequency: 'Frequency',
  freqStep: 'Frequency Step',
  logo: 'Remove logo',
  copyright: 'Remove copyright',
  title: 'Title',
  subtitle: 'Subtitle',
  source: 'Source',
  information: 'Information',
  reset: 'reset label',
  uniqFocusOption: 'serie',
  display: 'Display',
  displayOptions: {
    name: 'Name',
    code: 'Code',
    both: 'Both'
  }
};

export default labels;
