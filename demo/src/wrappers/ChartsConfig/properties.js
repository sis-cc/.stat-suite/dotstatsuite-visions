import * as R from 'ramda';

const axisFactory = key => ([`min${key}`, `max${key}`, `pivot${key}`, `step${key}`]);

const longLabel = 'Sed maximus purus enim, id tristique mauris porta ac. Etiam aliquet rutrum lectus, consequat posuere nisl pharetra ac. Nam lobortis. Sed maximus purus enim, id tristique mauris porta ac. Etiam aliquet rutrum lectus, consequat posuere nisl pharetra ac. Nam lobortis. Sed maximus purus enim, id tristique mauris porta ac. Etiam aliquet rutrum lectus, consequat posuere nisl pharetra ac. Nam lobortis.';
const valuesFactory = (key, time) => R.times(
  index => ({
    value: `${key}-id-${index}`,
    label: index === 2 ? longLabel : `${key}-label-${index}`,
  }),
  time,
);

const onChange = id => v => console.log(id, v);

const dimensionFactory = ({id, key, time = 5, vl}) => {
  const options = valuesFactory(key || id, time);
  return {
    id,
    onChange: onChange(id),
    options,
    value: vl ? R.take(vl, options) : R.head(options),
  };
};

const dataEditFactory = key => ({
  id: key,
  onSubmit: onChange(`change:${key}`),
  onReset: onChange(`reset:${key}`),
  value: `My ${key}`,
  isActive: true,
});

export const dataEdit = {
  title: dataEditFactory('title'),
  subtitle: dataEditFactory('subtitle'),
  source: dataEditFactory('source'),
  logo: {
    id: 'logo',
    onChange: onChange(`logo`),
    checked: true,
    isActive: true,
  },
  copyright: {
    id: 'copyright',
    onChange: onChange(`copyright`),
    isActive: true,
  },
};

export default () => {
  return {
    highlight: dimensionFactory({id: 'highlight', vl: 2}),
    baseline: dimensionFactory({ id: 'baseline', vl: 1 }),
    scatterDimension: dimensionFactory({ id: 'scatterDimension' }),
    scatterX: dimensionFactory({ id: 'scatterX', key: 'scatterX-val' }),
    scatterY: dimensionFactory({ id: 'scatterY', key: 'scatterY-val' }),
    symbolDimension: dimensionFactory({ id: 'symbolDimension' }),
    stackedDimension: dimensionFactory({ id: 'stackedDimension' }),
    width: {
      id: 'width',
      onSubmit: onChange('width'),
      value: null,
      placeholder: 50
    },
    height: {
      id: 'height',
      onSubmit: onChange('height'),
      value: null,
    },
    ...R.reduce(
      (memo, id) => {
        memo[id] = { id, onSubmit: onChange(id), value: null };
        return memo;
      }, {}, axisFactory('X'),
    ),
    ...R.reduce(
      (memo, id) => {
        memo[id] = { id, onSubmit: onChange(id), value: null };
        return memo;
      }, {}, axisFactory('Y'),
    ),
    freqStep: {
      id: 'freqStep',
      onSubmit: onChange('freqStep'),
      value: null,
    },
    stackedMode: {
      id: 'stackedMode',
      onChange: onChange('stackedMode'),
      options: [{ value: 'values' }, { value: 'percent' }],
      value: 'percent',
    },
    displayMode: {
      id: 'displayMode',
      isActive: true,
      onChange: onChange(`display`),
      options: [{ value: 'name' }, { value: 'code' }, { value: 'both' }],
      value: 'name',
    },
  };
};