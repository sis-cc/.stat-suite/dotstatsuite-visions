import React from 'react';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { LabelDivider } from '../../../src';

const Wrapper = () => (
  <React.Fragment>
    <Divider />
    <LabelDivider label={'Jean Eudes'} />
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {`<LabelDivider label={'Jean Eudes'} />`}
    </SyntaxHighlighter>
    <Divider />
    <LabelDivider label={'Jean Eudes in Red'} color="red" />
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {`<LabelDivider label={'Jean Eudes in Red'} color="#ff0000" />`}
    </SyntaxHighlighter>
  </React.Fragment>
);

export default Wrapper;
