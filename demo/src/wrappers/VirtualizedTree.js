import React, { useState } from 'react';
import * as R from 'ramda';
import Usecase from '../components/Usecase';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import { VirtualizedTree } from '../../../src';

const addChildren = list => {
  const children = R.reduce((acc, item) => {
    if (R.isNil(item.parentId)) return acc;
    return R.append(
      R.times(
        num => ({
          count: Math.floor(Math.random() * 100) + 1,
          id: R.pipe(R.multiply(100000), R.add(num))(item.id),
          parentId: R.add(item.id, 10),
          label: `label #${R.pipe(R.multiply(100000), R.add(num))(item.id)}`,
        }),
        100,
      ),
    )(acc);
  }, [])(list);
  return R.pipe(R.append(list), R.flatten)(children);
};

const largeList = R.pipe(
  R.times(id => ({ count: Math.floor(Math.random() * 100) + 1, id, label: `label #${id}`, isEnabled: true, hasData: true })),
  R.map(item => {
    if (item.id <= 10) return item;
    if (item.id <= 20) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 30) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 40) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 50) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 60) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    return R.assoc('parentId', item.id - 10)(item);
  }),
  addChildren,
)(100);

const snippet = '';

const SanitizedInnerHTML = ({ html, ...rest }) => {
  return (
    <span dangerouslySetInnerHTML={{ __html: html }} {...rest} />
  );
};

const labels = {
  selection: {
    title: 'Bulk selections',
    currentLevel: 'Entire currently displayed list',
    level: 'Entire hierarchy level'
  },
  childrenNavigateDesc: 'Drill-down to next level',
  backHelper: 'back to this level',
  disableItemLabel: 'Disable',
};

const Wrapper = () => {
  const [list, setList] = useState(largeList);
  const [accessibility, setAccessibility] = useState(false);
  const [disableAccessor, setDisableAccessor] = useState(true);
  const [expandedIds, setExpandedIds] = useState({});
  const [simpleSelectionMode, setSimpleSelectionMode] = useState(false);

  const changeSelection = (ids) => {
    console.log(ids) // demo purpose
    setList(R.map((item) => {
      if (R.includes(item.id, ids)) return R.over(R.lensProp('isSelected'), R.not)(item)
      return item;
    })(list))
  }

  const expand = (id) => {
    setExpandedIds({ ...expandedIds, [id]: id });
  }

  const collapse = (id) => {
    setExpandedIds(R.dissoc(id, expandedIds));
  }

  return (
    <Usecase title="Demo" defaultExpanded snippet={snippet}>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant={disableAccessor ? "contained" : "outlined"}
              color="primary"
              onClick={() => setDisableAccessor(!disableAccessor)}
            >
              disable Accessor
            </Button>
            &nbsp;&nbsp;
            <Button
              size="small"
              variant={simpleSelectionMode ? "contained" : "outlined"}
              color="primary"
              onClick={() => setSimpleSelectionMode(!simpleSelectionMode)}
            >
              disable multi selection
            </Button>
          </Grid>
          <Grid item xs={12}>
            <VirtualizedTree
              items={list}
              isNarrow={false}
              disableAccessor={disableAccessor ? R.pipe(R.prop('isEnabled'), R.not) : undefined}
              changeSelection={changeSelection}
              accessibility={accessibility}
              labels={{
                disableItemLabel: 'Disable',
              }}
              labelRenderer={R.prop('label')}
              expandedIds={expandedIds}
              expand={expand}
              collapse={collapse}
              simpleSelectionMode={simpleSelectionMode}
            />
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
