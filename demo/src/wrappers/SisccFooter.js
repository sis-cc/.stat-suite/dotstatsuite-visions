import React, { Fragment } from 'react';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { SisccFooter, ExpansionPanel } from '../../../src';

const props = {
  leftLabel: 'Built by icon SIS-CC using .Stat Suite',
  rightLabel: <Fragment>This is a beta version of the .Stat Data Explorer.<br/>
    It covers a subset of the final expected features and is designed for test purposes only.</Fragment>,
};

const Wrapper = () => {
  const { leftLabel, rightLabel } = props;

  return (
    <React.Fragment>
      <ExpansionPanel id="expectedProps" label="Expected props" maxHeight={false}>
        <SyntaxHighlighter language="json" style={okaidia}>
          {JSON.stringify({ ...props }, null, '\t')}
        </SyntaxHighlighter>
      </ExpansionPanel>
      <Divider />
      <SisccFooter leftLabel={leftLabel} rightLabel={rightLabel} />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<SisccFooter
  leftLabel={leftLabel}
  rightLabel={rightLabel}
/>`}
      </SyntaxHighlighter>
      <Divider />
    </React.Fragment>
  );
};

export default Wrapper;
