import React from 'react';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { VerticalButton } from '../../../src';
import MicIcon from '@mui/icons-material/Mic';

const Wrapper = () => {
  const [isSelected, setSelected] = React.useState(false);
  return (
    <React.Fragment>
      <Divider />
      <VerticalButton
        selected={isSelected}
        onClick={() => setSelected(!isSelected)}
        color="primary"
        startIcon={<MicIcon />}
      >
        Mute
      </VerticalButton>
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<VerticalButton 
  selected={isSelected} 
  onClick={() => setSelected(!isSelected)} 
  color='primary' 
  startIcon={<MicIcon />}
>
  Mute
</VerticalButton>

<VerticalButton 
  selected={isSelected} 
  onClick={() => setSelected(!isSelected)} 
  color='primary'
>
  Mute
</VerticalButton>

<VerticalButton 
  selected={isSelected} 
  onClick={() => setSelected(!isSelected)} 
  color='primary' 
  startIcon={<MicIcon />}
/>`}
      </SyntaxHighlighter>
      <Divider />
      <VerticalButton
        selected={isSelected}
        onClick={() => setSelected(!isSelected)}
        color="primary"
        loading
        startIcon={<MicIcon />}
      >
        Mute
      </VerticalButton>
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<VerticalButton 
  selected={isSelected} 
  onClick={() => setSelected(!isSelected)} 
  color='primary'
  loading
  startIcon={<MicIcon />}
>
  Mute
</VerticalButton>`}
      </SyntaxHighlighter>
    </React.Fragment>
  );
};

export default Wrapper;
