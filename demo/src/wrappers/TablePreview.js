import React from 'react';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { TablePreview, ExpansionPanel } from '../../../src';

const props = {
  header: [
    {
      id: 'DSB',
      name: 'Disability status',
      count: 5,
    },
    {
      id: 'MEASURE',
      name: 'Measure',
      count: 2,
    },
  ],
  sections: [
    {
      id: 'REF_AREA',
      value: 'REF_AREA',
      name: 'Reference area',
      count: 3,
    },
    {
      id: 'SURVEY',
      value: 'SURVEY',
      name: 'Survey',
      count: 3,
    },
  ],
  rows: [
    {
      id: 'SEX',
      value: 'SEX',
      name: 'Sex',
      count: 2,
    },
    {
      id: 'TIME_PERIOD',
      value: 'TIME_PERIOD',
      name: 'Time period',
      count: 4,
    },
  ],
};

const Wrapper = () => {
  const { header, sections, rows } = props;
  return (
    <React.Fragment>
      <ExpansionPanel id="expectedProps" label="Expected props" maxHeight={false}>
        <SyntaxHighlighter language="json" style={okaidia}>
          {JSON.stringify({ ...props }, null, '\t')}
        </SyntaxHighlighter>
      </ExpansionPanel>
      <Divider />
      <TablePreview />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {'<TablePreview />'}
      </SyntaxHighlighter>
      <Divider />
      <TablePreview rows={rows} />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<TablePreview
  rows={rows}
/>`}
      </SyntaxHighlighter>
      <Divider />
      <TablePreview header={header} rows={rows} />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<TablePreview
  header={header}
  rows={rows}
/>`}
      </SyntaxHighlighter>
      <Divider />
      <TablePreview sections={sections} rows={rows} />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<TablePreview
  sections={sections}
  rows={rows}
/>`}
      </SyntaxHighlighter>
      <Divider />
      <TablePreview header={header} sections={sections} rows={rows} />
      <SyntaxHighlighter language="jsx" style={okaidia}>
        {`<TablePreview
  header={header}
  sections={sections}
  rows={rows}
/>`}
      </SyntaxHighlighter>
    </React.Fragment>
  );
};
export default Wrapper;
