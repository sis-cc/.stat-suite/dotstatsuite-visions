import React from 'react';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import { PeriodPicker } from '../../../src';
import Usecase from '../components/Usecase';
import { newDate } from '../../../src/utils';

const labels = {
  frequency: 'Frequency',
  year: 'year', // de.period.year
  semester: 'semester', // de.period.semester
  quarter: 'quarter', // de.period.quarter
  month: 'month', // de.period.month
  week: 'week', // de.period.week
  hour: 'hour', // de.period.hour
  minute: 'minute', // de.period.minute
  full_hour: 'full_hour', // de.period.full_hour
  day: 'day', // de.period.day
  end: 'end', // de.period.end
  start: 'start', // de.period.start
  A: 'Annual',
  S: 'Half-Yearly',
  Q: 'Quarterly',
  M: 'Monthly',
  W: 'Weekly',
  D: 'Daily',
  H: 'Hourly',
  N: 'Minutely',
  B: 'Business',
};

const Wrapper = () => {
  const [period, setPeriod] = React.useState([newDate("2020-03-01T12:00:00.000Z"), undefined]);
  const [frequency, setFrequency] = React.useState('W');
  const [isFrequenceDisabled, setIsFrequenceDisabled] = React.useState(false);
  const [isPeriodDisabled, setIsPeriodDisabled] = React.useState(false);
  const [isDisabled, setIsDisabled] = React.useState(false);

  const boundaries = [newDate("1970-01-01T00:00:00.000Z"), newDate("2021-05-15T15:54:00.000Z")]

  return (
    <Usecase title="Demo" defaultExpanded>
      <Container>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant={isFrequenceDisabled ? 'contained' : 'outlined'}
              color="primary"
              style={{ margin: 4 }}
              onClick={() => setIsFrequenceDisabled(!isFrequenceDisabled)}
            >
              disable Frequency
            </Button>
            <Button
              style={{ margin: 4 }}
              size="small"
              variant={isPeriodDisabled ? 'contained' : 'outlined'}
              color="primary"
              onClick={() => setIsPeriodDisabled(!isPeriodDisabled)}
            >
              hide Period
            </Button>
            <Button
              style={{ margin: 4 }}
              size="small"
              variant={isDisabled ? 'contained' : 'outlined'}
              color="primary"
              onClick={() => setIsDisabled(!isDisabled)}
            >
              disable Period
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <PeriodPicker
              defaultFrequency={frequency}
              changeFrequency={setFrequency}
              availableFrequencies={['A', 'Q', 'M']}
              boundaries={boundaries}
              availableBoundaries={['1990-06-01T00:00:00', '2020-05-31T00:00:00']}
              period={period}
              changePeriod={setPeriod}
              labels={labels}
              frequencyDisabled={isFrequenceDisabled}
              periodDisabled={isPeriodDisabled}
              disabled={isDisabled}
            />
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
