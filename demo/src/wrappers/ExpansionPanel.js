import React from 'react';
import { Container, Grid, Button, Typography } from '@mui/material';
import Usecase from '../components/Usecase';
import { ExpansionPanel, Tag } from '../../../src';
import { useStyles } from '../../../src/ExpansionPanel/styles';

const snippet = `<ExpansionPanel
  id="Visons_Expansion_Panel"
  label={'Label of Panel'}
  tag={<Tag>344/3242</Tag>}
  isPinned={R.prop('isPinned')}
  pinnedIcon={pinnedIcon}
  pinnedLabel='Pinned'
>
  children
</ExpansionPanel>`;

const Wrapper = () => {
  const classes = useStyles();

  const [isPinned, setPinned] = React.useState(true);
  const [isPopper, setIsPopper] = React.useState(false);

  return (
    <Usecase title="Demo" snippet={snippet} defaultExpanded>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={() => setPinned(!isPinned)}
            >
              Pinned
            </Button>
            &nbsp;
            <Button
              size="small"
              variant="contained"
              color="primary"
              onClick={() => setIsPopper(!isPopper)}
            >
              popper
            </Button>
          </Grid>
          <Grid item xs={6}>
            <ExpansionPanel
              id="Visions_Expansion_Panel"
              label={'Label of Panel'}
              tag={<Tag>344/3242</Tag>}
              isPinned={isPinned}
              pinnedLabel="Pinned"
              isPopper={isPopper}
              popperLabel={'help'}
            >
              children
            </ExpansionPanel>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="body2" className={classes.title}>
              Filters
            </Typography>
            <ExpansionPanel label={'Label of Panel'} tag={<Tag>344/3242</Tag>}>
              children
            </ExpansionPanel>
            <ExpansionPanel label={'More filters'} moreFilters>
              <ExpansionPanel label={'filter one'} tag={<Tag>41/342</Tag>}>
                children
              </ExpansionPanel>
              <ExpansionPanel label={'filter too'} tag={<Tag>58/32942</Tag>}>
                children
              </ExpansionPanel>
            </ExpansionPanel>
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
