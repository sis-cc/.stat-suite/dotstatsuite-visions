import React from 'react';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Divider from '../components/Divider';
import { TableHeader } from '../../../src';

const Wrapper = () => (
  <React.Fragment>
    <Divider />
    <TableHeader header={{ title: 'Title' }} />
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {'<TableHeader header={{ title: "Title" }}/>'}
    </SyntaxHighlighter>
    <Divider />
    <TableHeader
      header={{ title: 'Title', subtitle: ['Elem 1', 'Elem 2', 'Elem 3'], uprs: 'uprs' }}
    />
    <SyntaxHighlighter language="jsx" style={okaidia}>
      {
        '<TableHeader header={{ title: "Title", subtitle: ["Elem 1","Elem 2", "Elem 3"], uprs: "uprs" }} />'
      }
    </SyntaxHighlighter>
  </React.Fragment>
);

export default Wrapper;
