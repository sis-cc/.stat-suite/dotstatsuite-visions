import React from 'react';
import * as R from 'ramda';
import Usecase from '../components/Usecase';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import FolderIcon from '@mui/icons-material/Folder';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import { DataFooter, T4_BREAKPOINTS } from '../../../src';

const LOGO = 'http://de-qa-oecd.redpelicans.com/assets/siscc/data-explorer/images/sis-cc-logo.png';

const btnFactory = (label, handler) => (
  <Button style={{width: 125}} size="small" color="primary" onClick={handler}>{label}</Button>
);

const Wrapper = () => {
  const source = { label: 'Source', link: 'data.oecd.org' };

  const copyright = {
    label: <span dangerouslySetInnerHTML={{ __html: "&copy;SIS-CC" }}></span>,
    content: <span dangerouslySetInnerHTML={{
      __html: "Copyright: <a href='data.oecd.org'>terms</a>",
    }}></span>,
  };

  const [isCopyrighted, setIsCopyrighted] = React.useState(true);
  const [hasLogo, setHasLogo] = React.useState(true);
  const [isSourced, setIsSourced] = React.useState(true);
  const [isLegended, setIsLegended] = React.useState(false);
  const [isEdgyLegend, setIsEdgyLegend] = React.useState(false);
  const [isSingleLegend, setIsSingleLegend] = React.useState(false);
  const [isBordered, setIsBordered] = React.useState(true);

  const legend = (
    <List style={{ padding: 0 }}>
      {R.map(value => (
        <ListItem key={value}  style={{ padding: 0 }}>
          <ListItemIcon style={{ minWidth: 0 }}><FolderIcon size="small" /></ListItemIcon>
          <ListItemText primary={
            isEdgyLegend
            ? 'Nullam vitae nunc posuere, vestibulum augue eu, fermentum nisi, Etiam eget nunc sed metus egestas rutrum'
            : `item #${value}`
          }/>
        </ListItem>
      ), isSingleLegend ? [1] : [1,2,3])}
    </List>
  );

  const breakpoints = R.pipe(R.omit(['xs2', 'xs3', 'md2']), R.toPairs)(T4_BREAKPOINTS);

  return (
    <Usecase title="Demo" defaultExpanded>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            {btnFactory(
              `${hasLogo ? 'unset' : 'set'} logo`,
              () => setHasLogo(!hasLogo)
            )}
            &nbsp;
            {btnFactory(
              `${isSourced ? 'unset' : 'set'} source`,
              () => setIsSourced(!isSourced)
            )}
            &nbsp;
            {btnFactory(
              `${isCopyrighted ? 'unset' : 'set'} copyright`,
              () => setIsCopyrighted(!isCopyrighted)
            )}
            &nbsp;
            {btnFactory(
              `${isLegended ? 'unset' : 'set'} legend`,
              () => {
                setIsLegended(!isLegended);
                setIsEdgyLegend(false);
              }
            )}
            &nbsp;
            {btnFactory(
              `${isSingleLegend ? 'unset' : 'set'} single legend`,
              () => {
                setIsSingleLegend(!isSingleLegend);
              }
            )}
            &nbsp;
            {btnFactory(
              `${isEdgyLegend ? 'unset' : 'set'} edgy legend`,
              () => {
                setIsEdgyLegend(!isEdgyLegend);
                setIsLegended(false);
              }
            )}
            &nbsp;
            {btnFactory(
              `${isBordered ? 'unset' : 'set'} borders`,
              () => setIsBordered(!isBordered)
            )}
          </Grid>
          <Grid item xs={12}>
            <Typography display="block" variant="body2">Resize me!</Typography>
            <Box border={isBordered ? 1 : 0}>
              <DataFooter
                source={isSourced ? source : null}
                legend={isLegended || isEdgyLegend ? legend : null}
                copyright={isCopyrighted ? copyright : null}
                logo={hasLogo ? LOGO : null}
              />
            </Box>
          </Grid>
          {R.map(([key, val]) => (
            <Grid item xs={12} key={key}>
              <Typography display="block" variant="body2">{key}: {val}px</Typography>
              <Box style={{
                width: val + (isBordered ? 2 : 0), border: isBordered ? '1px solid' : 0
              }} >
                <DataFooter
                  source={isSourced ? source : null}
                  legend={isLegended || isEdgyLegend ? legend : null}
                  copyright={isCopyrighted ? copyright : null}
                  logo={hasLogo ? LOGO : null}
                />
              </Box>
            </Grid>
          ), breakpoints)}
        </Grid>  
      </Container>
    </Usecase>
  );
};

export default Wrapper;
