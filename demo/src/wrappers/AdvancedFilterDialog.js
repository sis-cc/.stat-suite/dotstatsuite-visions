import React, { useState } from 'react';
import * as R from 'ramda';
import Usecase from '../components/Usecase';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import { useTheme } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { AdvancedFilterDialog } from '../../../src';

const tree = [
  { id: 'AUS', label: 'AUS-label', hasData: true, isEnabled: true, order: 4, parentId: undefined },
  { id: 'AUT', label: 'AUT-label', hasData: false, isEnabled: true, order: 5, parentId: undefined },
  { id: 'OECD', label: 'OECD-label', hasData: false, isEnabled: false, hierarchicalId: 'OECD', order: 0, parentId: undefined },
  { id: 'EU', label: 'EU-label', hasData: true, isEnabled: true, hierarchicalId: 'EU', order: 1, parentId: undefined, isForced: true },
  { id: 'FRA', label: 'FRA-label', hasData: true, isEnabled: true, order: 3, parentId: 'EU', imposedIds: ['EU'], hierarchicalId: 'EU.FRA', isSelected: false },
  { id: 'GER', label: 'GER-label', hasData: true, isEnabled: true, order: 6, parentId: 'EU', imposedIds: ['EU'], hierarchicalId: 'EU.GER' },
  { id: 'GER', label: 'GER-label', hasData: true, isEnabled: true, order: 6, parentId: 'OECD', hierarchicalId: 'OECD.GER', imposedIds: ['OECD'] },
  { id: 'JPN', label: 'JPN-label', hasData: true, isEnabled: true, order: 2, parentId: 'OECD', hierarchicalId: 'OECD.JPN' },
  { id: 'FRA', label: 'FRA-label', hasData: true, isEnabled: true, order: 3, parentId: 'OECD', hierarchicalId: 'OECD.FRA', isSelected: false },
  { id: 'a', label: 'World-label', hasData: false, isEnabled: false, order: 0, parentId: undefined, hierarchicalId: 'A', isSelected: false },
  { id: 'b', label: 'World-label', hasData: false, isEnabled: false, order: 0, parentId: undefined, hierarchicalId: 'B', isSelected: false },
  { id: 'c', label: 'World-label', hasData: false, isEnabled: false, order: 0, parentId: undefined, hierarchicalId: 'C', isSelected: false },
  { id: 'd', label: 'World-label', hasData: false, isEnabled: false, order: 0, parentId: undefined, hierarchicalId: 'D', isSelected: false },
  { id: 'e', label: 'World-label', hasData: false, isEnabled: false, order: 0, parentId: undefined, hierarchicalId: 'E', isSelected: false }
];
const a = [
  {
    "id": "GDP",
    "label": "1--Gross domestic product",
    "order": 1,
    "position": 0,
    "display": true,
    "hasData": false,
    "isEnabled": false
  },
  {
    "id": "B1_GA",
    "label": "Gross domestic product (output approach)",
    "order": 1,
    "parentId": "GDP",
    "position": 1,
    "display": true,
    "hasData": true,
    "isSelected": true,
    "isEnabled": true
  },
  {
    "id": "D1",
    "label": "Compensation of employees",
    "order": 1,
    "parentId": "B1_GI",
    "position": 6,
    "display": true,
    "hasData": true,
    "isEnabled": false
  },
  {
    "id": "B1_GE",
    "label": "Gross domestic product (expenditure approach)",
    "order": 2,
    "parentId": "GDP",
    "position": 3,
    "display": true,
    "hasData": true,
    "isSelected": true,
    "isEnabled": true
  },
  {
    "id": "B1_GI",
    "label": "Gross domestic product (income approach)",
    "order": 3,
    "parentId": "GDP",
    "position": 5,
    "display": true,
    "hasData": true,
    "isSelected": true,
    "isEnabled": true
  },
  {
    "id": "B11",
    "label": "External balance of goods and services",
    "order": 4,
    "parentId": "B1_GE",
    "position": 7,
    "display": true,
    "hasData": true,
    "isSelected": true,
    "isEnabled": true
  },
  {
    "id": "D11VA",
    "label": "Agriculture, forestry and fishing (ISIC rev4)",
    "order": 7,
    "parentId": "D11",
    "position": 88,
    "display": true,
    "hasData": true,
    "isEnabled": false
  },
  {
    "id": "D11VB_E",
    "label": "Industry, including energy (ISIC rev4)",
    "order": 8,
    "parentId": "D11",
    "position": 36,
    "display": true,
    "hasData": true,
    "isEnabled": false
  },
  {
    "id": "D11",
    "label": "Wages and salaries",
    "order": 17,
    "parentId": "D1",
    "position": 12,
    "display": true,
    "hasData": true,
    "isEnabled": false
  }
]
const smallList = [
  { count: 22, id: 1, label: 'Blue', isSelected: true, description: 'Green is the color of the sky' },
  { count: 2, id: 2, label: 'Green', isSelected: true, description: 'Green is the color of the tree' },
  {
    count: 8,
    id: 3,
    label: 'The MuiListItemSecondaryAction name can be used for providing default props or style overrides at the theme level',
    isSelected: true,
    description: `<span><strong>This indicator is based on harmonized definitions used by the ILO in the context of producing global estimates. Data may differ from nationally reported figures. </strong>
    Children refer to persons ages 5-17 years old. For more information, refer to the 
    <a href="https://ilostat.ilo.org/resources/concepts-and-definitions/">concepts and definitions</a> page.</span>`
  },
  { count: 9, id: 4, label: 'Yellow' },
  { count: 14, id: 5, label: 'Pink', svgPath: 'M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z' },
  { id: 6, label: 'Purple' },
];

const addChildren = list => {
  const children = R.reduce((acc, item) => {
    if (R.isNil(item.parentId)) return acc;
    return R.append(
      R.times(
        num => ({
          count: Math.floor(Math.random() * 100) + 1,
          id: R.pipe(R.multiply(100000), R.add(num))(item.id),
          parentId: R.add(item.id, 10),
          label: `label #${R.pipe(R.multiply(100000), R.add(num))(item.id)}`,
        }),
        100,
      ),
    )(acc);
  }, [])(list);
  return R.pipe(R.append(list), R.flatten)(children);
};

const largeList = R.pipe(
  R.times(id => ({ count: Math.floor(Math.random() * 100) + 1, id, label: `label #${id}`, isEnabled: true, hasData: true })),
  R.map(item => {
    if (item.id <= 10) return item;
    if (item.id <= 20) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 30) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 40) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 50) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    if (item.id <= 60) return R.assoc('parentId', R.subtract(item.id)(10))(item);
    return R.assoc('parentId', item.id - 10)(item);
  }),
  addChildren,
)(100);

const snippet = '';

const SanitizedInnerHTML = ({ html, ...rest }) => {
  return (
    <span dangerouslySetInnerHTML={{ __html: html }} {...rest} />
  );
};

const labels = {
  selection: {
    title: 'Bulk selections',
    currentLevel: 'Entire currently displayed list',
    level: 'Entire hierarchy level'
  },
  childrenNavigateDesc: 'Drill-down to next level',
  backHelper: 'back to this level',
  disableItemLabel: 'Disable',
};

const Wrapper = () => {
  const [filterOpenId, setFilterOpenId] = useState('spotlight');
  const originals = { normal: smallList, spotlight: tree, filter: largeList, advanced: largeList };
  const [lists, setLists] = useState(originals);
  const [accessibility, setAccessibility] = useState(false);
  const [disableAccessor, setDisableAccessor] = useState(true);
  const [isOpenDialog, setIsOpenDialog] = useState(true);

  const changeSelection = (selection, ids) => {
    console.log(selection, ids) // demo purpose
    setLists({
      ...lists,
      [selection]: R.map((item) => {
        if (R.includes(item.id, ids)) return R.over(R.lensProp('isSelected'), R.not)(item)
        return item;
      })(R.prop(selection)(lists))
    })
  }
  const theme = useTheme();
  const isNarrow = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <Usecase title="Demo" defaultExpanded snippet={snippet}>
      <Container>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Button
              size="small"
              variant="text"
              color="primary"
              onClick={() => setLists(originals)}
            >
              Back to default
            </Button>
            &nbsp;&nbsp;
            <Button
              size="small"
              variant={accessibility ? "contained" : "outlined"}
              color="primary"
              onClick={() => setAccessibility(!accessibility)}
            >
              Accessibility
            </Button>
            &nbsp;&nbsp;
            <Button
              size="small"
              variant={disableAccessor ? "contained" : "outlined"}
              color="primary"
              onClick={() => setDisableAccessor(!disableAccessor)}
            >
              disable Accessor
            </Button>
            &nbsp;&nbsp;
          </Grid>
          <Grid item xs={12}>
            <Button
              size="small"
              variant="outlined"
              color="primary"
              onClick={() => setIsOpenDialog(true)}
            >
              Open Dialog
            </Button>
            <AdvancedFilterDialog
              id="advanced"
              title="Advanced Filter"
              isOpen={isOpenDialog}
              items={R.prop('advanced')(lists)}
              isNarrow={isNarrow}
              disableAccessor={disableAccessor ? R.pipe(R.prop('isEnabled'), R.not) : undefined}
              changeSelection={changeSelection}
              onClose={() => setIsOpenDialog(false)}
              accessibility={accessibility}
              labels={{
                apply: 'Apply',
                cancel: 'Cancel',
                colapseAll: 'Colapse All',
                expandAll: 'Expand All',
                selectAll: 'Select All',
                deselectAll: 'Deselect All',
                disableItemLabel: 'Disable',
                placeholder: (a) => `${a} items`,
                singleSelection: 'Single item',
                childrenSelection: 'Item and direct children underneath',
                branchSelection: 'Whole branch',
                levelSelection: 'All items on same level',
                selectionMode: 'Selection Mode: ',
                hint: "Did you already know that you can select several items either by holding the mouse button while selecting neighbored items or by holding the 'Shift' key while selecting the last neighbored item or by holding the 'Ctrl' key while selecting the next individual item(s)?",
              }}
              labelRenderer={R.prop('label')}
            />
          </Grid>
        </Grid>
      </Container>
    </Usecase>
  );
};

export default Wrapper;
