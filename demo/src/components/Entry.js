import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import makeStyles from '@mui/styles/makeStyles';
import LockIcon from '@mui/icons-material/Lock';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import { SyntaxHighlighter } from '../components/SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import { getTagByType, getComponentById } from '../utils';
import components from '../components';

const useStyles = makeStyles(theme => ({
  wrapper: {
    paddingBottom: theme.spacing(3),
  },
  card: {
    marginBottom: theme.spacing(3),
  },
  cardContent: {
    paddingTop: 0,
    paddingBottom: 0,
  },
  details: {
    flexDirection: 'column',
    padding: 0,
  },
}));

const Entry = ({ componentId, children }) => {
  const classes = useStyles();
  const component = getComponentById(componentId)(components);
  const visibility = R.pipe(getTagByType('api'), R.propEq('visibility', 'private'))(component);
  const props = R.pipe(getTagByType('props'), R.prop('string'))(component);
  const theme = R.pipe(getTagByType('theme'), R.prop('string'))(component);
  const description = R.path(['description', 'full'], component);
  const componentCard = (
    <Card className={classes.card} square elevation={2}>
      <CardHeader
        title={componentId}
        subheader={<span dangerouslySetInnerHTML={{ __html: description }} />}
        avatar={visibility ? <LockIcon color="secondary" /> : <LockOpenIcon color="secondary" />}
      />
    </Card>
  );
  if (visibility) return componentCard;
  return (
    <div className={classes.wrapper}>
      {componentCard}
      {props && (
        <Accordion square elevation={2}>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography variant="body2">PropTypes</Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.details}>
            <SyntaxHighlighter language="javascript" style={okaidia}>
              {props}
            </SyntaxHighlighter>
          </AccordionDetails>
        </Accordion>
      )}
      {theme && (
        <Accordion square elevation={2}>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography variant="body2">Theme</Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.details}>
            <SyntaxHighlighter language="javascript" style={okaidia}>
              {theme}
            </SyntaxHighlighter>
          </AccordionDetails>
        </Accordion>
      )}
      {children}
    </div>
  );
};

Entry.propTypes = {
  componentId: PropTypes.string.isRequired,
  children: PropTypes.node,
};

export default Entry;
