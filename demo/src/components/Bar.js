import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import makeStyles from '@mui/styles/makeStyles';
import Badge from '@mui/material/Badge';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import CodeIcon from '@mui/icons-material/Code';
import HomeIcon from '@mui/icons-material/Home';
import MenuIcon from '@mui/icons-material/Menu';
import CloseIcon from '@mui/icons-material/Close';
import InvertColorsIcon from '@mui/icons-material/InvertColors';
import FormatTextdirectionLToRIcon from '@mui/icons-material/FormatTextdirectionLToR';
import FormatTextdirectionRToLIcon from '@mui/icons-material/FormatTextdirectionRToL';
import AccessibilityNewIcon from '@mui/icons-material/AccessibilityNew';
import AccessibilityIcon from '@mui/icons-material/Accessibility';
import ColorLensIcon from '@mui/icons-material/ColorLens';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import themes from '../themes';
import meta from '../../../package.json';
import components from '../components';

const useStyles = makeStyles(theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  grow: {
    flexGrow: 1,
  },
}));

export const Bar = ({
  themeId,
  setThemeId,
  setComponentId,
  toggleRtl,
  isRtl,
  toggleA11y,
  isA11y,
  toggleDrawer,
  drawers,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleTheme = key => {
    setThemeId(key);
    setAnchorEl(null);
  };

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar>
        <IconButton color="inherit" onClick={toggleDrawer('left')} edge="start" size="large">
          {drawers.left ? <CloseIcon /> : <MenuIcon />}
        </IconButton>
        <Typography variant="body2" color="inherit">
          <strong>{meta.name}</strong>
        </Typography>
        &nbsp;&nbsp;
        <img src="https://img.shields.io/npm/v/@sis-cc/dotstatsuite-visions?style=flat-square" />
        &nbsp;&nbsp;
        <img src="https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-visions/badges/master/pipeline.svg?style=flat-square" />
        &nbsp;&nbsp;
        <img src="https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-visions/badges/master/coverage.svg?style=flat-square" />
        &nbsp;&nbsp;
        <Typography variant="body2" color="inherit" className={classes.grow}>
          {components.length} components
        </Typography>
        <IconButton onClick={() => setComponentId()} color="inherit" size="large">
          <HomeIcon />
        </IconButton>
        <IconButton onClick={toggleDrawer('right')} color="inherit" size="large">
          <ColorLensIcon />
        </IconButton>
        <IconButton onClick={() => toggleRtl()} color="inherit" size="large">
          {isRtl ? <FormatTextdirectionLToRIcon /> : <FormatTextdirectionRToLIcon />}
        </IconButton>
        <IconButton onClick={e => setAnchorEl(e.currentTarget)} color="inherit" size="large">
          <InvertColorsIcon />
        </IconButton>
        <IconButton onClick={() => toggleA11y()} color="inherit" size="large">
          { isA11y ? <AccessibilityNewIcon /> : <AccessibilityIcon />}
        </IconButton>
        <Menu
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={() => setAnchorEl(null)}
        >
          {R.map(
            key => (
              <MenuItem
                selected={R.equals(key, themeId)}
                key={key}
                onClick={() => handleTheme(key)}
              >
                {key}
              </MenuItem>
            ),
            R.keys(themes),
          )}
        </Menu>
        <IconButton
          color="inherit"
          href={meta.repository}
          target="_blank"
          edge="end"
          size="large">
          <CodeIcon />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

Bar.propTypes = {
  themeId: PropTypes.string,
  setThemeId: PropTypes.func.isRequired,
  setComponentId: PropTypes.func.isRequired,
  toggleRtl: PropTypes.func.isRequired,
  isRtl: PropTypes.bool,
  toggleA11y: PropTypes.func.isRequired,
  isA11y: PropTypes.bool,
  toggleDrawer: PropTypes.func.isRequired,
  drawers: PropTypes.object,
};

export default Bar;
