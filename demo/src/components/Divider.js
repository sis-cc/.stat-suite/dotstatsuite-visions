import React from 'react';
import PropTypes from 'prop-types';
import makeStyles from '@mui/styles/makeStyles';
import Divider from '@mui/material/Divider';
import { LabelDivider } from '../../../src';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    width: '100%'
  },
}));

export const SpaceDivider = ({ label }) => {
  const classes = useStyles();
  return label ? (
    <LabelDivider label={label} className={classes.root} />
  ) : (
    <Divider className={classes.root} />
  );
};

SpaceDivider.propTypes = {
  label: PropTypes.string,
};

export default SpaceDivider;
