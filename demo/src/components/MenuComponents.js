import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import makeStyles from '@mui/styles/makeStyles';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Badge from '@mui/material/Badge';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import Tooltip from '@mui/material/Tooltip';
import SearchIcon from '@mui/icons-material/Search';
import { getId } from '../utils';
import components from '../components';
import { getTagByType, getComponentById } from '../utils';

const useStyles = makeStyles(theme => ({
  spotlight: {
    position: 'fixed',
    backgroundColor: 'white',
    width: "240px", // artisanal
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    marginTop: "64px", // artisanal
    zIndex: 1,
  },
  list: {
    marginTop: "40px", // artisanal
  },
  label: {
    display: 'flex',
    flexGrow: 1,
  },
  badge: {
    margin: theme.spacing(1),
    zIndex: 0,
  },
}));

const getComponent = id => getComponentById(id)(components);
const sideItems = R.pipe(
  R.map(item => {
    const id = getId(item);
    return {
      id,
      hasTestIdBadge: R.pipe(getTagByType('testId'), R.isNil)(getComponent(id)),
      hasDemoReady: R.pipe(getTagByType('demoReady'), R.isNil)(getComponent(id)),
    };
  }),
  R.sortBy(R.compose(R.toLower, R.prop('id'))),
)(components);

const MenuComponents = ({ componentId, setComponentId }) => {
  const classes = useStyles();
  const [term, setTerm] = useState('');
  const filteredItems = R.isEmpty(term)
    ? sideItems
    : R.filter(R.pipe(R.prop('id'), R.toLower, R.includes(R.toLower(term))), sideItems);
  return (
    <Fragment>
      <TextField
        value={term}
        onChange={e => setTerm(e.target.value)}
        className={classes.spotlight}
        size="small"
        variant="standard"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          ),
        }}
      />
      <List dense className={classes.list}>
        {R.map(({ id, hasTestIdBadge, hasDemoReady }) => (
          <ListItemButton
            key={id}
            onClick={() => setComponentId(id)}
            selected={R.equals(componentId, id)}
            className={classes.label}
          >
            <ListItemText>
              <Typography variant="body2">{id}</Typography>
            </ListItemText>
            <Tooltip title={hasTestIdBadge ? 'Need testid API' : ''}>
              <Badge
                overlap="rectangular"
                classes={{ root: classes.badge }}
                color={hasTestIdBadge ? 'primary' : undefined}
                variant="dot"
              />
            </Tooltip>
            <Tooltip title={hasDemoReady ? 'Need new demo' : ''}>
              <Badge
                overlap="rectangular"
                classes={{ root: classes.badge }}
                color={hasDemoReady ? 'error' : undefined}
                variant="dot"
              />
            </Tooltip>
          </ListItemButton>
        ))(filteredItems)}
      </List>
    </Fragment>
  );
};

MenuComponents.propTypes = {
  componentId: PropTypes.string,
  setComponentId: PropTypes.func.isRequired,
};

export default MenuComponents;