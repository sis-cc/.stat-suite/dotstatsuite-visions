import React from 'react';
import PropTypes from 'prop-types';
import makeStyles from '@mui/styles/makeStyles';
import { SyntaxHighlighter } from './SyntaxHighlighter';
import { okaidia } from 'react-syntax-highlighter/dist/esm/styles/prism';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Typography from '@mui/material/Typography';

const useStyles = makeStyles(theme => ({
  details: {
    flexDirection: 'column',
    padding: 0,
  },
  wrapper: {
    paddingBottom: theme.spacing(3),
  },
}));

const Usecase = ({ title, snippet, children, defaultExpanded }) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Accordion square elevation={2} defaultExpanded={defaultExpanded} TransitionProps={{ unmountOnExit: true }}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography variant="body2">{title}</Typography>
        </AccordionSummary>
        <AccordionDetails className={classes.details}>
          <div className={classes.wrapper}>{children}</div>
          {snippet && (
            <SyntaxHighlighter language="jsx" style={okaidia}>
              {snippet}
            </SyntaxHighlighter>
          )}
        </AccordionDetails>
      </Accordion>
    </React.Fragment>
  );
};

Usecase.propTypes = {
  title: PropTypes.string.isRequired,
  snippet: PropTypes.string,
  children: PropTypes.node.isRequired,
};

export default Usecase;
