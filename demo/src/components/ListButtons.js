import React from 'react';
import * as R from 'ramda';
import Button from '@mui/material/Button';
import { Grid } from '@mui/material';

const ListButtons = ({ listOfButtons, setToggle, toggle }) => {
  const buttons = R.pipe(
    R.unnest,
    R.mergeAll,
    R.keys,
  )(listOfButtons);

  return (
    <Grid container>
      {R.map(key => (
        <Grid item key={key}>
          <Button
            style={{ margin: 4 }}
            key={key}
            variant="outlined"
            onClick={() => setToggle({ ...toggle, [key]: !R.prop(key)(toggle) })}
          >
            {key}
          </Button>
        </Grid>
      ), buttons)}
    </Grid>
  );
};

export default ListButtons;
